default:
	@echo "try running: make -C c/lib install && make -C c/bin install"
	@echo "see README.md for more details"

documentation: index.html mandelbrot-perturbator.pdf mandelbrot-perturbator.png

index.html: README.md
	pandoc -s --toc --css mandelbrot-perturbator.css README.md -o index.html

mandelbrot-perturbator.pdf: README.md mandelbrot-perturbator.png
	pandoc -s --toc README.md -o mandelbrot-perturbator.pdf

mandelbrot-perturbator.png: mandelbrot-perturbator.toml
	m-perturbator-gtk --dpi=300 -i=mandelbrot-perturbator.toml -o=mandelbrot-perturbator.png
