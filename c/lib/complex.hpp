// mandelbrot-perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#ifndef MANDELBROT_PERTURBATOR_COMPLEX_H
#define MANDELBROT_PERTURBATOR_COMPLEX_H 1

#include <cmath>

template <typename R>
struct complex
{
  R re, im;
  inline complex(const complex<R> &c) : re(c.re), im(c.im) { };
  template<typename T>
  explicit inline complex(const complex<T> &c) : re(R(c.re)), im(R(c.im)) { }
  inline complex(const R &r, const R &i) : re(r), im(i) { };
  inline complex(const R &r) : re(r), im(0) { };
  inline complex(const int &r) : re(r), im(0) { };
  inline complex() : re(0), im(0) { };
  inline complex<R> &operator=(const complex<R> &y) = default;
  inline complex<R> &operator+=(const complex<R> &y)
  {
    return *this = *this + y;
  };
  inline complex<R> &operator-=(const complex<R> &y)
  {
    return *this = *this - y;
  };
  inline complex<R> &operator*=(const complex<R> &y)
  {
    return *this = *this * y;
  };
  inline complex<R> &operator/=(const complex<R> &y)
  {
    return *this = *this / y;
  };
};

template<typename R>
inline const R real(const complex<R> &x) { return x.re; }

template<typename R>
inline const R imag(const complex<R> &x) { return x.im; }

template<typename R>
inline const R norm(const complex<R> &x) { return x.re * x.re + x.im * x.im; }

template<typename R>
inline const R abs(const complex<R> &x) { return sqrt(norm(x)); }

template<typename R>
inline const R arg(const complex<R> &x) { return std::atan2(x.im, x.re); }

template<typename R>
inline complex<R> operator-(const complex<R> &x)
{
  return complex<R>(-x.re, -x.im);
}

template<typename R>
inline complex<R> operator-(const int &y, const complex<R> &x)
{
  return complex<R>(y - x.re, -x.im);
}

template<typename R>
inline complex<R> operator-(const complex<R> &x, const complex<R> &y)
{
  return complex<R>(x.re - y.re, x.im - y.im);
}

template<typename R>
inline complex<R> operator-(const complex<R> &x, const int &y)
{
  return complex<R>(x.re - y, x.im);
}

template<typename R>
inline complex<R> operator+(const complex<R> &x, const int &y)
{
  return complex<R>(x.re + y, x.im);
}

template<typename R>
inline complex<R> operator+(const complex<R> &x, const R &y)
{
  return complex<R>(x.re + y, x.im);
}

template<typename R>
inline complex<R> operator+(const complex<R> &x, const complex<R> &y)
{
  return complex<R>(x.re + y.re, x.im + y.im);
}

template<typename R>
inline complex<R> operator*(const int &x, const complex<R> &y)
{
  return complex<R>(x * y.re, x * y.im);
}

template<typename R>
inline complex<R> operator*(const R &x, const complex<R> &y)
{
  return complex<R>(x * y.re, x * y.im);
}

template<typename R>
inline complex<R> operator*(const complex<R> &y, const R &x)
{
  return complex<R>(x * y.re, x * y.im);
}

template<typename R>
inline complex<R> operator/(const complex<R> &y, const R &x)
{
  return complex<R>(y.re / x, y.im / x);
}

template<typename R>
inline complex<R> operator*(const complex<R> &x, const complex<R> &y)
{
  return complex<R>(x.re * y.re - x.im * y.im, x.re * y.im + x.im * y.re);
}

template<typename R>
inline R dot(const complex<R> &x, const complex<R> &y)
{
  return x.re * y.re + x.im * y.im;
}

template<typename R>
inline complex<R> operator/(const complex<R> &x, const complex<R> &y)
{
  return complex<R>(x.re * y.re + x.im * y.im, -x.re * y.im + x.im * y.re) / norm(y);
}

#endif
