// mandelbrot-perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#include <algorithm>
#include <climits>
#include <cmath>
#include <cstdbool>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <mpfr.h>

#include "edouble.cc"
#include "complex.hpp"

template< typename R >
struct z2c_bla_t {
  complex<R> A, B;
  R r;
  int l; // FIXME only store this for the last in each level
};

template< typename R >
struct z2c_orbit_t {
  int N;
  complex<R> *o;
  int L; // number of levels (ceil(log2(N))
  struct z2c_bla_t<R> **bla; // [L]
};

struct z2c_orbit {
  float_type tag;
  union {
    z2c_orbit_t<double> *d;
    z2c_orbit_t<long double> *l;
    z2c_orbit_t<edouble> *e;
  } u;
};

struct z2c_reference {
  mpfr_t v[6];
  int n;
};

struct z2c_reference *z2c_reference_new(const mpfr_t cx, const mpfr_t cy, const mpfr_t zx, const mpfr_t zy, int n) {
  struct z2c_reference *r = (struct z2c_reference *) malloc(sizeof(*r));
  if (! r) { return 0; }
  mpfr_prec_t p = std::max(std::max(mpfr_get_prec(cx), mpfr_get_prec(cy)), std::max(mpfr_get_prec(zx), mpfr_get_prec(zy)));
  for (int i = 0; i < 6; ++i) {
    mpfr_init2(r->v[i], p);
    mpfr_set_si(r->v[i], 0, MPFR_RNDN);
  };
  mpfr_set(r->v[0], cx, MPFR_RNDN);
  mpfr_set(r->v[1], cy, MPFR_RNDN);
  mpfr_set(r->v[2], zx, MPFR_RNDN);
  mpfr_set(r->v[3], zy, MPFR_RNDN);
  r->n = n;
  return r;
}

void z2c_reference_delete(struct z2c_reference *r) {
  for (int i = 0; i < 6; ++i) {
    mpfr_clear(r->v[i]);
  }
  free(r);
}

void z2c_reference_step(struct z2c_reference *r) {
  mpfr_sqr(r->v[4], r->v[2], MPFR_RNDN);
  mpfr_sqr(r->v[5], r->v[3], MPFR_RNDN);
  mpfr_sub(r->v[4], r->v[4], r->v[5], MPFR_RNDN);
  mpfr_mul(r->v[5], r->v[2], r->v[3], MPFR_RNDN);
  mpfr_mul_2si(r->v[5], r->v[5], 1, MPFR_RNDN);
  mpfr_add(r->v[2], r->v[4], r->v[0], MPFR_RNDN);
  mpfr_add(r->v[3], r->v[5], r->v[1], MPFR_RNDN);
  r->n += 1;
}

int z2c_reference_get_n(const struct z2c_reference *r) {
  return r->n;
}

void z2c_reference_get_zr(const struct z2c_reference *ref, mpfr_t zx, mpfr_t zy) {
  mpfr_set_prec(zx, mpfr_get_prec(ref->v[2]));
  mpfr_set_prec(zy, mpfr_get_prec(ref->v[3]));
  mpfr_set(zx, ref->v[2], MPFR_RNDN);
  mpfr_set(zy, ref->v[3], MPFR_RNDN);
}

template< typename R >
struct z2c_orbit_t<R> *z2c_orbit_t_new(int N)
{
  struct z2c_orbit_t<R> *o = (struct z2c_orbit_t<R> *) malloc(sizeof(*o));
  o->o = (complex<R> *) calloc(1, N * sizeof(complex<R>));
  o->N = N - 1;
  int count = 1;
  int m = o->N - 1;
  for ( ; m > 1; m = (m + 1) >> 1)
  {
    count++;
  }
  o->L = count;
  o->bla = (z2c_bla_t<R> **) calloc(1, sizeof(o->bla) * count);
  m = o->N - 1;
  for (int ix = 0; ix < count; ++ix, m = (m + 1) >> 1)
  {
    o->bla[ix] = (z2c_bla_t<R> *) malloc(sizeof(*o->bla[ix]) * m);
  }
  return o;
}

template< typename R>
void z2c_orbit_t_bla(z2c_orbit_t<R> *o, R c)
{
  const R epsilon = R(1e-6); // FIXME
  const complex<R> *Z = o->o;
  // one-step
  #pragma omp parallel for schedule(static, 65536)
  for (int m = 1; m < o->N; ++m)
  {
    const complex<R> A = R(2) * Z[m];
    const complex<R> B = R(1);
    const R xA = abs(A);
    const R r_nonlinear = epsilon * xA - c / xA;
    const R r = std::max(R(0), r_nonlinear);
    const int l = 1;
    z2c_bla_t<R> b1 = { A, B, r, l };
    o->bla[0][m - 1] = b1;
  }
  // merge
  int src = 0;
  for (int msrc = o->N - 1; msrc > 1; msrc = (msrc + 1) >> 1)
  {
    int dst = src + 1;
    int mdst = (msrc + 1) >> 1;
    #pragma omp parallel for schedule(static, 65536)
    for (int m = 0; m < mdst; ++m)
    {
      const int mx = m * 2;
      const int my = m * 2 + 1;
      if (my < msrc)
      {
        const z2c_bla_t<R> x = o->bla[src][mx];
        const z2c_bla_t<R> y = o->bla[src][my];
        const int l = x.l + y.l;
        const complex<R> A = y.A * x.A;
        const complex<R> B = y.A * x.B + y.B;
        const R xA = abs(x.A);
        const R xB = abs(x.B);
        const R r = std::min(x.r, std::max(R(0), (y.r - xB * c) / xA));
        z2c_bla_t<R> xy = { A, B, r, l };
        o->bla[dst][m] = xy;
      }
      else
      {
        o->bla[dst][m] = o->bla[src][mx];
      }
    }
    src++;
  }
}

struct z2c_orbit *z2c_orbit_new(const mpfr_t cx, const mpfr_t cy, const mpfr_t radius, int N, float_type tag) {
  struct z2c_orbit *o = (struct z2c_orbit *) malloc(sizeof(*o));
  mpfr_t zx, zy;
  mpfr_init2(zx, mpfr_get_prec(cx));
  mpfr_init2(zy, mpfr_get_prec(cy));
  mpfr_set_ui(zx, 0, MPFR_RNDN);
  mpfr_set_ui(zy, 0, MPFR_RNDN);
  z2c_reference *r = z2c_reference_new(cx, cy, zx, zy, 0);
  switch (tag) {
    case ft_double: {
      o->u.d = z2c_orbit_t_new<double>(N);
      int n;
      while ((n = z2c_reference_get_n(r)) < N)
      {
        z2c_reference_get_zr(r, zx, zy);
        o->u.d->o[n] = complex<double>(mpfr_get_d(zx, MPFR_RNDN), mpfr_get_d(zy, MPFR_RNDN));
        z2c_reference_step(r);
      }
      z2c_orbit_t_bla(o->u.d, mpfr_get_d(radius, MPFR_RNDN));
      break;
    }
    case ft_long_double: {
      o->u.l = z2c_orbit_t_new<long double>(N);
      int n;
      while ((n = z2c_reference_get_n(r)) < N)
      {
        z2c_reference_get_zr(r, zx, zy);
        o->u.l->o[n] = complex<long double>(mpfr_get_ld(zx, MPFR_RNDN), mpfr_get_ld(zy, MPFR_RNDN));
        z2c_reference_step(r);
      }
      z2c_orbit_t_bla(o->u.l, mpfr_get_ld(radius, MPFR_RNDN));
      break;
    }
    case ft_edouble: {
      o->u.e = z2c_orbit_t_new<edouble>(N);
      int n;
      while ((n = z2c_reference_get_n(r)) < N)
      {
        z2c_reference_get_zr(r, zx, zy);
        o->u.e->o[n] = complex<edouble>(edouble(zx), edouble(zy));
        z2c_reference_step(r);
      }
      edouble radius_(radius);
      z2c_orbit_t_bla(o->u.e, radius_);
      break;
    }
    default: assert(! "float type valid");
  }
  z2c_reference_delete(r);
  mpfr_clear(zx);
  mpfr_clear(zy);
  o->tag = tag;
  return o;
}

template< typename R >
void z2c_orbit_t_delete(struct z2c_orbit_t<R> *o) {
  for (int l = 0; l < o->L; ++l)
  {
    free(o->bla[l]);
  }
  free(o->bla);
  free(o->o);
  free(o);
}

void z2c_orbit_delete(struct z2c_orbit *o) {
  switch (o->tag) {
    case ft_double:      z2c_orbit_t_delete(o->u.d); break;
    case ft_long_double: z2c_orbit_t_delete(o->u.l); break;
    case ft_edouble:     z2c_orbit_t_delete(o->u.e); break;
    default: assert(! "float type valid");
  }
  free(o);
}

