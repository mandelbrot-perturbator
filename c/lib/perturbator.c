// mandelbrot-perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#include <cassert>
#include <cmath>
#include <cstdbool>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <atomic>

#include <pthread.h>

#include <mpc.h>

#include <mandelbrot-numerics.h>

#include "mandelbrot-perturbator.h"

#include "complex.hpp"
#include "edouble.cc"

static const double twopi = 6.283185307179586;

float mpfr_get(const mpfr_t &op, mpfr_rnd_t rnd, float dummy) {
  (void) dummy;
  return mpfr_get_flt(op, rnd);
}

double mpfr_get(const mpfr_t &op, mpfr_rnd_t rnd, double dummy) {
  (void) dummy;
  return mpfr_get_d(op, rnd);
}

long double mpfr_get(const mpfr_t &op, mpfr_rnd_t rnd, long double dummy) {
  (void) dummy;
  return mpfr_get_ld(op, rnd);
}

edouble mpfr_get(const mpfr_t &op, mpfr_rnd_t rnd, edouble dummy) {
  (void) dummy;
  (void) rnd;
  return edouble(op);
}

static inline bool isfinite_(double x)
{
  return !(std::isnan(x) || std::isinf(x));
}

static inline bool isfinite_(long double x)
{
  return !(std::isnan(x) || std::isinf(x));
}

template< typename R >
static inline bool isfinite(const complex< R > &z)
{
  return isfinite(real(z)) && isfinite(imag(z));
}

enum float_type
  { ft_float
  , ft_double
  , ft_long_double
  , ft_edouble
  };

enum render_method
  { rm_plain
  , rm_perturb
  };

template< typename R > float_type float_type_tag();
template<> float_type float_type_tag<double>() { return ft_double; }
template<> float_type float_type_tag<long double>() { return ft_long_double; }
template<> float_type float_type_tag<edouble>() { return ft_edouble; }

// don't use float, derivative overflows too easily...
#define EXP_THRESHOLD_PLAIN_FLOAT (-21)
#define EXP_THRESHOLD_PLAIN_DOUBLE (-50)
#define EXP_THRESHOLD_PLAIN_LONG_DOUBLE (sizeof(long double) == sizeof(double) ? EXP_THRESHOLD_PLAIN_DOUBLE : -60)
// these are half of the full range, to avoid issues with underflowing |z|^2
#define EXP_THRESHOLD_PERTURB_FLOAT (-120/2)
#define EXP_THRESHOLD_PERTURB_DOUBLE (-1016/2)
#define EXP_THRESHOLD_PERTURB_LONG_DOUBLE (sizeof(long double) == sizeof(double) ? EXP_THRESHOLD_PERTURB_DOUBLE : -16376/2)
#define EXP_THRESHOLD_PERTURB_EDOUBLE (-(1LL<<62LL)/2)

#include "z2c.c"

template <typename R>
struct reference;

struct perturbator {
  int workers;
  int width;
  int height;
  int detect_glitches;
  int correct_glitches;
  int approx_skip;
  int maxiters;
  double escape_radius;
  double glitch_threshold;
  int precision;
  int maxchunk;
  mpc_t center;
  mpfr_t radius;
  double escape_radius_2;
  double log_escape_radius_2;
  int newton_steps_root;
  int newton_steps_child;
  int order;
  int64_t threshold;
  int logging;

  pthread_mutex_t mutex;
  pthread_cond_t cond, cond_done;
  int nthreads;
  pthread_t *threads;

  int volatile active_workers;
  bool volatile running;

  int volatile queue_id;
  int volatile start_id;

  std::atomic<int> scanline;

  std::atomic<int> done_pixels;

  enum float_type ft;
  enum render_method rm;
  void *volatile refs;

  // [j * width + i]
  int32_t *output_dwell_n;
  float   *output_dwell_f;
  float   *output_dwell_a;
  float   *output_distance;
  int32_t *output_domain_n;
  float   *output_domain_x;
  float   *output_domain_y;
};

extern const int32_t *perturbator_get_dwell_n (struct perturbator *img) { return img->output_dwell_n; }
extern const float   *perturbator_get_dwell_f (struct perturbator *img) { return img->output_dwell_f; }
extern const float   *perturbator_get_dwell_a (struct perturbator *img) { return img->output_dwell_a; }
extern const float   *perturbator_get_distance(struct perturbator *img) { return img->output_distance; }
extern const int32_t *perturbator_get_domain_n(struct perturbator *img) { return img->output_domain_n; }
extern const float   *perturbator_get_domain_x(struct perturbator *img) { return img->output_domain_x; }
extern const float   *perturbator_get_domain_y(struct perturbator *img) { return img->output_domain_y; }

static bool image_running(struct perturbator *img) {
  return img->running;
}

extern int perturbator_active(struct perturbator *img) {
  pthread_mutex_lock(&img->mutex);
  bool active = img->active_workers;
  pthread_mutex_unlock(&img->mutex);
  return active;
}

#define LOG_VIEW  1
#define LOG_QUEUE 2
#define LOG_CACHE 4

#define image_log(img, aspect, ...) do{ \
  if ((img)->logging & (aspect)) { mpfr_fprintf(stderr, __VA_ARGS__); } \
  }while(0)

int mpfr_add(mpfr_t &rop, const mpfr_t &op1, float op2, mpfr_rnd_t rnd) {
  return mpfr_add_d(rop, op1, op2, rnd);
}

int mpfr_add(mpfr_t &rop, const mpfr_t &op1, double op2, mpfr_rnd_t rnd) {
  return mpfr_add_d(rop, op1, op2, rnd);
}

int mpfr_add(mpfr_t &rop, const mpfr_t &op1, long double op2, mpfr_rnd_t rnd) {
  mpfr_t tmp;
  mpfr_init2(tmp, 64);
  to_mpfr(op2, tmp);
  int r = mpfr_add(rop, op1, tmp, rnd);
  mpfr_clear(tmp);
  return r;
}

int mpfr_add(mpfr_t &rop, const mpfr_t &op1, edouble op2, mpfr_rnd_t rnd) {
  mpfr_t tmp;
  mpfr_init2(tmp, 53);
  to_mpfr(op2, tmp);
  int r = mpfr_add(rop, op1, tmp, rnd);
  mpfr_clear(tmp);
  return r;
}

template <typename R>
void to_mpc(const complex<R> &from, mpc_t &to) {
  to_mpfr(real(from), mpc_realref(to));
  to_mpfr(imag(from), mpc_imagref(to));
}

template <typename T>
int mpc_add(mpc_t &rop, const mpc_t &op1, const complex<T> &op2, mpc_rnd_t rnd) {
  (void) rnd;
          mpfr_add(mpc_realref(rop), mpc_realref(op1), real(op2), MPFR_RNDN);
  return  mpfr_add(mpc_imagref(rop), mpc_imagref(op1), imag(op2), MPFR_RNDN);
}

template <typename T>
int mpc_div(mpc_t &rop, const mpc_t &op1, const complex<T> &op2, mpc_rnd_t rnd) {
  mpc_t tmp;
  mpc_init2(tmp, 64);
  to_mpc(op2, tmp);
  int r = mpc_div(rop, op1, tmp, rnd);
  mpc_clear(tmp);
  return r;
}

template <typename T>
complex<T> mpc_get(const mpc_t &op, mpc_rnd_t rnd, T dummy) {
  (void) rnd;
  return complex<T>(mpfr_get(mpc_realref(op), MPFR_RNDN, dummy), mpfr_get(mpc_imagref(op), MPFR_RNDN, dummy));
}

template <typename R>
struct pixel {
  complex<R> c;
  complex<R> z;
  complex<R> dz;
  complex<R> mz;
  uint32_t iters;
};

template <typename R>
static int cmp_pixel_by_iters_asc_r(const void *a, const void *b, void *p) {
  const struct pixel<R> *q = (const struct pixel<R> *) p;
  const uint32_t *x = (const uint32_t *) a;
  const uint32_t *y = (const uint32_t *) b;
  if (q[*x].iters < q[*y].iters) { return -1; }
  if (q[*x].iters > q[*y].iters) { return  1; }
/*
  R x2 = norm(x->z);
  R y2 = norm(y->z);
  if (x2 < y2) { return -1; }
  if (x2 > y2) { return  1; }
*/
  return 0;
}

template <typename R>
struct reference {
  struct reference<R> *next;

  int queue_id;
  int start_id;

  int period;
  int iters;
  mpc_t c;
  mpc_t z;
};

template <typename R>
static void reference_release(struct reference<R> *ref) {
  if (ref) {
    mpc_clear(ref->c);
    mpc_clear(ref->z);
    free(ref);
  }
}


template <typename R>
int image_dequeue_plain(struct perturbator *img)
{
  pthread_mutex_lock(&img->mutex);
  img->active_workers -= 1;
  int j = img->scanline++;
  if (img->running && j < img->height) {
    img->active_workers += 1;
  }
  else
  {
    j = -1;
    if (! img->active_workers)
    {
      img->running = false;
    }
  }
  if (! img->running)
  {
    pthread_cond_broadcast(&img->cond_done);
  }
  pthread_mutex_unlock(&img->mutex);
  return j;
}

template <typename R>
static struct reference<R> *image_dequeue(struct perturbator *img, struct reference<R> *ref) {
  pthread_mutex_lock(&img->mutex);
//  assert(img->ft == FT);
  if (ref) {
    image_log(img, LOG_QUEUE, "%8d DONE   %8d\n", ref->start_id, ref->queue_id);
  }
  // release the old reference
  reference_release(ref);
  // if no workers are working and the queue is empty, it would be empty forever
  img->active_workers -= 1;
  while (img->running && ! img->refs) {
    if (! img->active_workers) {
      img->running = false;
      pthread_cond_broadcast(&img->cond);
    } else {
      pthread_cond_wait(&img->cond, &img->mutex);
    }
  }

  if (img->running && img->refs) {
    // still work to do
    ref = (struct reference<R> *) img->refs;
    img->refs = ref->next;
    ref->start_id = (img->start_id += 1);
    img->active_workers += 1;
    image_log(img, LOG_QUEUE, "%8d START  %8d%8d%8d\n", ref->start_id, ref->queue_id, ref->iters, ref->period);
  } else {
    ref = 0;
  }
  if (! img->running)
  {
    pthread_cond_broadcast(&img->cond_done);
  }
  pthread_mutex_unlock(&img->mutex);
  return ref;
}

template <typename R>
static void image_enqueue(struct perturbator *img, struct reference<R> *ref) {
  pthread_mutex_lock(&img->mutex);
//  assert(img->ft == FT);
  ref->queue_id = (img->queue_id += 1);
  image_log(img, LOG_QUEUE, "         QUEUE  %8d%8d%8d\n", ref->queue_id, ref->iters, ref->period);
  // link into reference queue (sorted by count descending)
  ref->next = (struct reference<R> *) img->refs;
  img->refs = ref;
  // wake waiting workers
  pthread_cond_broadcast(&img->cond);
  pthread_mutex_unlock(&img->mutex);
}

template <typename R>
static void *image_worker(void *arg);

template <typename R>
static void *image_worker_plain(void *arg);

template <typename R>
void perturbator_start_internal_plain(struct perturbator *img) {
  img->nthreads = img->workers;
  img->threads = (pthread_t *) calloc(1, img->nthreads * sizeof(*img->threads));
  img->running = true;
  img->scanline = 0;
  pthread_mutex_lock(&img->mutex);
  img->active_workers = img->nthreads;
  for (int i = 0; i < img->nthreads; ++i) {
    pthread_create(&img->threads[i], 0, image_worker_plain<R>, img);
  }
  pthread_mutex_unlock(&img->mutex);
}

template <typename R>
void perturbator_start_internal(struct perturbator *img) {

  // create reference
//  assert(img->ft == FT);
  struct reference<R> *ref = (struct reference<R> *) calloc(1, sizeof(*ref));
  mpc_init2(ref->c, img->precision);
  mpc_init2(ref->z, img->precision);
  mpc_set(ref->c, img->center, MPC_RNDNN);
  mpc_set_ui_ui(ref->z, 0, 0, MPC_RNDNN);
  ref->iters = 0;
  ref->period = 0;
  img->nthreads = 1;
  img->threads = (pthread_t *) calloc(1, img->nthreads * sizeof(*img->threads));
  img->running = true;
  image_enqueue(img, ref);
  pthread_mutex_lock(&img->mutex);
  img->active_workers = img->nthreads;
  for (int i = 0; i < img->nthreads; ++i) {
    pthread_create(&img->threads[i], 0, image_worker<R>, img);
  }
  pthread_mutex_unlock(&img->mutex);
}

template <typename R>
void release_refs(struct perturbator *img) {
//  assert(img->ft == FT);
  for (struct reference<R> *ref = (struct reference<R> *) img->refs; ref; ) {
    struct reference<R> *next = ref->next;
    reference_release(ref);
    ref = next;
  }
  img->refs = 0;
}

template<typename R>
void rebase_to_new_reference(int offset, int count, const uint32_t *ix, pixel<R> *px, complex<R> dc) {
  for (int k = 0; k < count; ++k) {
    px[ix[offset + k]].c += dc;
  }
}

float ldexp(float x, int64_t e) { return std::ldexp(x, e); } // FIXME e range
double ldexp(double x, int64_t e) { return std::ldexp(x, e); } // FIXME e range
long double ldexp(long double x, int64_t e) { return std::ldexp(x, e); } // FIXME e range

template <typename R>
static void *image_worker(void *threadarg) {
  struct perturbator *img = (struct perturbator *) threadarg;
  image_log(img, LOG_QUEUE, "         ENTER\n");

  pthread_mutex_lock(&img->mutex);

  int maxiters = img->maxiters;
  R er2 = img->escape_radius_2;
  R loger2 = img->log_escape_radius_2;
  mpc_t center;
  mpc_init2(center, mpc_get_prec(img->center));
  mpc_set(center, img->center, MPC_RNDNN);
  mpfr_t radius;
  mpfr_init2(radius, 53);
  mpfr_set(radius, img->radius, MPFR_RNDN);
  R pixel_spacing = mpfr_get(radius, MPFR_RNDN, R(0)) * R(2.0 / img->height);
  int width = img->width;
  int height = img->height;
  R vdiameter = R(2.0) * mpfr_get(img->radius, MPFR_RNDN, R(0));
  R hdiameter = img->width * vdiameter / img->height;
  pthread_mutex_unlock(&img->mutex);

  struct reference<R> *ref = 0;
  while ( (ref = image_dequeue(img, ref)) ) {

    mpc_set(ref->c, center, MPC_RNDNN);
    z2c_orbit *orbit = z2c_orbit_new(mpc_realref(ref->c), mpc_imagref(ref->c), radius, maxiters, float_type_tag<R>());

    const complex<R> *Z;
    const z2c_bla_t<R> **bla;
    int N = 0;
    int L = 0;
    switch (float_type_tag<R>())
    {
      case ft_double:      Z = (const complex<R> *) orbit->u.d->o; N = orbit->u.d->N; L = orbit->u.d->L; bla = (const z2c_bla_t<R> **) orbit->u.d->bla; break;
      case ft_long_double: Z = (const complex<R> *) orbit->u.l->o; N = orbit->u.l->N; L = orbit->u.l->L; bla = (const z2c_bla_t<R> **) orbit->u.l->bla; break;
      case ft_edouble:     Z = (const complex<R> *) orbit->u.e->o; N = orbit->u.e->N; L = orbit->u.e->L; bla = (const z2c_bla_t<R> **) orbit->u.e->bla; break;
      default: assert(! "float type valid");
    }

    #pragma omp parallel for schedule(static, 1)
    for (int jj = 0; jj < height; ++jj) {
      R y = R((jj + 0.5) / height - 0.5) * vdiameter;
      if (image_running(img)) {
        for (int ii = 0; ii < width; ++ii) {
          R x = R((ii + 0.5) / width - 0.5) * hdiameter;
          complex<R> c0(x, -y);
          int index = jj * width + ii;
          pixel<R> p;
          p.c = c0;
          p.z = 0;
          p.dz = 0;
          p.iters = 0;
          complex<R> dcdc = pixel_spacing;
          complex<R> dzdc = p.dz;
          complex<R> z = p.z;
          complex<R> c = p.c;
          const R two = R(2.0);

          int m = 0;
          for (int n = 0; n < maxiters; )
          {
            assert(m < N);
            const complex<R> Zz = Z[m] + z;
            const R Zz2 = norm(Zz);
            if (Zz2 > er2)
            {
              // escaped
              img->output_dwell_n [index] = n;
              img->output_dwell_f [index] = 1 - log2(log(to_ld(Zz2)) / to_ld(loger2)); // smooth iters
              img->output_dwell_a [index] = arg(complex<long double>(to_ld(real(Zz)), to_ld(imag(Zz)))) / twopi;
              img->output_distance[index] = sqrt(to_ld(Zz2)) * log(to_ld(Zz2)) / to_ld(sqrt(norm(dzdc))); // de
              img->done_pixels++;
              break;
            }
            if (Zz2 < norm(z) || m == N - 1)
            {
              // rebase
              z = Zz;
              m = 0;
            }
            const z2c_bla_t<R> *b = 0;
            if (1 <= m && m < N - 1)
            {
              R r = abs(z);
              int ix = m - 1;
              for (int level = 0; level < L; ++level)
              {
                int ixm = (ix << level) + 1;
                if (m == ixm && r < bla[level][ix].r)
                {
                  b = &bla[level][ix];
                }
                else
                {
                  break;
                }
                ix = ix >> 1;
              }
            }
            if (b && m + b->l < N)
            {
              dzdc = b->A * dzdc + b->B * dcdc;
              z = b->A * z + b->B * c;
              m += b->l;
              n += b->l;
            }
            else
            {
              dzdc = two * Zz * dzdc + dcdc;
              z = (two * Z[m] + z) * z + c;
              m += 1;
              n += 1;
            }

          } // for n
          if (! image_running(img)) { break; }
        } // for ii
      } // if running
    } // for jj
    z2c_orbit_delete(orbit);
  } // while ref

  image_log(img, LOG_QUEUE, "         END\n");
  return 0;
}


template <typename R>
static m_newton attractor_step(complex<R> &z, complex<R> z_guess, complex<R> c, int period, R epsilon2) {
  complex<R> zz = z_guess;
  complex<R> dzz = 1;
  for (int i = 0; i < period; ++i) {
    dzz = 2 * zz * dzz;
    zz = zz * zz + c;
  }
  if (norm(zz - z_guess) <= epsilon2) {
    z = z_guess;
    return m_converged;
  }
  complex<R> z_new = z_guess - (zz - z_guess) / (dzz - 1);
  complex<R>  d = z_new - z_guess;
  if (norm(d) <= epsilon2) {
    z = z_new;
    return m_converged;
  }
  if (isfinite(d)) {
    z = z_new;
    return m_stepped;
  } else {
    z = z_guess;
    return m_failed;
  }
}

template <typename R>
static m_newton attractor(complex<R> &z_out, complex<R> z_guess, complex<R> c, int period, int maxsteps, R epsilon2) {
  m_newton result = m_failed;
  complex<R> z = z_guess;
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = attractor_step(z, z, c, period, epsilon2))) {
      break;
    }
  }
  z_out = z;
  return result;
}

template <typename R>
static bool interior_de(R &de_out, complex<R> &dz_out, complex<R> z, complex<R> c, int p, int steps, R epsilon2) {
  complex<R> z00 = 0;
  if (m_failed != attractor(z00, z, c, p, steps, epsilon2)) {
    complex<R> z0 = z00;
    complex<R> dz0 = 1;
    for (int j = 0; j < p; ++j) {
      dz0 = 2 * z0 * dz0;
      z0 = z0 * z0 + c;
    }
    if (norm(dz0) <= 1) {
      complex<R> z1 = z00;
      complex<R> dz1 = 1;
      complex<R> dzdz1 = 0;
      complex<R> dc1 = 0;
      complex<R> dcdz1 = 0;
      for (int j = 0; j < p; ++j) {
        dcdz1 = 2 * (z1 * dcdz1 + dz1 * dc1);
        dc1 = 2 * z1 * dc1 + 1;
        dzdz1 = 2 * (dz1 * dz1 + z1 * dzdz1);
        dz1 = 2 * z1 * dz1;
        z1 = z1 * z1 + c;
      }
      de_out = (1 - norm(dz1)) / sqrt(norm(dcdz1 + dzdz1 * dc1 / (1 - dz1)));
      dz_out = dz1;
      return true;
    }
  }
  return false;
}

template <typename R>
struct partial
{
  complex<R> z;
  int p;
};

template <typename R>
static void *image_worker_plain(void *threadarg) {
  struct perturbator *img = (struct perturbator *) threadarg;
  image_log(img, LOG_QUEUE, "         ENTER\n");

  pthread_mutex_lock(&img->mutex);

  int width = img->width;
  int height = img->height;
  int maxiters = img->maxiters;
  int maxpartials = maxiters;
  R escape_radius_2 = img->escape_radius_2;
  R log_escape_radius_2 = img->log_escape_radius_2;
  complex<R> center = mpc_get(img->center, MPC_RNDNN, R(0));
  R radius = mpfr_get(img->radius, MPFR_RNDN, R(0));
  R pixel_spacing = radius * R(2.0 / height);
  R epsilon2 = std::nextafter(R(256), R(1.0/0.0)) - R(256);
  epsilon2 *= epsilon2;

  pthread_mutex_unlock(&img->mutex);

  struct partial<R> *partials = (struct partial<R> *)malloc(sizeof(*partials) * maxpartials);

  int j;
  while ( 0 <= (j = image_dequeue_plain<R>(img)) )
  {
    if (! image_running(img)) break;

    R y = ((j + R(0.5)) / height - R(0.5)) * R(2.0) * radius;

    int bias = 0;
    for (int i = 0; i < width; ++i)
    {
      if (! image_running(img)) break;

      int index = j * width + i;

      R x = ((i + R(0.5)) / width - R(0.5)) * R(2.0) * radius * R(width) / R(height);

      int npartials = 0;
      complex<R> dc(x, -y);
      complex<R> c(center + dc);
      complex<R> z(0, 0);
      complex<R> dz(0, 0);
      complex<R> mz(1.0/0.0, 0);
      R mi(R(1.0)/R(0.0));
      bool done = false;
      for (int k = 1; k < maxiters; ++k)
      {
        dz = R(2.0) * z * dz + R(1.0);
        z = z * z + c;
        R z2 = norm(z);
        if (z2 <= mi)
        {
          complex<R> a = z / mz;
          img->output_domain_n[index] = k;
          img->output_domain_x[index] = a.re;
          img->output_domain_y[index] = a.im;
          mi = z2;
          mz = z;
          if (bias >= 0)
          {
            // store partial
            if (partials && npartials < maxpartials)
            {
              partials[npartials].z = z;
              partials[npartials].p = k;
              npartials++;
            }
          }
          else
          {
            // do interior check
            complex<R> dz = 0;
            R de = -1;
            if (interior_de(de, dz, z, c, k, 64, epsilon2))
            {
              // output interior
              img->output_dwell_n [index] = -k; // period
              img->output_dwell_f [index] = sqrt(norm(dz)); // radius
              img->output_dwell_a [index] = arg(dz) / twopi; // angle
              img->output_distance[index] = -de / pixel_spacing; // de
              img->done_pixels++;
              done = true;
              bias = -1;
              break;
            }
          }
        }
        if (z2 > escape_radius_2)
        {
          // output exterior
          img->output_dwell_n [index] = k;
          img->output_dwell_f [index] = 1 - log2(log(z2) / log_escape_radius_2); // smooth iters
          img->output_dwell_a [index] = arg(z) / twopi;
          img->output_distance[index] = sqrt(z2) * log(z2) / sqrt(norm(dz * pixel_spacing)); // de
          img->done_pixels++;
          done = true;
          bias = 1;
          break;
        }
      }

      if (! image_running(img)) break;

      if (bias >= 0 && ! done)
      {
        // check stored partials
        for (int p = 0; p < npartials; ++p)
        {
          // do interior check
          z = partials[p].z;
          int k = partials[p].p;
          complex<R> dz = 0;
          R de = -1;
          if (interior_de(de, dz, z, c, k, 64, epsilon2))
          {
            // output interior
            img->output_dwell_n [index] = -k; // period
            img->output_dwell_f [index] = sqrt(norm(dz)); // radius
            img->output_dwell_a [index] = arg(dz) / twopi; // angle
            img->output_distance[index] = -de / pixel_spacing; // de
            img->done_pixels++;
            bias = -1;
            break;
          }
        }
      }

    } // for i
  } // while ref

  free(partials);
  image_log(img, LOG_QUEUE, "         END\n");
  return 0;
}


extern struct perturbator *perturbator_new(int workers, int width, int height, int maxiters, int maxchunk, double escape_radius, double glitch_threshold) {
  struct perturbator *img = (struct perturbator *) calloc(1, sizeof(*img));

  img->workers = workers;
  img->width = width;
  img->height = height;
  img->detect_glitches = 1;
  img->correct_glitches = 1;
  img->maxiters = maxiters;
  img->maxchunk = maxchunk;
  img->escape_radius = escape_radius;
  img->glitch_threshold = glitch_threshold;
  img->precision = 53;

  mpc_init2(img->center, 53);
  mpfr_init2(img->radius, 53);

  img->escape_radius_2 = escape_radius * escape_radius;
  img->log_escape_radius_2 = log(img->escape_radius_2);

  img->newton_steps_root = 64;
  img->newton_steps_child = 8;

  img->order = 64;
  img->threshold = 64;
  img->logging = LOG_VIEW | LOG_CACHE | LOG_QUEUE;

  img->output_dwell_n  = (int32_t *) calloc(1, width * height * sizeof(*img->output_dwell_n));
  img->output_dwell_f  = (float   *) calloc(1, width * height * sizeof(*img->output_dwell_f));
  img->output_dwell_a  = (float   *) calloc(1, width * height * sizeof(*img->output_dwell_a));
  img->output_distance = (float   *) calloc(1, width * height * sizeof(*img->output_distance));
  img->output_domain_n = (int32_t *) calloc(1, width * height * sizeof(*img->output_domain_n));
  img->output_domain_x = (float   *) calloc(1, width * height * sizeof(*img->output_domain_x));
  img->output_domain_y = (float   *) calloc(1, width * height * sizeof(*img->output_domain_y));

  pthread_mutex_init(&img->mutex, 0);
  pthread_cond_init(&img->cond, 0);
  pthread_cond_init(&img->cond_done, 0);

  image_log(img, LOG_QUEUE | LOG_CACHE, "sequence action   queued   iters  period    active count\n");
  image_log(img, LOG_QUEUE | LOG_CACHE, "-------- ------ -------- ------- ------- ---------------\n");
  return img;
}


extern void perturbator_start(struct perturbator *img, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius) {

  img->precision = std::max(53, int(53 - 2 * mpfr_get_exp(radius)));

  image_log(img, LOG_VIEW, "real=%Re\nimag=%Re\nradius=%Re\nprecision=%d\n", centerx, centery, radius, img->precision);

  mpc_set_prec(img->center, img->precision);
  mpc_set_fr_fr(img->center, centerx, centery, MPC_RNDNN);
  mpfr_set(img->radius, radius, MPFR_RNDN);

  memset(img->output_dwell_n,  0, img->width * img->height * sizeof(*img->output_dwell_n));
  memset(img->output_dwell_f,  0, img->width * img->height * sizeof(*img->output_dwell_f));
  memset(img->output_dwell_a,  0, img->width * img->height * sizeof(*img->output_dwell_a));
  memset(img->output_distance, 0, img->width * img->height * sizeof(*img->output_distance));
  memset(img->output_domain_n, 0, img->width * img->height * sizeof(*img->output_domain_n));
  memset(img->output_domain_x, 0, img->width * img->height * sizeof(*img->output_domain_x));
  memset(img->output_domain_y, 0, img->width * img->height * sizeof(*img->output_domain_y));
  img->done_pixels = 0;

  mpfr_t pixel_spacing;
  mpfr_init2(pixel_spacing, 53);
  mpfr_set(pixel_spacing, img->radius, MPFR_RNDN);
  mpfr_mul_d(pixel_spacing, pixel_spacing, 2.0 / img->height, MPFR_RNDN);
  int64_t e = mpfr_get_exp(pixel_spacing);
  mpfr_clear(pixel_spacing);

  img->rm =
    e >= EXP_THRESHOLD_PLAIN_LONG_DOUBLE ? rm_plain : rm_perturb;
  switch (img->rm)
  {
    case rm_plain:
      img->ft =
        e >= EXP_THRESHOLD_PLAIN_DOUBLE ? ft_double :
        ft_long_double;
      switch (img->ft)
      {
        case ft_double:
          image_log(img, LOG_VIEW, "         TYPE     f53e11          double plain\n");
          perturbator_start_internal_plain<double>(img);
          return;
        case ft_long_double:
          image_log(img, LOG_VIEW, "         TYPE     f64e15     long double plain\n");
          perturbator_start_internal_plain<long double>(img);
          return;
      }
      break;
    case rm_perturb:
      img->ft =
        e >= EXP_THRESHOLD_PERTURB_DOUBLE ? ft_double :
        e >= EXP_THRESHOLD_PERTURB_LONG_DOUBLE ? ft_long_double :
        ft_edouble;
      switch (img->ft)
      {
        case ft_double:
          image_log(img, LOG_VIEW, "         TYPE     f53e11          double perturb\n");
          perturbator_start_internal<double>(img);
          return;
        case ft_long_double:
          image_log(img, LOG_VIEW, "         TYPE     f64e15     long double perturb\n");
          perturbator_start_internal<long double>(img);
          return;
        case ft_edouble:
          image_log(img, LOG_VIEW, "         TYPE     f53e64         edouble perturb\n");
          perturbator_start_internal<edouble>(img);
          return;
      }
      break;
  }
  assert(! "valid float type");
}


extern void perturbator_stop(struct perturbator *img, int force) {
  if (force) {
    img->running = false;
  }
  pthread_mutex_lock(&img->mutex);
  pthread_cond_broadcast(&img->cond);
  pthread_mutex_unlock(&img->mutex);
  for (int i = 0; i < img->nthreads; ++i) {
    pthread_join(img->threads[i], 0);
  }
  free(img->threads);
  img->threads = 0;
  switch (img->rm)
  {
    case rm_plain:
      break;
    case rm_perturb:
      switch (img->ft) {
        case ft_float:        release_refs<float>(img); return;
        case ft_double:       release_refs<double>(img); return;
        case ft_long_double:  release_refs<long double>(img); return;
        case ft_edouble:      release_refs<edouble>(img); return;
      }
      assert(! "valid float type");
      break;
  }
}

extern bool perturbator_wait(struct perturbator *img, const struct timespec *ts)
{
  int timed_out = 0;
  pthread_mutex_lock(&img->mutex);
  while (timed_out == 0 && img->running)
  {
    timed_out = pthread_cond_timedwait(&img->cond_done, &img->mutex, ts);
  }
  bool active = img->running;
  pthread_mutex_unlock(&img->mutex);
  return active;
}

void perturbator_set_maximum_iterations(struct perturbator *img, int maxiters) {
  img->maxiters = maxiters;
}

double perturbator_get_progress(struct perturbator *img)
{
  return img->done_pixels / (double) (img->width * img->height);
}

int perturbator_get_width(struct perturbator *img)
{
  return img->width;
}

int perturbator_get_height(struct perturbator *img)
{
  return img->height;
}
