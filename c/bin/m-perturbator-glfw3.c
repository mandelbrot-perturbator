// perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#define _POSIX_C_SOURCE 199309L

#include <math.h>
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <mpfr.h>
#include <mpc.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <mandelbrot-numerics.h>
#include <mandelbrot-perturbator.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/threading.h>
#endif


static inline int max(int a, int b) {
  return a > b ? a : b;
}

static int envi(const char *name, int def) {
  const char *e = getenv(name);
  if (e) {
    return atoi(e);
  } else {
    return def;
  }
}

static double envd(const char *name, double def) {
  const char *e = getenv(name);
  if (e) {
    return atof(e);
  } else {
    return def;
  }
}

static int envr(mpfr_t out, const char *name, const char *def) {
  const char *e = getenv(name);
  if (e) {
    return mpfr_set_str(out, e,   10, MPFR_RNDN);
  } else {
    return mpfr_set_str(out, def, 10, MPFR_RNDN);
  }
}

#ifdef __EMSCRIPTEN__

static const char *blit_vert =
  "uniform vec4 bounds;\n"
  "attribute highp float vertexID;\n"
  "varying highp vec2 texCoord;\n"
  "void main() {\n"
  "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = bounds.xy; } else\n"
  "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = bounds.zy; } else\n"
  "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = bounds.xw; } else\n"
  "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = bounds.zw; }\n"
  "}\n"
  ;

static const char *blit_frag =
  "uniform sampler2D tex;\n"
  "varying highp vec2 texCoord;\n"
  "void main() {\n"
  "  gl_FragColor = texture2D(tex, texCoord);\n"
  "}\n"
  ;

static const char *simple_vert =
  "attribute highp float vertexID;\n"
  "varying highp vec2 texCoord;\n"
  "void main() {\n"
  "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = vec2(0.0, 1.0); } else\n"
  "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = vec2(1.0, 1.0); } else\n"
  "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = vec2(0.0, 0.0); } else\n"
  "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = vec2(1.0, 0.0); }\n"
  "}\n"
  ;

static const char *simple_frag =
  "#extension GL_OES_standard_derivatives : enable\n"
  "uniform sampler2D tex;\n"
  "uniform bool show_lines;\n"
  "uniform highp float weight;\n"
  "varying highp vec2 texCoord;\n"
  "int px(vec2 tc) {\n"
  "  highp vec4 t = texture2D(tex, tc);\n"
  "  int k = 0;\n"
  "  k += (t.x / 2.0) - floor(t.x / 2.0) >= 0.5 ? 1 : 2;\n"
  "  k += t.z >= 0.0 ? 4 : 8;\n"
  "  k += 16 * int(floor(t.x));\n"
  "  return k;\n"
  "}\n"
  "highp float tanh_(highp float y) {\n"
  "  highp float x = clamp(y, -3.0, 3.0);\n"
  "  return x * (27.0 + x * x) / (27.0 + 9.0 * x * x);\n"
  "}\n"
  "void main() {\n"
  "  highp float e = 1.0;\n"
  "  highp vec2 dx = dFdx(texCoord);\n"
  "  highp vec2 dy = dFdy(texCoord);\n"
  "  if (show_lines) {\n"
  "    e = px(texCoord) == px(texCoord + dx + dy) && px(texCoord + dx) == px(texCoord + dy) ? 1.0 : 0.0;\n"
  "  }\n"
  "  highp vec2 me = texture2D(tex, texCoord).xy;\n"
  "  highp float s = tanh_(clamp(texture2D(tex, texCoord).w / weight, 0.0, 8.0));\n"
  "  highp float unescaped = dot(me, me);\n"
  "  bool glitched = me.x >= 0.0 && me.y < 0.0;\n"
  "  gl_FragColor = vec4(glitched ? vec3(1.0,0.0,0.0) : unescaped <= 0.0 ? vec3(1.0, 0.7, 0.0) : vec3(mix(0.0, mix(0.9, 1.0, e), s)), unescaped == 0.0 ? 0.0 : 1.0);\n"
  "}\n"
  ;

#else

static const char *blit_vert =
  "#version 130\n"
  "uniform vec4 bounds;\n"
  "in float vertexID;\n"
  "out vec2 texCoord;\n"
  "void main() {\n"
  "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = bounds.xy; } else\n"
  "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = bounds.zy; } else\n"
  "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = bounds.xw; } else\n"
  "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = bounds.zw; }\n"
  "}\n"
  ;

static const char *blit_frag =
  "#version 130\n"
  "uniform sampler2D tex;\n"
  "in vec2 texCoord;\n"
  "out vec4 fragColor;\n"
  "void main() {\n"
  "  fragColor = texture(tex, texCoord);\n"
  "}\n"
  ;

static const char *simple_vert =
  "#version 130\n"
  "in float vertexID;\n"
  "out vec2 texCoord;\n"
  "void main() {\n"
  "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = vec2(0.0, 1.0); } else\n"
  "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = vec2(1.0, 1.0); } else\n"
  "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = vec2(0.0, 0.0); } else\n"
  "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = vec2(1.0, 0.0); }\n"
  "}\n"
  ;

static const char *simple_frag =
  "#version 130\n"
  "uniform isampler2D tex_x;\n"
  "uniform  sampler2D tex_y;\n"
  "uniform  sampler2D tex_z;\n"
  "uniform  sampler2D tex_w;\n"
  "uniform bool show_lines;\n"
  "uniform float weight;\n"
  "in vec2 texCoord;\n"
  "out vec4 fragColor;\n"
  "int px(vec2 tc) {\n"
  "  int n = texture(tex_x, tc).x;\n"
  "  int k = 0;\n"
  "  k += (n % 2) >= 1 ? 1 : 2;\n"
  "  k += (texture(tex_z, tc).x) >= 0 ? 4 : 8;\n"
  "  k += 16 * n;\n"
  "  return k;\n"
  "}\n"
  "void main() {\n"
  "  float e = 1.0;\n"
  "  vec2 dx = dFdx(texCoord);\n"
  "  vec2 dy = dFdy(texCoord);\n"
  "  if (show_lines) {\n"
  "    e = px(texCoord) == px(texCoord + dx + dy) && px(texCoord + dx) == px(texCoord + dy) ? 1.0 : 0.0;\n"
  "  }\n"
  "  int me = texture(tex_x, texCoord).x;\n"
  "  float mef = texture(tex_y, texCoord).x;\n"
  "  float s = tanh(clamp(texture(tex_w, texCoord).x / weight, 0.0, 8.0));\n"
  "  float unescaped = float(me * me) + mef * mef;\n"
  "  bool glitched = me >= 0 && mef < 0.0;\n"
  "  float g = -mef;\n"
  "  fragColor = vec4(glitched ? tanh(clamp(vec3(g,g*0.1,g*0.01), vec3(0.0), vec3(4.0))) : unescaped <= 0.0 ? vec3(1.0, 0.7, 0.0) : vec3(mix(0.0, mix(0.9, 1.0, e), s)), unescaped == 0.0 ? 0.0 : 1.0);\n"
  "}\n"
  ;

#endif

struct state_s {
  GLFWwindow *window;
  struct perturbator *context;

  bool should_restart;
  bool should_redraw;
  bool should_save_now;
  bool should_save_when_done;
  bool should_view_morph;

  GLuint fbo;

  GLuint blit_vao;
  GLuint blit_p;
  GLint bounds_u;

  GLuint colourize_vao;
  GLuint colourize_p;
  bool show_lines;
  GLint show_lines_u;
  double log2weight;
  GLint weight_u;

  int width;
  int height;

  int precision;
  mpfr_t centerx;
  mpfr_t centery;
  mpfr_t radius;
  int maxiters;

  double srcx0;
  double srcy0;
  double srcx1;
  double srcy1;

  uint8_t *ppm;
};
typedef struct state_s state_t;
state_t state_d;

static void refresh_callback(void *user_pointer);

#ifdef __EMSCRIPTEN__
void go(state_t *state);
void refresh_callback(void *user_pointer);
#endif

static void button_handler(GLFWwindow *window, int button, int action, int mods) {
  state_t *state = glfwGetWindowUserPointer(window);
  if (action == GLFW_PRESS) {
    double srcx0 = 0, srcy0 = 0, srcx1 = state->width, srcy1 = state->height;
    double x = 0, y = 0;
    glfwGetCursorPos(window, &x, &y);
    mpfr_t cx, cy, r;
    mpfr_init2(cx, state->precision);
    mpfr_init2(cy, state->precision);
    mpfr_init2(r, 53);
    mpfr_set(cx, state->centerx, MPFR_RNDN);
    mpfr_set(cy, state->centery, MPFR_RNDN);
    mpfr_set(r, state->radius, MPFR_RNDN);
    double w = state->width;
    double h = state->height;
    double yy = h - y;
    double dx = 2 * ((x + 0.5) / w - 0.5) * (w / h);
    double dy = 2 * (0.5 - (y + 0.5) / h);
    mpfr_t ddx, ddy;
    mpfr_init2(ddx, 53);
    mpfr_init2(ddy, 53);
    mpfr_mul_d(ddx, r, dx, MPFR_RNDN);
    mpfr_mul_d(ddy, r, dy, MPFR_RNDN);
    switch (button) {
      case GLFW_MOUSE_BUTTON_LEFT:
        mpfr_mul_2si(ddx, ddx, -1, MPFR_RNDN);
        mpfr_mul_2si(ddy, ddy, -1, MPFR_RNDN);
        mpfr_add(cx, cx, ddx, MPFR_RNDN);
        mpfr_add(cy, cy, ddy, MPFR_RNDN);
        mpfr_mul_2si(r, r, -1, MPFR_RNDN);
        state->precision += 1;
        mpfr_set_prec(state->centerx, state->precision);
        mpfr_set_prec(state->centery, state->precision);
        mpfr_set(state->centerx, cx, MPFR_RNDN);
        mpfr_set(state->centery, cy, MPFR_RNDN);
        mpfr_set(state->radius, r, MPFR_RNDN);
        state->should_restart = true;
        srcx0 = 0.5 * (srcx0 - x) + x;
        srcx1 = 0.5 * (srcx1 - x) + x;
        srcy0 = 0.5 * (srcy0 - yy) + yy;
        srcy1 = 0.5 * (srcy1 - yy) + yy;
        break;
      case GLFW_MOUSE_BUTTON_RIGHT:
        mpfr_sub(cx, cx, ddx, MPFR_RNDN);
        mpfr_sub(cy, cy, ddy, MPFR_RNDN);
        mpfr_mul_2si(r, r, 1, MPFR_RNDN);
        state->precision -= 1;
        mpfr_set_prec(state->centerx, state->precision);
        mpfr_set_prec(state->centery, state->precision);
        mpfr_set(state->centerx, cx, MPFR_RNDN);
        mpfr_set(state->centery, cy, MPFR_RNDN);
        mpfr_set(state->radius, r, MPFR_RNDN);
        state->should_restart = true;
        srcx0 = 2.0 * (srcx0 - x) + x;
        srcx1 = 2.0 * (srcx1 - x) + x;
        srcy0 = 2.0 * (srcy0 - yy) + yy;
        srcy1 = 2.0 * (srcy1 - yy) + yy;
        break;
      case GLFW_MOUSE_BUTTON_MIDDLE:
        mpfr_add(cx, cx, ddx, MPFR_RNDN);
        mpfr_add(cy, cy, ddy, MPFR_RNDN);
        mpfr_set(state->centerx, cx, MPFR_RNDN);
        mpfr_set(state->centery, cy, MPFR_RNDN);
        state->should_restart = true;
        srcx0 -= state->width / 2 - x;
        srcx1 -= state->width / 2 - x;
        srcy0 -= state->height / 2 - yy;
        srcy1 -= state->height / 2 - yy;
        break;
    }
    mpfr_clear(cx);
    mpfr_clear(cy);
    mpfr_clear(r);
    mpfr_clear(ddx);
    mpfr_clear(ddy);
#ifdef __EMSCRIPTEN__
    glBindFramebuffer(GL_FRAMEBUFFER, state->fbo);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
#else
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, state->fbo);
    glBlitFramebuffer(0, 0, state->width, state->height, 0, 0, state->width, state->height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
#endif

    state->srcx0 = srcx0;
    state->srcy0 = srcy0;
    state->srcx1 = srcx1;
    state->srcy1 = srcy1;
  }
#ifdef __EMSCRIPTEN__
  if (state->should_restart)
  {
    state->should_restart = false;
    go(state);
  }
#endif
(void) mods;
}

static void key_handler(GLFWwindow *window, int key, int scancode, int action, int mods) {
  state_t *state = glfwGetWindowUserPointer(window);
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q:
      case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GL_TRUE);
        break;
      case GLFW_KEY_J:
        state->should_view_morph = true;
        break;
      case GLFW_KEY_L:
        state->show_lines = ! state->show_lines;
        state->should_redraw = true;
        break;
      case GLFW_KEY_C:
        glBindFramebuffer(GL_FRAMEBUFFER, state->fbo);
        glClear(GL_COLOR_BUFFER_BIT);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        state->should_redraw = true;
        break;
      case GLFW_KEY_S:
        if (mods & GLFW_MOD_SHIFT) {
          state->should_save_now = true;
        } else {
          state->should_save_when_done = true;
        }
        break;
      case GLFW_KEY_9:
        state->log2weight -= 0.25;
        state->should_redraw = true;
        break;
      case GLFW_KEY_0:
        state->log2weight += 0.25;
        state->should_redraw = true;
        break;
    }
  }
#ifdef __EMSCRIPTEN__
  if (state->should_restart)
  {
    state->should_restart = false;
    go(state);
  }
  if (state->should_redraw)
  {
    state->should_redraw = false;
    refresh_callback(state);
  }
#endif
(void) scancode;
}

static void refresh_callback(void *user_pointer) {
  state_t *state = user_pointer;
  assert(state);
  glBindVertexArray(state->blit_vao);
  glUseProgram(state->blit_p);
  glUniform4f(state->bounds_u, state->srcx0 / state->width, state->srcy0 / state->height, state->srcx1 / state->width, state->srcy1 / state->height);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(state->colourize_vao);
  glUseProgram(state->colourize_p);
  glActiveTexture(GL_TEXTURE0); glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, state->width, state->height, GL_RED, GL_INT, perturbator_get_dwell_n(state->context));
  glActiveTexture(GL_TEXTURE1); glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, state->width, state->height, GL_RED, GL_FLOAT, perturbator_get_dwell_f(state->context));
  glActiveTexture(GL_TEXTURE2); glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, state->width, state->height, GL_RED, GL_FLOAT, perturbator_get_dwell_a(state->context));
  glActiveTexture(GL_TEXTURE3); glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, state->width, state->height, GL_RED, GL_FLOAT, perturbator_get_distance(state->context));
  glUniform1i(state->show_lines_u, state->show_lines);
  glUniform1f(state->weight_u, exp2f(state->log2weight));
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static void handle_view_morph(struct perturbator *context, state_t *state) {
  if (state->should_view_morph) {
    state->should_view_morph = false;
#if 0
    mpc_t nucleus, size;
    mpc_init2(nucleus, 53);
    mpc_init2(size, 53);
    int period = perturbator_get_primary_reference(context, mpc_realref(nucleus), mpc_imagref(nucleus));
    m_r_size(size, nucleus, period);
    mpfr_t r, r2;
    mpfr_init2(r, 53);
    mpfr_init2(r2, 53);
    mpc_abs(r, size, MPFR_RNDN);
    mpfr_sqrt(r2, r, MPFR_RNDN);
    mpfr_mul(r, r, r2, MPFR_RNDN);
    mpfr_sqrt(r, r, MPFR_RNDN);
    mpfr_mul_2si(r, r, 3, MPFR_RNDN);
/*
    mpfr_set(r, state->radius, MPFR_RNDN);
    int e = mpfr_get_exp(r);
    mpfr_mul_2si(r, r, e / 2, MPFR_RNDN);
*/
    int p = max(53, 53 - 2 * mpfr_get_exp(r));
    state->precision = p;
    mpfr_set_prec(state->centerx, p);
    mpfr_set_prec(state->centery, p);
    mpfr_set(state->centerx, mpc_realref(nucleus), MPFR_RNDN);
    mpfr_set(state->centery, mpc_imagref(nucleus), MPFR_RNDN);
    mpfr_set(state->radius, r, MPFR_RNDN);
    state->should_restart = true;
    mpc_clear(nucleus);
    mpc_clear(size);
    mpfr_clear(r2);
    mpfr_clear(r);
#endif
  }
}

void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, NULL, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL shader program info log\n", name);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_GEOMETRY_SHADER: tname = "geometry"; break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, NULL, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL %s shader info log\n", name, tname);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, NULL);
  glCompileShader(shader);
  debug_shader(shader, type, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

GLint compile_program(const char *name, const GLchar *vert, const GLchar *frag) {
  GLint program = glCreateProgram();
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  glBindAttribLocation(program, 0, "vertexID");
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}

#ifdef __EMSCRIPTEN__

void go(state_t *state)
{
  size_t bytes = state->precision * 3 + 100; // FIXME
  char *script = malloc(bytes);
  mpfr_snprintf(script, bytes, "go('%Re', '%Re', '%Re', '%d');", state->centerx, state->centery, state->radius, state->maxiters);
  emscripten_run_script(script);
  free(script);
}

void main1(void)
{
  state_t *state = &state_d;
  glfwPollEvents();
  refresh_callback(state);
  glfwSwapBuffers(state->window);
}

EMSCRIPTEN_KEEPALIVE int set(const char *cx, const char *cy, const char *r)
{
  static bool first = true;
  state_t *state = &state_d;
  if (! first)
  {
    perturbator_stop(state->context, true);
    first = false;
  }
  mpfr_set_str(state->radius, r, 10, MPFR_RNDN);
  state->precision = max(53, 53 - mpfr_get_exp(state->radius));
  mpfr_set_prec(state->centerx, state->precision);
  mpfr_set_prec(state->centery, state->precision);
  mpfr_set_str(state->centerx, cx, 10, MPFR_RNDN);
  mpfr_set_str(state->centery, cy, 10, MPFR_RNDN);
  perturbator_start(state->context, state->centerx, state->centery, state->radius);
  return 0;
}

#endif


extern int main(int argc, char **argv) {
  state_t *state = &state_d;
  memset(state, 0, sizeof(*state));

#ifdef __EMSCRIPTEN__
  int workers = envi("threads", emscripten_num_logical_cores());
#else
  int workers = envi("threads", 4);
#endif
  int width = envi("width", 640);
  int height = envi("height", 360);
  int detect_glitches = envi("detect_glitches", 1);
  int approx_skip = envi("approx_skip", 0);
  int maxiters = envi("maxiters", 1 << 20);
  int chunk = envi("chunk", 1 << 8);
  double escape_radius = envd("escaperadius", 25);
  double glitch_threshold = envd("glitchthreshold", 1e-6);
  int precision = envi("precision", 53);
  int zoom_start = envi("zoom_start", 0);
  int zoom_count = envi("zoom_count", 0);
  double weight = envd("weight", 0);

  mpfr_init2(state->radius, 53);
  envr(state->radius, "radius", "2.0");

  int e = max(53, 53 - mpfr_get_exp(state->radius));
  if (e > precision) {
    fprintf(stderr, "WARNING: increasing precision to %d\n", e);
    precision = e;
  }

  mpfr_init2(state->centerx, precision);
  mpfr_init2(state->centery, precision);
  envr(state->centerx, "real", "-0.75");
  envr(state->centery, "imag", "0.0");

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  state->window = glfwCreateWindow(width, height, "perturbator", 0, 0);
  glfwMakeContextCurrent(state->window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew

#ifdef __EMSCRIPTEN__
  {
    EMSCRIPTEN_WEBGL_CONTEXT_HANDLE gl = emscripten_webgl_get_current_context();
    EM_BOOL have_gl_extension_OES_texture_float = emscripten_webgl_enable_extension(gl, "OES_texture_float");
    EM_BOOL have_gl_extension_OES_standard_derivatives = emscripten_webgl_enable_extension(gl, "OES_standard_derivatives");
    assert(have_gl_extension_OES_texture_float);
    assert(have_gl_extension_OES_standard_derivatives);
  }
#endif

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glClearColor(1.0, 0.7, 0.0, 1.0);
  glDisable(GL_DEPTH_TEST);

  GLuint ftex;
  glGenTextures(1, &ftex);
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, ftex);
//  printf("%dx%d GL_RGBA\n", width, height);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glGenFramebuffers(1, &state->fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, state->fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ftex, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  GLuint tex[4];
  glGenTextures(4, tex);
  for (int t = 0; t < 4; ++t)
  {
    glActiveTexture(GL_TEXTURE0 + t);
    glBindTexture(GL_TEXTURE_2D, tex[t]);
#ifdef __EMSCRIPTEN__
    glTexImage2D(GL_TEXTURE_2D, 0, t == 0 ? GL_R32I : GL_R32F, width, height, 0, GL_RED, t == 0 ? GL_INT : GL_FLOAT, 0); // FIXME
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
#else
    glTexImage2D(GL_TEXTURE_2D, 0, t == 0 ? GL_R32I : GL_R32F, width, height, 0, GL_RED, t == 0 ? GL_INT : GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
#endif
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }

  GLuint vbo;
  GLubyte vbo_data[4] = { 0, 1, 2, 3 };
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vbo_data), vbo_data, GL_STATIC_DRAW);

  glGenVertexArrays(1, &state->blit_vao);
  glBindVertexArray(state->blit_vao);
  state->blit_p = compile_program("blit", blit_vert, blit_frag);
  glUseProgram(state->blit_p);
  glUniform1i(glGetUniformLocation(state->blit_p, "tex"), 1);
  state->bounds_u = glGetUniformLocation(state->blit_p, "bounds");
  GLint attr = glGetAttribLocation(state->blit_p, "vertexID");
  glVertexAttribPointer(attr, 1, GL_UNSIGNED_BYTE, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(attr);
  glBindVertexArray(0);

  glGenVertexArrays(1, &state->colourize_vao);
  glBindVertexArray(state->colourize_vao);
  state->colourize_p = compile_program("colourize", simple_vert, simple_frag);
  glUseProgram(state->colourize_p);
  state->show_lines_u = glGetUniformLocation(state->colourize_p, "show_lines");
  state->weight_u = glGetUniformLocation(state->colourize_p, "weight");
  attr = glGetAttribLocation(state->colourize_p, "vertexID");
  glVertexAttribPointer(attr, 1, GL_UNSIGNED_BYTE, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(attr);

  state->ppm = malloc(width * height * 3);
  assert(state->ppm);

  state->should_restart = false;
  state->should_redraw = false;
  state->should_save_now = false;
  state->should_save_when_done = false;
  state->width = width;
  state->height = height;
  state->precision = precision;
  state->log2weight = weight;
  state->maxiters = maxiters;
  state->srcx0 = 0;
  state->srcy0 = 0;
  state->srcx1 = state->width;
  state->srcy1 = state->height;

  state->context = perturbator_new(workers, width, height, maxiters, chunk, escape_radius, glitch_threshold);
#if 0
  perturbator_set_detect_glitches(state->context, detect_glitches);
  perturbator_set_approx_skip(state->context, approx_skip);
#endif

  glfwSetWindowUserPointer(state->window, state);

#ifdef __EMSCRIPTEN__

  glfwSetMouseButtonCallback(state->window, button_handler);
  glfwSetKeyCallback(state->window, key_handler);
  perturbator_start(state->context, state->centerx, state->centery, state->radius);
  emscripten_set_main_loop(main1, 0, 1);

#else

  if (zoom_count) {
    if (zoom_count > 1)
    {
      mpfr_set_d(state->radius, 256, MPFR_RNDN);
      mpfr_div_2exp(state->radius, state->radius, zoom_start, MPFR_RNDN);
    }
    for (int z = zoom_start; z < zoom_count; ++z) {
      glfwPollEvents();
      if (glfwWindowShouldClose(state->window)) {
        break;
      }
      fprintf(stderr, "%8d FRAME\n", z);
      perturbator_start(state->context, state->centerx, state->centery, state->radius);
      perturbator_stop(state->context, false);
      refresh_callback(state);
      glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, state->ppm);
      printf("P6\n%d %d\n255\n", width, height);
      for (int y = height - 1; y >= 0; --y) {
        fwrite(state->ppm + y * width * 3, width * 3, 1, stdout);
      }
      fflush(stdout);
      mpfr_div_2exp(state->radius, state->radius, 1, MPFR_RNDN);
      glfwSwapBuffers(state->window);
    }

  } else {
    glfwSetMouseButtonCallback(state->window, button_handler);
    glfwSetKeyCallback(state->window, key_handler);

    bool first = true;
    while (! glfwWindowShouldClose(state->window)) {
      int e = glGetError();
      if (e)
        fprintf(stderr, "E: %d\n", e);

      state->should_restart = false;
  //    fprintf(stderr, "start\n");
      if (! first) {
        perturbator_stop(state->context, true);
      }
      first = false;
      perturbator_start(state->context, state->centerx, state->centery, state->radius);

  //    fprintf(stderr, "wait_timeout\n");
      while (perturbator_active(state->context)) {
        struct timespec delta = { 0, 33333333 };
        nanosleep(&delta, 0);
  //      fprintf(stderr, "refresh\n");
        refresh_callback(state);
        glfwSwapBuffers(state->window);

        glfwPollEvents();
        if (glfwWindowShouldClose(state->window)) {
          break;
        }

        if (state->should_save_now) {
          state->should_save_now = false;
          glReadPixels(0, 0, state->width, state->height, GL_RGB, GL_UNSIGNED_BYTE, state->ppm);
          printf("P6\n%d %d\n255\n", state->width, state->height);
          fwrite(state->ppm, state->width * state->height * 3, 1, stdout);
          fflush(stdout);
        }

        handle_view_morph(state->context, state);

        if (state->should_restart) {
          state->should_restart = false;
  //        fprintf(stderr, "start\n");
          perturbator_stop(state->context, true);
          perturbator_start(state->context, state->centerx, state->centery, state->radius);
        }
  //      fprintf(stderr, "wait_timeout\n");
      }

      if (glfwWindowShouldClose(state->window)) {
        break;
      }

      refresh_callback(state);
      glfwSwapBuffers(state->window);

      while (! state->should_restart) {

        if (state->should_save_now || state->should_save_when_done) {
          state->should_save_now = false;
          state->should_save_when_done = false;
          glReadPixels(0, 0, state->width, state->height, GL_RGB, GL_UNSIGNED_BYTE, state->ppm);
          printf("P6\n%d %d\n255\n", state->width, state->height);
          fwrite(state->ppm, state->width * state->height * 3, 1, stdout);
          fflush(stdout);
        }

        glfwWaitEvents();
        if (glfwWindowShouldClose(state->window)) {
          break;
        }

  //      if (state->should_redraw) {
  //        state->should_redraw = false;
  //        fprintf(stderr, "refresh\n");
          refresh_callback(state);
          glfwSwapBuffers(state->window);
//          glfwSwapBuffers(state->window);
  //      }

        handle_view_morph(state->context, state);

      }

    }

  //  fprintf(stderr, "delete\n");
    perturbator_stop(state->context, true);
  }


//  image_delete(context);
  free(state->ppm);
  glfwDestroyWindow(state->window);
  glfwTerminate();

#endif

  return 0;
(void) argc;
(void) argv;
}
