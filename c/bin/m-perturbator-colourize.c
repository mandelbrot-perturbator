// mandelbrot-perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  size_t width = 0;
  size_t height = 0;
  if (argc > 1)
    width = atoi(argv[1]);
  if (argc > 2)
    height = atoi(argv[2]);
  size_t pixels = width * height;
  size_t ibytes = 4 * sizeof(float) * pixels;
  size_t obytes = 3 * pixels;
  float *i = malloc(ibytes);
  unsigned char *o = malloc(obytes);
  fread(i, ibytes, 1, stdin);
  double n = 0;
  double count = 0;
  double mi = 1.0/0.0;
  double ma = -1.0/0.0;
  #pragma omp parallel for reduction(+:count) reduction(+:n) reduction(min:mi) reduction(max:ma)
  for (size_t p = 0; p < pixels; ++p)
  {
    double ni = i[4 * p + 0];
    double nf = i[4 * p + 1];
    if (ni <= 0.0 && nf <= 0.0)
    {
      o[3 * p + 0] = 255;
      o[3 * p + 1] = 179;
      o[3 * p + 2] = 0;
    }
    else
    {
      float de = i[4 * p + 3];
      int g = 255 * tanhf(de);
      o[3 * p + 0] = g;
      o[3 * p + 1] = g;
      o[3 * p + 2] = g;
      double nif = ni + nf;
      mi = mi > nif ? nif : mi;
      ma = ma < nif ? nif : ma;
      n += nif;
      count += 1;
    }
  }
  fprintf(stderr, "%.16g %.16g %.16g\n", mi, n / count, ma);
  printf("P6\n%ld %ld\n255\n", width, height);
  fwrite(o, obytes, 1, stdout);
  free(i);
  free(o);
  return 0;
}
