// mandelbrot-perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#define _POSIX_C_SOURCE 200809L
#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <mpfr.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <cairo-pdf.h>
#include <cairo-svg.h>
#include <toml.h>

#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>
#include <mandelbrot-perturbator.h>

#define pi 3.141592653589793
#define twopi 6.283185307179586

int serialize_toml(const char *filename);
int deserialize_toml(const char *filename);
int deserialize_old(const char *filename);

void rgb_invert(double *r, double *g, double *b)
{
  double ma = fmax(fmax(*r, *g), *b);
  double mi = fmin(fmin(*r, *g), *b);
  if (ma == 0 || mi == 1)
  {
    *r = 1 - *r;
    *g = 1 - *g;
    *b = 1 - *b;
  }
}

char *escape_for_toml_string(const char *s) // TODO
{
  return 0; // no escape necessary
}


struct view {
  mpfr_t cx, cy, radius, pixel_spacing;
};

static struct view *view_new() {
  struct view *v = malloc(sizeof(struct view));
  mpfr_init2(v->cx, 53);
  mpfr_init2(v->cy, 53);
  mpfr_init2(v->radius, 53);
  mpfr_init2(v->pixel_spacing, 53);
  return v;
}

static void view_delete(struct view *v) {
  mpfr_clear(v->cx);
  mpfr_clear(v->cy);
  mpfr_clear(v->radius);
  mpfr_clear(v->pixel_spacing);
  free(v);
}

static struct view *view_copy(const struct view *v) {
  struct view *v2 = view_new();
  mpfr_set_prec(v2->cx, mpfr_get_prec(v->cx));
  mpfr_set_prec(v2->cy, mpfr_get_prec(v->cy));
  mpfr_set(v2->cx, v->cx, GMP_RNDN);
  mpfr_set(v2->cy, v->cy, GMP_RNDN);
  mpfr_set(v2->radius, v->radius, GMP_RNDN);
  return v2;
}

struct point {
  struct point *next;
  struct point *pred;
  mpc_t xy;
};

static struct point *point_new(struct point *next) {
  struct point *l = calloc(1, sizeof(*l));
  l->next = next;
  if (next)
  {
    l->pred = next->pred;
    next->pred = l;
  }
  mpc_init2(l->xy, 53);
  return l;
}

static void points_delete(struct point *l) {
  struct point *next = l;
  while ((l = next)) {
    next = l->next;
    mpc_clear(l->xy);
    l->next = 0;
    l->pred = 0;
    free(l);
  }
}

static struct point *points_copy(const struct point *last, struct point **tail)
{
  struct point *result = 0;
  int first = 1;
  while (last)
  {
    result = point_new(result);
    if (first)
    {
      if (tail)
      {
        *tail = result;
      }
      first = 0;
    }
    mpfr_set_prec(mpc_realref(result->xy), mpfr_get_prec(mpc_realref(last->xy)));
    mpfr_set_prec(mpc_imagref(result->xy), mpfr_get_prec(mpc_imagref(last->xy)));
    mpc_set(result->xy, last->xy, MPC_RNDNN);
    last = last->pred;
  }
  return result;
}

struct annotation_style
{
  int line_type;
  int fill_type;
  double colour_r;
  double colour_g;
  double colour_b;
};

enum annotation_t {
  annotation_ray_out,
  annotation_ray_in,
  annotation_nucleus,
  annotation_misiurewicz,
  annotation_text,
  annotation_wake,
  annotation_atom,
  annotation_domain,
  annotation_misiurewicz_domain,
  annotation_domain_estimate
};

struct annotation_ray_out {
  struct point *line_start;
  struct point *line_end;
};

struct annotation_ray_in {
  struct point *line_start;
  struct point *line_end;
  struct m_binangle angle;
  struct m_r_exray_in *ray;
  int depth;
/*
  int have_nucleus;
  int have_landing;
  mpfr_t nucleusx, nucleusy, landingx, landingy, eps_2;
*/
};

struct annotation_text {
  mpc_t xy;
};

struct annotation_nucleus {
  mpc_t xy;
  int period;
  mpfr_t size;
  mpfr_t domain_size;
};

struct annotation_misiurewicz {
  mpc_t xy;
  int period;
  int preperiod;
  mpfr_t domain_size;
};

struct annotation_wake {
  struct annotation *next_wake;
  struct annotation *pred_wake;
  char *ray_lo;
  char *ray_hi;
  int ray_depth;
  struct point *line_start;
  struct point *line_end;
  mpq_t width;
};

struct annotation_atom {
  mpc_t nucleus;
  int period;
  m_shape shape;
  struct point *line_start;
  struct point *line_end;
};

struct annotation_domain {
  mpc_t nucleus;
  int period;
  int loperiod;
  struct point *line_start;
  struct point *line_end;
};

struct annotation_misiurewicz_domain {
  mpc_t misiurewicz;
  int period;
  int lopreperiod;
  int hipreperiod;
  struct point *line_start;
  struct point *line_end;
};

struct annotation_domain_estimate {
  mpc_t c;
  mpfr_t r;
  int preperiod;
  int period;
};

struct annotation {
  struct annotation *next;
  int id;
  char *label;
  int selected;
  struct annotation_style style;
  enum annotation_t tag;
  union {
    struct annotation_ray_out ray_out;
    struct annotation_ray_in ray_in;
    struct annotation_text text;
    struct annotation_nucleus nucleus;
    struct annotation_misiurewicz misiurewicz;
    struct annotation_wake wake;
    struct annotation_atom atom;
    struct annotation_domain domain;
    struct annotation_misiurewicz_domain misiurewicz_domain;
    struct annotation_domain_estimate domain_estimate;
  } u;
};

static bool annotation_set_colour(struct annotation *a, double r, double g, double b)
{
  a->style.colour_r = r;
  a->style.colour_g = g;
  a->style.colour_b = b;
  return true;
}

static bool annotation_set_line_type(struct annotation *a, int t)
{
  a->style.line_type = t;
  return true;
}

static bool annotation_set_fill_type(struct annotation *a, int t)
{
  a->style.fill_type = t;
  return true;
}

enum colour_theme_t
{
  colour_theme_monochrome = 0,
  colour_theme_low_colour = 1,
  colour_theme_full_colour = 2
};

enum key_theme_t
{
  key_theme_none = 0,
  key_theme_overlay = 1,
  key_theme_detach = 2
};

static struct
{
  // fractal image
  double dpi_print;
  double dpi_screen;
  cairo_surface_t *surface;
  cairo_surface_t *mask;
  cairo_surface_t *selection_surface;
  GtkWidget *da, *window;
  int width_print, height_print;
  int width_screen, height_screen;
  // zoom tool
  gulong zoom_motion_notify_handler, zoom_button_press_handler, zoom_button_release_handler;
  // info tool
  gulong info_button_press_handler;
  // ray out tool
  gulong ray_out_button_press_handler;
  // nucleus tool
  gulong nucleus_motion_notify_handler, nucleus_button_press_handler, nucleus_button_release_handler;
  // misurewicz tool
  gulong misiurewicz_motion_notify_handler, misiurewicz_button_press_handler, misiurewicz_button_release_handler;
  // selection tool
  gulong selection_button_press_handler;
  // bond tool
  gulong bond_button_press_handler;
  // transient box drawing
  int box;
  double box_aspect, box_x1, box_y1, box_x2, box_y2;
  // transient ball drawing
  int ball;
  double ball_x1, ball_y1, ball_x2, ball_y2;
  // mandelbrot view
  struct view *view;
  double escape_radius;
  mpfr_t escape_radius2;
  int maximum_iterations;
  int chunk;
  int threads;
  double glitch_threshold;
  struct perturbator *image;
  // annotations
  GtkTreeStore *annostore;
  GtkWidget *annotree;
  struct annotation *anno;
  // doubly-linked list of wakes sorted by width, narrowest first
  // real nodes have next_wake != 0 && pred_wake != 0
  // sentinel nodes at end have a null in one
  struct annotation wakes_head;
  struct annotation wakes_tail;
  GtkWidget *filaments_period, *filaments_preperiod, *filaments_depth;
  GtkWidget *rays_of_nrays;
  // async computations
  // FIXME REMOVE {{
  double progress;
  int cancelled;
  GtkWidget *dialog, *bar;
  // FIXME REMOVE }}
  GAsyncQueue *annotation_queue; // annotations to GUI go via here
  GThreadPool *task_workers; // annotation tasks go to here
  GtkWidget *task_box; // annotation tasks in progress displayed here
  GtkWidget *log;
  // theme
  GtkToolItem *dark_widget;
  GtkToolItem *colour_widget;
  int dark;
  int colour;
  // dashed lines
  GtkListStore *dashstore[2];
  GtkWidget *dashcombo;
  // pattern fills
  cairo_pattern_t *fillpattern[2][26];
  GtkListStore *fillstore[2];
  GtkWidget *fillcombo;
  // colours
  GtkWidget *colourbutton;
  // current style
  struct annotation_style style;
  // key
  enum key_theme_t key;
  // menus for programmatic update when loading files
  GtkWidget *menu_theme_dark;
  GtkWidget *menu_theme_light;
  GtkWidget *menu_colour_monochrome;
  GtkWidget *menu_colour_low;
  GtkWidget *menu_colour_full;
  GtkWidget *menu_key_none;
  GtkWidget *menu_key_overlay;
  GtkWidget *menu_key_detach;
} G;

void set_dark_theme(bool use_dark_theme, bool fromGUI);
void set_colour_theme(enum colour_theme_t use_colour_theme);
void set_key_theme(enum key_theme_t use_key_theme);

static void screen_to_param(double x, double y, mpfr_t cx, mpfr_t cy) {
  double sx = (x - G.width_screen / 2.0) / (G.height_screen / 2.0);
  double sy = (G.height_screen / 2.0 - y) / (G.height_screen / 2.0);
  mpfr_set_prec(cx, mpfr_get_prec(G.view->cx));
  mpfr_set_prec(cy, mpfr_get_prec(G.view->cy));
  mpfr_set_d(cx, sx, GMP_RNDN);
  mpfr_set_d(cy, sy, GMP_RNDN);
  mpfr_mul(cx, cx, G.view->radius, GMP_RNDN);
  mpfr_mul(cy, cy, G.view->radius, GMP_RNDN);
  mpfr_add(cx, cx, G.view->cx, GMP_RNDN);
  mpfr_add(cy, cy, G.view->cy, GMP_RNDN);
}

static void distance_to_screen(double *x, const mpfr_t cr) {
  mpfr_t tx;
  mpfr_init2(tx, 53);
  mpfr_div(tx, cr, G.view->radius, GMP_RNDN);
  double sx = mpfr_get_d(tx, GMP_RNDN);
  *x = sx * G.height_screen / 2.0;
  mpfr_clear(tx);
}

static void param_to_screen(double *x, double *y, const mpfr_t cx, const mpfr_t cy) {
  mpfr_t tx, ty;
  mpfr_init2(tx, mpfr_get_prec(G.view->cx));
  mpfr_init2(ty, mpfr_get_prec(G.view->cy));
  mpfr_sub(tx, cx, G.view->cx, GMP_RNDN);
  mpfr_sub(ty, cy, G.view->cy, GMP_RNDN);
  mpfr_div(tx, tx, G.view->radius, GMP_RNDN);
  mpfr_div(ty, ty, G.view->radius, GMP_RNDN);
  double sx = mpfr_get_d(tx, GMP_RNDN);
  double sy = mpfr_get_d(ty, GMP_RNDN);
  *x = sx * G.height_screen / 2.0 + G.width_screen / 2.0;
  *y = G.height_screen / 2.0 - sy * G.height_screen / 2.0;
  mpfr_clear(tx);
  mpfr_clear(ty);
}

static void param_to_screen_clamped(double *x, double *y, const mpfr_t cx, const mpfr_t cy) {
  mpfr_t tx, ty, bound;
  mpfr_init2(tx, mpfr_get_prec(G.view->cx));
  mpfr_init2(ty, mpfr_get_prec(G.view->cy));
  mpfr_init2(bound, 53);
  // FIXME should depend on image aspect ratio ?
  // FIXME should clip to circle ?
  mpfr_set_si(bound, 10, MPFR_RNDN);
  mpfr_sub(tx, cx, G.view->cx, GMP_RNDN);
  mpfr_sub(ty, cy, G.view->cy, GMP_RNDN);
  mpfr_div(tx, tx, G.view->radius, GMP_RNDN);
  mpfr_div(ty, ty, G.view->radius, GMP_RNDN);
  mpfr_min(tx, tx, bound, MPFR_RNDN);
  mpfr_min(ty, ty, bound, MPFR_RNDN);
  mpfr_neg(bound, bound, MPFR_RNDN);
  mpfr_max(tx, tx, bound, MPFR_RNDN);
  mpfr_max(ty, ty, bound, MPFR_RNDN);
  double sx = mpfr_get_d(tx, GMP_RNDN);
  double sy = mpfr_get_d(ty, GMP_RNDN);
  *x = sx * G.height_screen / 2.0 + G.width_screen / 2.0;
  *y = G.height_screen / 2.0 - sy * G.height_screen / 2.0;
  mpfr_clear(tx);
  mpfr_clear(ty);
  mpfr_clear(bound);
}

static void log_append(const char *text) {
  GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(G.log));
  GtkTextIter end;
  gtk_text_buffer_get_iter_at_offset(buf, &end, -1);
  gtk_text_buffer_insert(buf, &end, text, -1);
  gtk_text_buffer_get_iter_at_offset(buf, &end, -1);
  GtkTextMark *eof = gtk_text_buffer_get_mark(buf, "EOF");
  if (eof) {
    gtk_text_buffer_move_mark(buf, eof, &end);
  } else {
    eof = gtk_text_buffer_create_mark(buf, "EOF", &end, FALSE);
  }
  gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(G.log), eof, 0.0, FALSE, 0.0, 0.0);
}

static gboolean log_append_thread(void *text) {
  log_append(text);
  free(text);
  return G_SOURCE_REMOVE;
}

static gboolean render_thread_progress(void *userdata) {
  (void) userdata;
  if (! G.cancelled)
  {
    if (G.progress == 0)
    {
      gtk_progress_bar_pulse(GTK_PROGRESS_BAR(G.bar));
    }
    else
    {
      gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (G.bar), G.progress);
    }
  }
  return G_SOURCE_REMOVE;
}


static int progresscb(void *userdata, double progress) {
  (void) userdata;
  G.progress = progress;
  gdk_threads_add_idle (render_thread_progress, 0);
  return ! G.cancelled;
}


static void dorender(struct view *v, struct perturbator *image, int maxiters) {
  char *buf = calloc(1, 65536); // FIXME fixed size buffer
  mpfr_snprintf(buf, 65536, "RENDER\nreal: %Re\nimag: %Re\nradius: %Re\n", v->cx, v->cy, v->radius);
  gdk_threads_add_idle(log_append_thread, buf);
  perturbator_set_maximum_iterations(image, maxiters);
  perturbator_start(image, v->cx, v->cy, v->radius);
  struct timespec timeout;
  do
  {
    progresscb(0, perturbator_get_progress(image));
    clock_gettime(CLOCK_REALTIME, &timeout);
    timeout.tv_nsec += 0.25 * 1000 * 1000 * 1000;
    while (timeout.tv_nsec >= 1000 * 1000 * 1000)
    {
      timeout.tv_nsec -= 1000 * 1000 * 1000;
      timeout.tv_sec += 1;
    }
  } while ((! G.cancelled) && perturbator_wait(image, &timeout));
  perturbator_stop(image, G.cancelled);
}

static int channel(float c) {
  return fminf(fmaxf(roundf(255 * c), 0), 255);
}

static void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu) {
  float i, f, p, q, t, r, g, b;
  if (s == 0) { r = g = b = v; } else {
    h = 6 * (h - floorf(h));
    int ii = i = floorf(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  *red = r;
  *grn = g;
  *blu = b;
}

static void colour(int *r, int *g, int *b, int final_n, float final_z_abs, float final_z_arg, float de, int final_p, int t) {
  (void) final_n;
  float radius = 0.0f, angle = 0.0f;
  if (t > 0) {
    radius = 1 - final_z_abs;
    angle = final_z_arg;
  } else if (t < 0) {
    radius = -log2f(1.0f - final_z_abs);
    angle = fmodf(powf(2.0, floorf(radius) + 3.0f) * final_z_arg, 1.0f);
    radius = 1.0 - fmodf(fmodf(radius, 1.0f) + 1.0, 1.0);
  }
  float k = powf(0.5f, 0.5f - radius);
  float grid_weight = 0.05f;
/*
  float grid = fminf
    ( fminf( smoothstep(grid_weight - dradius, grid_weight + dradius, radius)
           , 1.0f - smoothstep(1.0f - grid_weight - dradius, 1.0f - grid_weight + dradius, radius))
    , fminf( smoothstep(grid_weight * k - dangle, grid_weight * k + dangle, angle)
           , 1.0f - smoothstep(1.0 - grid_weight * k - dangle, 1.0 - grid_weight * k + dangle, angle))
    );
*/
  int grid =
    grid_weight     < radius && radius < 1.0f - grid_weight &&
    grid_weight * k < angle  && angle  < 1.0f - grid_weight * k;
  float hue = (final_p - 1.0f) / 24.618033988749895f;
  float sat = 0.0f;//final_p > 0.0f ? 0.5f : 0.0f;
  float val = fmin(0.5 + 0.5 * tanh(fmin(fmax(fabs(de), 0.0), 4.0)), 0.8 + 0.2 * grid);
  if (t == 0) {
    hue = 0.0f;
    sat = 0.0f;
    val = 1.0f;
  }
  float red, grn, blu;
  hsv2rgb(hue, sat, G.dark ? 1 - val : val, &red, &grn, &blu);
  *r = channel(red);
  *g = channel(grn);
  *b = channel(blu);
}

static void docolour(struct perturbator *image) {
  if (!G.surface) {
    return;
  }
  const int32_t *dwell_n  = perturbator_get_dwell_n (image);
  const float   *dwell_f  = perturbator_get_dwell_f (image);
  const float   *dwell_a  = perturbator_get_dwell_a (image);
  const float   *distance = perturbator_get_distance(image);
  cairo_surface_flush(G.surface);
  uint32_t *data = (uint32_t *) cairo_image_surface_get_data(G.surface);
  int channels = 4;
  int stride = cairo_image_surface_get_stride(G.surface) / channels;
  #pragma omp parallel for
  for (int j = 0; j < G.height_print; ++j) {
    for (int i = 0; i < G.width_print; ++i) {
      int ki = j * G.width_print + i;
      double final_n_int = dwell_n[ki];
      double final_z_abs = dwell_f[ki];
      double final_z_arg = fmod(dwell_a[ki]+ 1, 1);
      double de          = distance[ki];
      int final_n = floor(final_n_int + final_z_abs);
      int final_p = de < 0 ? -final_n_int : 0;
      int t = de < 0 ? -1 : de > 0 ? 1 : 0;
      int r, g, b;
      colour(&r, &g, &b, final_n, final_z_abs, final_z_arg, de, final_p, t);
      int px = (r << 16) | (g << 8) | b;
      int ko = j * stride + i;
      data[ko] = (0xFF << 24) | px;
    }
  }
  cairo_surface_mark_dirty(G.surface);
}

struct render_data {
  struct view *view;
  struct perturbator *image;
  int maximum_iterations;
};

static gboolean render_thread_cancelled(gpointer user_data) {
  struct render_data *d = user_data;
  G.dialog = 0;
  G.bar = 0;
  log_append("\nCANCELLED");
  free(d);
  return G_SOURCE_REMOVE;
}

static gboolean render_thread_done(gpointer user_data) {
  struct render_data *d = user_data;
  gtk_widget_destroy(G.dialog);
  G.dialog = 0;
  G.bar = 0;
  if (G.view) {
    view_delete(G.view);
    G.view = 0;
  }
  G.view = d->view;
  free(d);
  gtk_widget_queue_draw(G.da);
  return G_SOURCE_REMOVE;
}

static gpointer render_thread(gpointer user_data) {
  struct render_data *d = user_data;
  if (! G.cancelled) {
    dorender(d->view, d->image, d->maximum_iterations);
  }
  int cancelled = G.cancelled;
  if (! cancelled) {
    docolour(d->image);
  }
  if (cancelled) {
    view_delete(d->view);
    d->view = 0;
    gdk_threads_add_idle(render_thread_cancelled, d);
  } else {
    gdk_threads_add_idle(render_thread_done, d);
  }
  return 0;
}

static void start_render(struct view *v) {

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);

  struct render_data *d = malloc(sizeof(struct render_data));
  d->view = v;
  d->image = G.image;
  d->maximum_iterations = G.maximum_iterations;
  G.cancelled = 0;
  G.progress = 0;

  GThread *thread = g_thread_new("render", render_thread, d);

  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}

static struct annotation *get_selected_annotation(void)
{
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer thing;
  if (gtk_tree_selection_get_selected(select, &model, &iter))
  {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing)
    {
      struct annotation *anno = (struct annotation *) thing;
      return anno;
    }
  }
  return 0;
}

static gboolean add_annotation(void *userdata) {
  static int nextid = 0;
  struct annotation *ls = userdata;
  ls->id = ++nextid;
  ls->next = G.anno;
  G.anno = ls;
  // maintain list of wakes sorted by width
  if (ls->tag == annotation_wake)
  {
    struct annotation *ws = G.wakes_head.u.wake.next_wake;
    while (ws->u.wake.next_wake)
    {
      if (mpq_cmp(ls->u.wake.width, ws->u.wake.width) <= 0)
        break;
      ws = ws->u.wake.next_wake;
    }
    // insert before ws
    ls->u.wake.next_wake = ws;
    ls->u.wake.pred_wake = ws->u.wake.pred_wake;
    ws->u.wake.pred_wake->u.wake.next_wake = ls;
    ws->u.wake.pred_wake = ls;
  }
  GtkTreeIter iter;
  gtk_tree_store_append(G.annostore, &iter, NULL);
  gtk_tree_store_set(G.annostore, &iter, 0, ls->label, 1, ls, -1);
  return G_SOURCE_REMOVE;
}

static void unlink_annotation(struct annotation *a)
{
  struct annotation *prev = 0;
  for (struct annotation *ls = G.anno; ls; ls = ls->next) {
    if (ls->next == a) {
      prev = ls;
      break;
    }
  }
  if (prev) {
    prev->next = prev->next->next;
  } else {
    if (G.anno == a) {
      G.anno = G.anno->next;
    }
  }
  a->next = 0;
  if (a->tag == annotation_wake)
  {
    a->u.wake.next_wake->u.wake.pred_wake = a->u.wake.pred_wake;
    a->u.wake.pred_wake->u.wake.next_wake = a->u.wake.next_wake;
    a->u.wake.next_wake = 0;
    a->u.wake.pred_wake = 0;
  }
}

static void free_annotation(struct annotation *a) {
  free(a->label);
  switch (a->tag) {
    case annotation_ray_out:
      points_delete(a->u.ray_out.line_start);
      a->u.ray_out.line_start = 0;
      a->u.ray_out.line_end = 0;
      break;
    case annotation_ray_in:
      points_delete(a->u.ray_in.line_start);
      a->u.ray_in.line_start = 0;
      a->u.ray_in.line_end = 0;
      m_r_exray_in_delete(a->u.ray_in.ray);
      m_binangle_clear(&a->u.ray_in.angle);
/*
      mpfr_clear(a->u.ray_in.nucleusx);
      mpfr_clear(a->u.ray_in.nucleusy);
      mpfr_clear(a->u.ray_in.landingx);
      mpfr_clear(a->u.ray_in.landingy);
      mpfr_clear(a->u.ray_in.eps_2);
*/
      break;
    case annotation_text:
      mpfr_clear(mpc_realref(a->u.text.xy));
      mpfr_clear(mpc_imagref(a->u.text.xy));
      break;
    case annotation_nucleus:
      mpc_clear(a->u.nucleus.xy);
      mpfr_clear(a->u.nucleus.size);
      mpfr_clear(a->u.nucleus.domain_size);
      break;
    case annotation_misiurewicz:
      mpc_clear(a->u.misiurewicz.xy);
      mpfr_clear(a->u.misiurewicz.domain_size);
      break;
    case annotation_wake:
      points_delete(a->u.wake.line_start);
      mpq_clear(a->u.wake.width);
      free(a->u.wake.ray_lo);
      free(a->u.wake.ray_hi);
      break;
    case annotation_atom:
      points_delete(a->u.atom.line_start);
      mpc_clear(a->u.atom.nucleus);
      break;
    case annotation_domain:
      points_delete(a->u.domain.line_start);
      mpc_clear(a->u.domain.nucleus);
      break;
    case annotation_misiurewicz_domain:
      points_delete(a->u.misiurewicz_domain.line_start);
      mpc_clear(a->u.misiurewicz_domain.misiurewicz);
      break;
    case annotation_domain_estimate:
      mpc_clear(a->u.domain_estimate.c);
      mpfr_clear(a->u.domain_estimate.r);
      break;
  }
  free(a);
}

static void delete_annotation(struct annotation *a)
{
  unlink_annotation(a);
  free_annotation(a);
}

static double image_peek_dwell(struct perturbator *image, int i, int j)
{
  int w = perturbator_get_width(image);
  int h = perturbator_get_height(image);
  if (! (0 <= i && i < w && 0 <= j && j < h))
  {
    return 0.0 / 0.0; // unknown
  }
  const int32_t *dwell_n = perturbator_get_dwell_n(image);
  const float   *dwell_f = perturbator_get_dwell_f(image);
  int k = j * w + i;
  double dwelln = dwell_n[k];
  double dwellf = dwell_f[k];
  if (dwelln <= 0 || dwellf <= 0) { // interior
    return 1.0 / 0.0;
  }
  return dwelln + dwellf;
}

struct task;
static struct task *task_new_ray_out(GAsyncQueue *output, const mpc_t c, int dwell, const struct annotation_style *s);
static struct task *task_new_ray_in(GAsyncQueue *output, const m_binangle *angle, int depth, const struct annotation_style *s);
static struct task *task_new_rays_of(GAsyncQueue *output, int count, int preperiod, int period, const mpc_t nucleus, const struct annotation_style *s);
static struct task *task_new_ray_extend(GAsyncQueue *output, struct annotation *anno, int depth, const struct annotation_style *s);
static struct task *task_new_nucleus(GAsyncQueue *output, const mpc_t c, const mpfr_t r, int dwell, const struct annotation_style *s);
static struct task *task_new_bond(GAsyncQueue *output, const mpc_t c, int period, const struct annotation_style *s);
static struct task *task_new_misiurewicz(GAsyncQueue *output, const mpc_t c, const mpfr_t r, int maxpreperiod, int maxperiod, const struct annotation_style *s);
static struct task *task_new_muunit(GAsyncQueue *output, const mpc_t nucleus, int period, const struct annotation_style *s);
static struct task *task_new_muunit_nucleus(GAsyncQueue *output, const mpc_t nucleus, int nucleus_period, int ray_period, const mpq_t angle, const struct annotation_style *s);
static struct task *task_new_filaments(GAsyncQueue *output, const mpc_t inner_nucleus, int inner_period, int outer_period, int preperiod, int depth, const struct annotation_style *s);
static struct task *task_new_wake(GAsyncQueue *output, struct annotation *lo, struct annotation *hi, const struct annotation_style *s);
static struct task *task_new_wake2(GAsyncQueue *output, char *lo, char *hi, int depth, const struct annotation_style *s);
static struct task *task_new_atom(GAsyncQueue *output, const mpc_t nucleus, int period, const struct annotation_style *s);
static struct task *task_new_domain(GAsyncQueue *output, const mpc_t nucleus, int period, int loperiod, const struct annotation_style *s);
static struct task *task_new_misiurewicz_domain(GAsyncQueue *output, const mpc_t misiurewicz, int period, int lopreperiod, int hipreperiod, const struct annotation_style *s);
static gboolean task_enqueue_cb(gpointer userdata);
static gboolean annotation_cb(gpointer userdata);

static void start_ray_out(double x, double y)
{
  double dwell = image_peek_dwell
    ( G.image
    , x * G.width_print / G.width_screen
    , y * G.height_print / G.height_screen
    );
  if (isinf(dwell) || isnan(dwell))
  {
    return;
  }
  mpc_t c;
  mpc_init2(c, 53);
  screen_to_param(x, y, mpc_realref(c), mpc_imagref(c));
  struct task *t = task_new_ray_out(G.annotation_queue, c, ceil(dwell), &G.style);
  mpc_clear(c);
  task_enqueue_cb(t);
}

static void start_ray_in(struct m_binangle *angle, int depth) {
  struct task *t = task_new_ray_in(G.annotation_queue, angle, depth, &G.style);
  task_enqueue_cb(t);
}

static void start_rays_of(int count, int preperiod, int period, const mpc_t nucleus) {
  struct task *t = task_new_rays_of(G.annotation_queue, count, preperiod, period, nucleus, &G.style);
  task_enqueue_cb(t);
}

static void start_ray_extend(struct annotation *anno, int depth) {
  unlink_annotation(anno);
  struct task *t = task_new_ray_extend(G.annotation_queue, anno, depth, &G.style);
  task_enqueue_cb(t);
}

static void start_nucleus(const mpfr_t x, const mpfr_t y, const mpfr_t r, int maxperiod)
{
  mpc_t c;
  mpc_init2(c, mpfr_get_prec(x));
  mpc_set_fr_fr(c, x, y, MPC_RNDNN);
  struct task *t = task_new_nucleus(G.annotation_queue, c, r, maxperiod, &G.style);
  mpc_clear(c);
  task_enqueue_cb(t);
}

static void start_bond(const mpc_t c, int period)
{
  struct task *t = task_new_bond(G.annotation_queue, c, period, &G.style);
  task_enqueue_cb(t);
}

static void start_wake(struct annotation *anno, int direction)
{
  // find neighbouring angle in direction
  mpq_t p, q, r;
  mpq_init(p);
  mpq_init(q);
  mpq_init(r);
  m_binangle_to_rational(p, &anno->u.ray_in.angle);
  if (direction > 0)
  {
    mpq_set_si(q, 1, 1);
  }
  else
  {
    mpq_set_si(q, 0, 1);
  }
  struct annotation *other = 0;
  struct annotation *a = G.anno;
  mpc_t d;
  mpc_init2(d, 53);
  while (a)
  {
    if (a->tag == annotation_ray_in &&
      a->u.ray_in.angle.pre.length == anno->u.ray_in.angle.pre.length &&
      a->u.ray_in.angle.per.length == anno->u.ray_in.angle.per.length)
    {
      m_binangle_to_rational(r, &a->u.ray_in.angle);
      // if (p < r < q || q < r < p) && endpoint is nearby, set q = r
      int pr = mpq_cmp(p, r);
      int rq = mpq_cmp(r, q);
      mpc_sub(d, a->u.ray_in.line_start->xy, anno->u.ray_in.line_start->xy, MPC_RNDNN);
      mpc_abs(mpc_realref(d), d, MPFR_RNDN);
      mpfr_div(mpc_realref(d), mpc_realref(d), G.view->radius, MPFR_RNDN);
      double distance = mpfr_get_d(mpc_realref(d), MPFR_RNDN);
      if (((pr < 0 && rq < 0) || (0 < pr && 0 < rq)) && distance < 0.1)
      {
        other = a;
        mpq_set(q, r);
      }
    }
    a = a->next;
  }
  mpc_clear(d);
  mpq_clear(p);
  mpq_clear(q);
  mpq_clear(r);
  if (other)
  {
    GtkTreeModel *model = GTK_TREE_MODEL(G.annostore);
    GtkTreeIter iter;
    gboolean ok = gtk_tree_model_get_iter_first(model, &iter);
    while (ok)
    {
      gpointer thing;
      gtk_tree_model_get(model, &iter, 1, &thing, -1);
      if (thing == anno || thing == other)
      {
        ok = gtk_tree_store_remove(G.annostore, &iter);
      }
      else
      {
        ok = gtk_tree_model_iter_next(model, &iter);
      }
    }
    unlink_annotation(anno);
    unlink_annotation(other);
    struct task *t = task_new_wake(G.annotation_queue, direction > 0 ? anno : other, direction > 0 ? other : anno, &G.style);
    task_enqueue_cb(t);
  }
}

static void start_wake2(int depth, char *lo, char *hi)
{
  struct task *t = task_new_wake2(G.annotation_queue, lo, hi, depth, &G.style);
  task_enqueue_cb(t);
}

static void start_atom(const mpc_t c, int period)
{
  struct task *t = task_new_atom(G.annotation_queue, c, period, &G.style);
  task_enqueue_cb(t);
}

static void start_domain(const mpc_t c, int period, int loperiod)
{
  struct task *t = task_new_domain(G.annotation_queue, c, period, loperiod, &G.style);
  task_enqueue_cb(t);
}

static void start_misiurewicz_domain(const mpc_t c, int period, int lopreperiod, int hipreperiod)
{
  struct task *t = task_new_misiurewicz_domain(G.annotation_queue, c, period, lopreperiod, hipreperiod, &G.style);
  task_enqueue_cb(t);
}

static void start_domain_estimate(const mpc_t c, const mpfr_t r, int preperiod, int period, const char *label)
{
  // do it here instead of async, work is small
  struct annotation *a = calloc(1, sizeof(*a));
  a->tag = annotation_domain_estimate;
  a->label = strdup(label);
  a->style = G.style;
  mpfr_prec_t prec = mpfr_get_prec(mpc_realref(c));
  mpc_init2(a->u.domain_estimate.c, prec);
  mpc_set(a->u.domain_estimate.c, c, MPC_RNDNN);
  mpfr_init2(a->u.domain_estimate.r, 53);
  mpfr_set(a->u.domain_estimate.r, r, MPFR_RNDN);
  a->u.domain_estimate.preperiod = preperiod;
  a->u.domain_estimate.period = period;
  annotation_cb(a);
}

static void box_rectangle(double *x, double *y, double *w, double *h) {
  double dx = fabs(G.box_x2 - G.box_x1);
  double dy = fabs(G.box_y2 - G.box_y1);
  if (dx <= G.box_aspect * dy) {
    *h = dy;
    *w = *h * G.box_aspect;
  } else {
    *w = dx;
    *h = *w / G.box_aspect;
  }
  *x = G.box_x1 - *w;
  *y = G.box_y1 - *h;
  *w *= 2;
  *h *= 2;
}

static void draw_box(cairo_t *cr) {
  double x, y, w, h;
  box_rectangle(&x, &y, &w, &h);
  cairo_rectangle(cr, x, y, w, h);
  if (G.dark)
  {
    cairo_set_source_rgba(cr, 1.0, 0.75, 0.75, 0.25);
  }
  else
  {
    cairo_set_source_rgba(cr, 0.5, 0.25, 0.25, 0.25);
  }
  cairo_fill_preserve(cr);
  if (G.dark)
  {
    cairo_set_source_rgba(cr, 1.0, 0.75, 0.75, 1);
  }
  else
  {
    cairo_set_source_rgba(cr, 0.5, 0.25, 0.25, 1);
  }
  cairo_stroke(cr);
}

static void ball_circle(double *x, double *y, double *r) {
  double dx = G.ball_x2 - G.ball_x1;
  double dy = G.ball_y2 - G.ball_y1;
  *x = G.ball_x1;
  *y = G.ball_y1;
  *r = hypot(dx, dy);
}

static void draw_ball(cairo_t *cr) {
  double x, y, r;
  ball_circle(&x, &y, &r);
  cairo_arc(cr, x, y, r, 0, 6.283185307179586);
  if (G.dark)
  {
    cairo_set_source_rgba(cr, 1.0, 0.75, 0.75, 0.25);
  }
  else
  {
    cairo_set_source_rgba(cr, 0.5, 0.25, 0.25, 0.25);
  }
  cairo_fill_preserve(cr);
  if (G.dark)
  {
    cairo_set_source_rgba(cr, 1.0, 0.75, 0.75, 1);
  }
  else
  {
    cairo_set_source_rgba(cr, 0.5, 0.25, 0.25, 1);
  }
  cairo_stroke(cr);
}

static void draw_text(cairo_t *ctx, cairo_t *ctxmask, cairo_t *ctxsel, double x, double y, double align_x, double align_y, double fontsize, bool bold, const char *label)
{
  bool dodraw = true;
  if (ctxmask)
  {
    cairo_surface_flush(G.mask);
    uint32_t *pixels = (uint32_t *) cairo_image_surface_get_data(G.mask);
    int channels = 4;
    int stride = cairo_image_surface_get_stride(G.mask) / channels;
    if (0 <= x && x < G.width_screen && 0 <= y && y < G.height_screen) {
      int i = x;
      int j = y;
      int k = stride * j + i;
      dodraw = (pixels[k] != 0xffffffff);
    }
  }
  if (dodraw)
  {
    if (-G.width_screen <= x && x < 2 * G.width_screen && -G.height_screen <= y && y < 2 * G.height_screen) {
      cairo_select_font_face(ctx, "LMSans10", CAIRO_FONT_SLANT_NORMAL, bold ? CAIRO_FONT_WEIGHT_BOLD : CAIRO_FONT_WEIGHT_NORMAL);
      cairo_set_font_size(ctx, fontsize * G.dpi_screen / 100);
      cairo_text_extents_t e;
      cairo_text_extents(ctx, label, &e);
      cairo_move_to(ctx, x - e.width * align_x, y + e.height * align_y);
      cairo_show_text(ctx, label);
      cairo_new_path(ctx); // clear current point
      // mask out rectangle centered on same point
      double xc = x - e.width * align_x + e.width / 2;
      double yc = y - e.height * align_y + e.height / 2;
      if (ctxmask)
      {
        cairo_move_to(ctxmask, xc - e.width, yc + e.height);
        cairo_line_to(ctxmask, xc + e.width, yc + e.height);
        cairo_line_to(ctxmask, xc + e.width, yc - e.height);
        cairo_line_to(ctxmask, xc - e.width, yc - e.height);
        cairo_line_to(ctxmask, xc - e.width, yc + e.height);
        cairo_fill(ctxmask);
      }
      if (ctxsel)
      {
        cairo_move_to(ctxsel, xc - e.width, yc + e.height);
        cairo_line_to(ctxsel, xc + e.width, yc + e.height);
        cairo_line_to(ctxsel, xc + e.width, yc - e.height);
        cairo_line_to(ctxsel, xc - e.width, yc - e.height);
        cairo_line_to(ctxsel, xc - e.width, yc + e.height);
        cairo_fill(ctxsel);
      }
    }
  }
}

static cairo_pattern_t *generate_fill_pattern(cairo_surface_t *parent, int p, double r, double g, double b)
{
  cairo_surface_t *surface = 0;
  if (parent)
  {
    surface = cairo_surface_create_similar(parent, CAIRO_CONTENT_COLOR_ALPHA, 16, 16);
  }
  else
  {
    surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 16, 16);
  }
  cairo_t *cr = cairo_create(surface);
  cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
  cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
  cairo_set_line_width(cr, 1);
  cairo_set_source_rgba(cr, r, g, b, 0.5);
  switch (((p % 26) + 26) % 26)
  {
    // short and long dashes in 4 directions
    case  0: { cairo_move_to(cr,  5,  8); cairo_line_to(cr, 11,  8); break; }
    case  1: { cairo_move_to(cr,  3,  8); cairo_line_to(cr, 13,  8); break; }
    case  2: { cairo_move_to(cr,  8,  5); cairo_line_to(cr,  8, 11); break; }
    case  3: { cairo_move_to(cr,  8,  4); cairo_line_to(cr,  8, 13); break; }
    case  4: { cairo_move_to(cr,  5,  5); cairo_line_to(cr, 11, 11); break; }
    case  5: { cairo_move_to(cr,  3,  4); cairo_line_to(cr, 13, 13); break; }
    case  6: { cairo_move_to(cr,  5, 11); cairo_line_to(cr, 11,  5); break; }
    case  7: { cairo_move_to(cr,  3, 13); cairo_line_to(cr, 13,  5); break; }
    // small and large crosses in 2 directions
    case  8: { cairo_move_to(cr,  5,  8); cairo_line_to(cr, 11,  8); cairo_move_to(cr,  8,  5); cairo_line_to(cr,  8, 11); break; }
    case  9: { cairo_move_to(cr,  3,  8); cairo_line_to(cr, 13,  8); cairo_move_to(cr,  8,  3); cairo_line_to(cr,  8, 13); break; }
    case 10: { cairo_move_to(cr,  5,  5); cairo_line_to(cr, 11, 11); cairo_move_to(cr,  5, 11); cairo_line_to(cr, 11,  5); break; }
    case 11: { cairo_move_to(cr,  3,  3); cairo_line_to(cr, 13, 13); cairo_move_to(cr,  3, 13); cairo_line_to(cr, 13,  3); break; }
    case 12: { cairo_move_to(cr,  3,  8); cairo_line_to(cr, 13,  8); cairo_move_to(cr,  8,  3); cairo_line_to(cr,  8, 13);
               cairo_move_to(cr,  3,  3); cairo_line_to(cr, 13, 13); cairo_move_to(cr,  3, 13); cairo_line_to(cr, 13,  3); break; }
    case 13: { cairo_move_to(cr,  5,  8); cairo_line_to(cr, 11,  8); cairo_move_to(cr,  8,  5); cairo_line_to(cr,  8, 11);
               cairo_move_to(cr,  5,  5); cairo_line_to(cr, 11, 11); cairo_move_to(cr,  5, 11); cairo_line_to(cr, 11,  5); break; }
    // small and large triangles in 4 directions
    case 14: { cairo_move_to(cr,  5,  5); cairo_line_to(cr, 11,  5); cairo_line_to(cr,  8, 11); cairo_close_path(cr); break; }
    case 15: { cairo_move_to(cr,  3,  3); cairo_line_to(cr, 13,  3); cairo_line_to(cr,  8, 13); cairo_close_path(cr); break; }
    case 16: { cairo_move_to(cr,  5, 11); cairo_line_to(cr, 11, 11); cairo_line_to(cr,  8,  5); cairo_close_path(cr); break; }
    case 17: { cairo_move_to(cr,  3, 13); cairo_line_to(cr, 13, 13); cairo_line_to(cr,  8,  3); cairo_close_path(cr); break; }
    case 18: { cairo_move_to(cr,  5,  5); cairo_line_to(cr,  5, 11); cairo_line_to(cr, 11,  8); cairo_close_path(cr); break; }
    case 19: { cairo_move_to(cr,  3,  3); cairo_line_to(cr,  3, 13); cairo_line_to(cr, 13,  8); cairo_close_path(cr); break; }
    case 20: { cairo_move_to(cr, 11,  5); cairo_line_to(cr, 11, 11); cairo_line_to(cr,  5,  8); cairo_close_path(cr); break; }
    case 21: { cairo_move_to(cr, 13,  3); cairo_line_to(cr, 13, 13); cairo_line_to(cr,  3,  8); cairo_close_path(cr); break; }
    // small and large squares in 2 directions
    case 22: { cairo_move_to(cr,  5,  5); cairo_line_to(cr, 11,  5); cairo_line_to(cr, 11, 11); cairo_line_to(cr,  5, 11); cairo_close_path(cr); break; }
    case 23: { cairo_move_to(cr,  3,  3); cairo_line_to(cr, 13,  3); cairo_line_to(cr, 13, 13); cairo_line_to(cr,  3, 13); cairo_close_path(cr); break; }
    case 24: { cairo_move_to(cr,  5,  8); cairo_line_to(cr,  8,  5); cairo_line_to(cr, 11,  8); cairo_line_to(cr,  8, 11); cairo_close_path(cr); break; }
    case 25: { cairo_move_to(cr,  3,  8); cairo_line_to(cr,  8,  3); cairo_line_to(cr, 13,  8); cairo_line_to(cr,  8, 13); cairo_close_path(cr); break; }
  }
  cairo_stroke(cr);
  cairo_destroy(cr);
  cairo_pattern_t *pattern = cairo_pattern_create_for_surface(surface);
  cairo_pattern_set_extend(pattern, CAIRO_EXTEND_REPEAT);
  cairo_surface_destroy(surface);
  return pattern;
}

static void initialize_fill_patterns(void)
{
  for (int dark = 0; dark < 2; ++dark)
  {
    for (int p = 0; p < 26; ++p)
    {
      G.fillpattern[dark][p] = generate_fill_pattern(0, p, dark, dark, dark);
    }
  }
}

struct dash { double pattern[8]; int count; };
static const struct dash dashes[16] =
#define gap 4
  { { { 0 }, 0 }
  , { { 2, gap }, 2 }
  , { { 4, gap }, 2 }
  , { { 8, gap }, 2 }
  , { { 2, gap, 4, gap }, 4 }
  , { { 2, gap, 8, gap }, 4 }
  , { { 4, gap, 8, gap }, 4 }
  , { { 2, gap, 2, gap, 4, gap }, 6 }
  , { { 2, gap, 2, gap, 8, gap }, 6 }
  , { { 2, gap, 4, gap, 4, gap }, 6 }
  , { { 2, gap, 8, gap, 8, gap }, 6 }
  , { { 4, gap, 8, gap, 8, gap }, 6 }
  , { { 2, gap, 2, gap, 2, gap, 4, gap }, 8 }
  , { { 2, gap, 2, gap, 2, gap, 8, gap }, 8 }
  , { { 2, gap, 2, gap, 4, gap, 4, gap }, 8 }
  , { { 4, gap, 2, gap, 8, gap, 2, gap }, 8 }
  };
#undef gap

static int inside_circle(double cx, double cy, double r, double x, double y)
{
  double dx = x - cx;
  double dy = y - cy;
  return dx * dx + dy * dy < r * r;
}

static void draw_annotation(cairo_t *ctx, cairo_t *ctxmask, cairo_t *ctxsel, struct annotation *ls) {
  double alpha = 1;
  if (ls->tag == annotation_wake)
  {
    alpha = 0.25;
  }
  if (ls->selected) {
    if (G.dark)
    {
      cairo_set_source_rgba(ctx, 1.0, 0.75, 0.75, alpha);
    }
    else
    {
      cairo_set_source_rgba(ctx, 0.5, 0.25, 0.25, alpha);
    }
    cairo_set_line_width(ctx, 3);
  } else {
    if (G.dark)
    {
      cairo_set_source_rgba(ctx, 1, 1, 1, alpha);
    }
    else
    {
      cairo_set_source_rgba(ctx, 0, 0, 0, alpha);
    }
    cairo_set_line_width(ctx, 1);
  }
  cairo_set_source_rgba(ctxsel, ((ls->id >> 16) & 0xFF) / 255.0, ((ls->id >> 8) & 0xFF) / 255.0, ((ls->id >> 0) & 0xFF) / 255.0, 1);
  struct point *l = 0;
  int fill_and_stroke = 0;
  struct annotation_style style = ls->style;
  int circle_at_line_end = 0;
  switch (ls->tag) {
    case annotation_ray_in:
      l = ls->u.ray_in.line_start;
      style.fill_type = -1;
      break;
    case annotation_ray_out:
      l = ls->u.ray_out.line_start;
      style.fill_type = -1;
      break;
    case annotation_text: { // typically bonds?
      double x, y;
      param_to_screen(&x, &y, mpc_realref(ls->u.text.xy), mpc_imagref(ls->u.text.xy));
      draw_text(ctx, ctxmask, ctxsel, x, y, 0.5, 0.5, 12, true, ls->label);
      break;
    }
    case annotation_nucleus: {
      double x, y;
      param_to_screen(&x, &y, mpc_realref(ls->u.nucleus.xy), mpc_imagref(ls->u.nucleus.xy));
      draw_text(ctx, ctxmask, ctxsel, x, y, 0.5, 0.5, 12, true, ls->label);
      break;
    }
    case annotation_misiurewicz: {
      double x, y;
      param_to_screen(&x, &y, mpc_realref(ls->u.misiurewicz.xy), mpc_imagref(ls->u.misiurewicz.xy));
      draw_text(ctx, ctxmask, ctxsel, x, y, 0.5, 0.5, 12, true, ls->label);
      break;
    }
    case annotation_wake:
      l = ls->u.wake.line_start;
      break;
    case annotation_atom:
      l = ls->u.atom.line_start;
      break;
    case annotation_domain:
      l = ls->u.domain.line_start;
      circle_at_line_end = 1;
      break;
    case annotation_misiurewicz_domain:
      l = ls->u.misiurewicz_domain.line_start;
      circle_at_line_end = 1;
      break;
    case annotation_domain_estimate:
    {
      // FIXME what to do about huge circles?
      double x, y, r;
      param_to_screen
        ( &x
        , &y
        , mpc_realref(ls->u.domain_estimate.c)
        , mpc_imagref(ls->u.domain_estimate.c)
        );
      distance_to_screen(&r, ls->u.domain_estimate.r);
      int inbounds
        =  0 <= x + r && x - r < G.width_screen
        && 0 <= y + r && y - r < G.height_screen;
      int outbounds
        =  inside_circle(x, y, r, 0, 0)
        && inside_circle(x, y, r, G.width_screen, 0)
        && inside_circle(x, y, r, 0, G.height_screen)
        && inside_circle(x, y, r, G.width_screen, G.height_screen);
      if (inbounds)
      {
        if (outbounds)
        {
          // circle surrounds the whole viewport
          cairo_rectangle(ctx, -10, -10, G.width_screen + 20, G.height_screen + 20);
          cairo_rectangle(ctxsel, -10, -10, G.width_screen + 20, G.height_screen + 20);
        }
        else
        {
          // circle intersects with viewport
          cairo_arc(ctx, x, y, r, 0, twopi);
          cairo_arc(ctxsel, x, y, r, 0, twopi);
        }
        fill_and_stroke = 1;
      }
      break;
    }
  }
  struct point *l0 = l;
  int count = 0;
  int anyinbounds = 0;
  if (l0) {
    cairo_set_line_width(ctxsel, 8.0);
    int first = 1;
    double x, y, oldx = 0, oldy = 0;
    int oldinbounds = 0;
    for (; l; l = l->next) {
      count++;
      param_to_screen_clamped(&x, &y, mpc_realref(l->xy), mpc_imagref(l->xy));
      int inbounds = 0 <= x && x < G.width_screen && 0 <= y && y < G.height_screen;
      anyinbounds |= inbounds;
      if (oldinbounds) {
        cairo_line_to(ctx, x, y);
        cairo_line_to(ctxsel, x, y);
      } else {
        if (inbounds) {
          if (first) {
            cairo_move_to(ctx, x, y);
            cairo_move_to(ctxsel, x, y);
          } else {
            cairo_line_to(ctx, oldx, oldy);
            cairo_line_to(ctx, x, y);
            cairo_line_to(ctxsel, oldx, oldy);
            cairo_line_to(ctxsel, x, y);
          }
        } else {
          // segment out of bounds
          cairo_line_to(ctx, x, y);
          cairo_line_to(ctxsel, x, y);
        }
      }
      oldinbounds = inbounds;
      oldx = x;
      oldy = y;
      first = 0;
    }
  }
  if (l0 || fill_and_stroke)
  {
    if (ls->selected)
    {
      cairo_set_dash(ctx, dashes[0].pattern, 0, 0);
    }
    else
    {
      cairo_set_dash(ctx, dashes[style.line_type & 0xF].pattern, dashes[style.line_type & 0xF].count, 0);
    }
    if (style.fill_type >= 0)
    {
      cairo_pattern_t *old = cairo_get_source(ctx);
      cairo_pattern_reference(old);
      if (G.colour == colour_theme_full_colour)
      {
        cairo_set_source_rgba(ctx, style.colour_r, style.colour_g, style.colour_b, 0.25);
        cairo_fill_preserve(ctx);
      }
      double fcr, fcg, fcb;
      if (G.colour == colour_theme_monochrome)
      {
        fcr = fcg = fcb = G.dark;
      }
      else
      {
        fcr = style.colour_r;
        fcg = style.colour_g;
        fcb = style.colour_b;
      }
      cairo_pattern_t *fp = generate_fill_pattern(cairo_get_target(ctx), style.fill_type, fcr, fcg, fcb);
      cairo_set_source(ctx, fp);
      cairo_fill_preserve(ctx);
      cairo_set_source(ctx, old);
      cairo_pattern_destroy(fp);
      cairo_fill_preserve(ctxsel);
    }
    if (G.colour && style.colour_r >= 0)
    {
      cairo_set_source_rgba(ctx, style.colour_r, style.colour_g, style.colour_b, 1);
    }
    cairo_stroke(ctx);
    cairo_stroke(ctxsel);
  }
  if (l0)
  {
    if (circle_at_line_end)
    {
      int i = 0;
      for (l = l0; l; l = l->next)
      {
        if (i % (count / 4) == 0)
        {
          double x, y;
          param_to_screen_clamped(&x, &y, mpc_realref(l->xy), mpc_imagref(l->xy));
          cairo_arc(ctx, x, y, 3.0 * G.dpi_screen / 100, 0, twopi);
          if (i == 0)
            cairo_fill(ctx);
          else
            cairo_stroke(ctx);
          cairo_arc(ctxsel, x, y, 3.0 * G.dpi_screen / 100, 0, twopi);
          if (i == 0)
            cairo_fill(ctx);
          else
            cairo_stroke(ctxsel);
        }
        ++i;
      }
    }
  }
}

void draw_key(cairo_t *ctx, struct annotation *ls, double *key_y)
{
  if (G.dark)
  {
    cairo_set_source_rgba(ctx, 1, 1, 1, 1);
  }
  else
  {
    cairo_set_source_rgba(ctx, 0, 0, 0, 1);
  }
  cairo_set_line_width(ctx, 1);
  struct annotation_style style = ls->style;
  char *label_for_key = 0;
  struct point *l = 0;
  switch (ls->tag)
  {
    case annotation_ray_in:
    {
      style.fill_type = -1;
      label_for_key = m_binangle_to_new_string(&ls->u.ray_in.angle);
      l = ls->u.ray_in.line_start;
      break;
    }
  }
  struct point *l0 = l;
  int anyinbounds = 0;
  if (l0 && label_for_key)
  {
    double x, y;
    for (; l; l = l->next)
    {
      param_to_screen_clamped(&x, &y, mpc_realref(l->xy), mpc_imagref(l->xy));
      int inbounds = 0 <= x && x < G.width_screen && 0 <= y && y < G.height_screen;
      anyinbounds |= inbounds;
    }
    if (anyinbounds)
    {
      const double key_x0 = 10 * G.dpi_screen / 100;
      const double key_x1 = 40 * G.dpi_screen / 100;
      const double key_x2 = 50 * G.dpi_screen / 100;
      cairo_move_to(ctx, key_x0, *key_y);
      cairo_line_to(ctx, key_x1, *key_y);
      cairo_set_dash(ctx, dashes[style.line_type & 0xF].pattern, dashes[style.line_type & 0xF].count, 0);
      if (G.colour && style.colour_r >= 0)
      {
        cairo_set_source_rgba(ctx, style.colour_r, style.colour_g, style.colour_b, 1);
      }
      cairo_stroke(ctx);
      draw_text(ctx, 0, 0, key_x2, *key_y, 0, 0.25, 12, false, label_for_key);
      *key_y += 18 * G.dpi_screen / 100;
    }
  }
  if (label_for_key)
  {
    free(label_for_key);
  }
}

static gboolean configure_event_cb(GtkWidget *widget, GdkEventConfigure *event, gpointer data) {
  (void) event;
  (void) data;
  if (G.surface) {
    return TRUE;
  }
  G.surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, G.width_print, G.height_print);
  G.mask = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, G.width_screen, G.height_screen);
  G.selection_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, G.width_screen, G.height_screen);
  struct view *v = view_copy(G.view);
  start_render(v);
  gtk_widget_queue_draw(widget);
  return TRUE;
}

struct draw_annotation_foreach_data
{
  int selected;
  cairo_t *cr;
  cairo_t *crmask;
  cairo_t *crsel;
};

static gboolean draw_annotation_foreach_func(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer userdata)
{
  (void) path;
  struct draw_annotation_foreach_data *d = userdata;
  gpointer thing;
  gtk_tree_model_get(model, iter, 1, &thing, -1);
  struct annotation *ls = thing;
  if (ls && ls->selected == d->selected && ls->tag != annotation_atom && ls->tag != annotation_wake) {
    draw_annotation(d->cr, d->crmask, d->crsel, ls);
  }
  return FALSE;
}

struct draw_key_foreach_data
{
  cairo_t *cr;
  double key_y;
};

static gboolean draw_key_foreach_func(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer userdata)
{
  (void) path;
  struct draw_key_foreach_data *d = userdata;
  gpointer thing;
  gtk_tree_model_get(model, iter, 1, &thing, -1);
  struct annotation *ls = thing;
  if (ls && ls->tag == annotation_ray_in)
  {
    draw_key(d->cr, ls, &d->key_y);
  }
  return FALSE;
}

static void clip_subtract_loop(cairo_t *cr, struct point *l)
{
  cairo_move_to(cr, -10, -10);
  cairo_line_to(cr, -10, G.height_screen + 10);
  cairo_line_to(cr, G.width_screen + 10, G.height_screen + 10);
  cairo_line_to(cr, G.width_screen + 10, -10);
  cairo_line_to(cr, -10, -10);
  cairo_close_path(cr);
  int first = 1;
  for (; l; l = l->next)
  {
    double x = -10, y = -10;
    param_to_screen_clamped(&x, &y, mpc_realref(l->xy), mpc_imagref(l->xy));
    if (first) {
      cairo_move_to(cr, x, y);
      first = 0;
    }
    else
    {
      cairo_line_to(cr, x, y);
    }
  }
  cairo_close_path(cr);
  cairo_clip(cr);
}

static gboolean draw_key_cb(GtkWidget *widget, cairo_t *cr, gpointer data) {
  (void) widget;
  (void) data;
  if (G.key != key_theme_none)
  {
    cairo_save(cr);
    if (G.key == key_theme_detach)
    {
      if (G.dark)
      {
        cairo_set_source_rgba(cr, 0, 0, 0, 1);
      }
      else
      {
        cairo_set_source_rgba(cr, 1, 1, 1, 1);
      }
      cairo_paint(cr);
    }
    struct draw_key_foreach_data userdata = { cr, 15 * G.dpi_screen / 100 };
    gtk_tree_model_foreach(GTK_TREE_MODEL(G.annostore), draw_key_foreach_func, &userdata);
    cairo_restore(cr);
  }
  return FALSE;
}

static gboolean draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data) {
  (void) widget;
  (void) data;
  cairo_save(cr);
  cairo_save(cr);
  cairo_scale(cr, G.width_screen / (double) G.width_print, G.height_screen / (double) G.height_print);
  cairo_set_source_surface(cr, G.surface, 0, 0);
  cairo_paint(cr);
  cairo_restore(cr);
  if (G.box) {
    draw_box(cr);
  }
  if (G.ball) {
    draw_ball(cr);
  }
  cairo_t *crmask = cairo_create(G.mask);
  cairo_set_source_rgba(crmask, 0, 0, 0, 1);
  cairo_paint(crmask);
  cairo_set_source_rgba(crmask, 1, 1, 1, 1);
  cairo_t *crsel = cairo_create(G.selection_surface);
  cairo_set_source_rgba(crsel, 0, 0, 0, 1);
  cairo_paint(crsel);
  cairo_set_antialias(crsel, CAIRO_ANTIALIAS_NONE);
  for (int selected = 1; selected >= 0; --selected)
  {
    struct draw_annotation_foreach_data userdata = { selected, cr, crmask, crsel };
    gtk_tree_model_foreach(GTK_TREE_MODEL(G.annostore), draw_annotation_foreach_func, &userdata);
  }

  cairo_set_fill_rule(cr, CAIRO_FILL_RULE_EVEN_ODD);
  cairo_set_fill_rule(crsel, CAIRO_FILL_RULE_EVEN_ODD);
  // draw atoms
  struct annotation *as = G.anno;
  while (as)
  {
    if (as->tag == annotation_atom)
    {
      draw_annotation(cr, crmask, crsel, as);
      clip_subtract_loop(cr, as->u.atom.line_start);
      clip_subtract_loop(crsel, as->u.atom.line_start);
    }
    as = as->next;
  }
  // draw wakes from narrowest to widest
  struct annotation *ws = G.wakes_head.u.wake.next_wake;
  while (ws->u.wake.next_wake)
  {
    draw_annotation(cr, crmask, crsel, ws);
    clip_subtract_loop(cr, ws->u.wake.line_start);
    clip_subtract_loop(crsel, ws->u.wake.line_start);
    ws = ws->u.wake.next_wake;
  }
  cairo_destroy(crmask);
  cairo_destroy(crsel);
  cairo_restore(cr);
  if (widget && G.key == key_theme_overlay)
  {
    draw_key_cb(widget, cr, data);
  }
  return FALSE;
}

static void save_png(const char *filename) {
  cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, G.width_print, G.height_print);
  cairo_t *cr = cairo_create(surface);
  cairo_scale(cr, G.width_print / (double) G.width_screen, G.height_print / (double) G.height_screen);
  draw_cb(0, cr, 0);
  if (G.key == key_theme_overlay)
  {
    draw_key_cb(0, cr, 0);
  }
  cairo_surface_write_to_png(surface, filename);
  if (G.key == key_theme_detach)
  {
    draw_key_cb(0, cr, 0);
    char *keyfilename = calloc(1, strlen(filename) + 10);
    if (keyfilename)
    {
      strcpy(keyfilename, filename);
      char *dot = strrchr(keyfilename, '.');
      if (dot)
      {
        *dot = 0;
      }
      strcat(keyfilename, ".key.png");
      cairo_surface_write_to_png(surface, keyfilename);
      free(keyfilename);
    }
  }
  cairo_destroy(cr);
  cairo_surface_destroy(surface);
}

static void save_pdf(const char *filename) {
  double w = 210 / 25.4 * 72;
  double h = 297 / 25.4 * 72;
  // double gw = (210 - 40) / 25.4 * 72;
  double gh = (297 - 40) / 25.4 * 72;
  double s = gh / G.width_screen;
  cairo_surface_t *pdf = cairo_pdf_surface_create(filename, w, h);
  cairo_t *cr = cairo_create(pdf);
  cairo_translate(cr, w / 2, h / 2);
  cairo_rotate(cr, -pi / 2);
  cairo_scale(cr, s, s);
  cairo_translate(cr, -G.width_screen / 2, -G.height_screen / 2);
  cairo_rectangle(cr, 0, 0, G.width_screen, G.height_screen);
  cairo_clip(cr);
  draw_cb(0, cr, 0);
  if (G.key == key_theme_overlay)
  {
    draw_key_cb(0, cr, 0);
  }
  cairo_show_page(cr);
  if (G.key == key_theme_detach)
  {
    draw_key_cb(0, cr, 0);
    cairo_show_page(cr);
  }
  cairo_destroy(cr);
  cairo_surface_destroy(pdf);
}

static void save_svg(const char *filename) {
  double w = (297 - 40) / 25.4 * 72;
  double h = (210 - 40) / 25.4 * 72;
  double s = w / G.width_screen;
  cairo_surface_t *svg = cairo_svg_surface_create(filename, w, h);
  cairo_t *cr = cairo_create(svg);
  cairo_scale(cr, s, s);
  cairo_rectangle(cr, 0, 0, G.width_screen, G.height_screen);
  cairo_clip(cr);
  draw_cb(0, cr, 0);
  if (G.key == key_theme_overlay)
  {
    draw_key_cb(0, cr, 0);
  }
  cairo_destroy(cr);
  cairo_surface_flush(svg);
  cairo_surface_destroy(svg);
  if (G.key == key_theme_detach)
  {
    char *keyfilename = calloc(1, strlen(filename) + 10);
    if (keyfilename)
    {
      strcpy(keyfilename, filename);
      char *dot = strrchr(keyfilename, '.');
      if (dot)
      {
        *dot = 0;
      }
      strcat(keyfilename, ".key.svg");
      svg = cairo_svg_surface_create(keyfilename, w, h);
      cr = cairo_create(svg);
      cairo_scale(cr, s, s);
      cairo_rectangle(cr, 0, 0, G.width_screen, G.height_screen);
      cairo_clip(cr);
      draw_key_cb(0, cr, 0);
      cairo_destroy(cr);
      cairo_surface_flush(svg);
      cairo_surface_destroy(svg);
      free(keyfilename);
    }
  }
}

static void close_window(void) {
  if (G.surface) {
    cairo_surface_destroy(G.surface);
  }
  gtk_main_quit();
}

static void save_clicked_cb(GtkToolButton *save, gpointer userdata) {
  (void) save;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    serialize_toml(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

static void save_image_clicked_cb(GtkToolButton *save_image, gpointer userdata) {
  (void) save_image;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Save PNG", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    save_png(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

static void save_pdf_clicked_cb(GtkToolButton *save_image, gpointer userdata) {
  (void) save_image;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Save PDF", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    save_pdf(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

static void save_svg_clicked_cb(GtkToolButton *save_image, gpointer userdata) {
  (void) save_image;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Save SVG", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    save_svg(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

int load_file(const char *filename)
{
  int retval = deserialize_toml(filename);
  if (retval)
  {
    retval = deserialize_old(filename);
  }
  return retval;
}

static void load_clicked_cb(GtkToolButton *load, gpointer userdata) {
  (void) load;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Load File", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    load_file(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

#if 0
static void resize_clicked_cb(GtkToolButton *resize, gpointer userdata) {
  (void) resize;
  (void) userdata;
  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("Size", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Resize", GTK_RESPONSE_ACCEPT, NULL);
  GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
  GtkWidget *entryw = gtk_entry_new();
  GtkWidget *entryh = gtk_entry_new();
  char *labelw = malloc(100);
  snprintf(labelw, 99, "%d", G.width_screen);
  char *labelh = malloc(100);
  snprintf(labelh, 99, "%d", G.height_screen);
  gtk_entry_set_text(GTK_ENTRY(entryw), labelw);
  gtk_entry_set_text(GTK_ENTRY(entryh), labelh);
  gtk_container_add(GTK_CONTAINER(box), entryw);
  gtk_container_add(GTK_CONTAINER(box), entryh);
  gtk_widget_show_all(dialog);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    const char *textw = gtk_entry_get_text(GTK_ENTRY(entryw));
    int w = atoi(textw);
    const char *texth = gtk_entry_get_text(GTK_ENTRY(entryh));
    int h = atoi(texth);
    if (w > 0 && h > 0) {
      resize_image(w, h);
    }
  }
  free(labelw);
  free(labelh);
  gtk_widget_destroy(dialog);
}
#endif

static void home_clicked_cb(GtkToolButton *home, gpointer userdata) {
  (void) home;
  (void) userdata;
  struct view *v = view_new();
  mpfr_set_d(v->cx, -0.75, GMP_RNDN);
  mpfr_set_d(v->cy, 0.0, GMP_RNDN);
  mpfr_set_d(v->radius, 1.5, GMP_RNDN);
  start_render(v);
}

static void zoom_out_clicked_cb(GtkToolButton *zoom_out, gpointer userdata) {
  (void) zoom_out;
  (void) userdata;
  struct view *v = view_copy(G.view);
  mpfr_mul_d(v->radius, v->radius, 10.0, GMP_RNDN);
  start_render(v);
}

static void wakel_clicked_cb(GtkToolButton *wakel, gpointer userdata) {
  (void) wakel;
  (void) userdata;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_ray_in)
  {
    start_wake(anno, 1);
  }
}

static void waker_clicked_cb(GtkToolButton *waker, gpointer userdata) {
  (void) waker;
  (void) userdata;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_ray_in)
  {
    start_wake(anno, -1);
  }
}


static void more_iters_clicked_cb(GtkToolButton *more_iters, gpointer userdata) {
  (void) more_iters;
  (void) userdata;
  if (G.maximum_iterations < (1 << 30))
  {
    G.maximum_iterations *= 2;
    struct view *v = view_copy(G.view);
    start_render(v);
  }
}

static void fewer_iters_clicked_cb(GtkToolButton *fewer_iters, gpointer userdata) {
  (void) fewer_iters;
  (void) userdata;
  if (G.maximum_iterations > (1 << 8))
  {
    G.maximum_iterations /= 2;
    struct view *v = view_copy(G.view);
    start_render(v);
  }
}

static gboolean zoom_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    G.box = TRUE;
    G.box_x1 = event->x;
    G.box_y1 = event->y;
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  } else {
    G.box = FALSE;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static gboolean zoom_button_release_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    if (G.box) {
      if (G.view) {
        double x, y, w, h;
        box_rectangle(&x, &y, &w, &h);
        if (w > 0 && h > 0) {
          struct view *v = view_new();
          x = (x + w/2.0 - G.width_screen / 2.0) / (G.height_screen / 2.0);
          y = (G.height_screen / 2.0 - (y + h/2.0)) / (G.height_screen / 2.0);
          double r = (h/2.0) / (G.height_screen / 2.0);
          mpfr_t clickx, clicky, logradius;
          mpfr_init2(clickx, 53);
          mpfr_init2(clicky, 53);
          mpfr_init2(logradius, 53);
          mpfr_set_d(clickx, x, GMP_RNDN);
          mpfr_set_d(clicky, y, GMP_RNDN);
          mpfr_mul(clickx, clickx, G.view->radius, GMP_RNDN);
          mpfr_mul(clicky, clicky, G.view->radius, GMP_RNDN);
          mpfr_mul_d(v->radius, G.view->radius, r, GMP_RNDN);
          mpfr_log2(logradius, v->radius, GMP_RNDN);
          int prec = fmax(53, 16 - mpfr_get_d(logradius, GMP_RNDN));
          mpfr_prec_round(v->cx, prec, GMP_RNDN);
          mpfr_prec_round(v->cy, prec, GMP_RNDN);
          mpfr_add(v->cx, G.view->cx, clickx, GMP_RNDN);
          mpfr_add(v->cy, G.view->cy, clicky, GMP_RNDN);
          mpfr_clear(clickx);
          mpfr_clear(clicky);
          mpfr_clear(logradius);
          start_render(v);
        }
      }
      G.box = FALSE;
      gtk_widget_queue_draw (widget);
    }
  }
  return TRUE;
}

static gboolean zoom_motion_notify_event_cb(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
  (void) data;
  if (event->state & GDK_BUTTON1_MASK) {
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void zoom_toggled_cb(GtkWidget *zoom, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(zoom));
  if (active) {
    G.box_aspect = G.width_screen / (double) G.height_screen;
    G.zoom_motion_notify_handler = g_signal_connect(G.da, "motion-notify-event", G_CALLBACK(zoom_motion_notify_event_cb), NULL);
    G.zoom_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(zoom_button_press_event_cb), NULL);
    G.zoom_button_release_handler = g_signal_connect(G.da, "button-release-event", G_CALLBACK(zoom_button_release_event_cb), NULL);
  } else {
    if (G.zoom_motion_notify_handler) {
      g_signal_handler_disconnect(G.da, G.zoom_motion_notify_handler);
    }
    if (G.zoom_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.zoom_button_press_handler);
    }
    if (G.zoom_button_release_handler) {
      g_signal_handler_disconnect(G.da, G.zoom_button_release_handler);
    }
    G.zoom_motion_notify_handler = 0;
    G.zoom_button_press_handler = 0;
    G.zoom_button_release_handler = 0;
  }
}

static void info_update(double x, double y) {
  int i = x * G.width_print / G.width_screen, j = y * G.height_print / G.height_screen;
  if (G.image && 0 <= i && i < G.width_print && 0 <= j && j < G.height_print) {
    mpfr_t cx, cy;
    mpfr_init2(cx, 53);
    mpfr_init2(cy, 53);
    screen_to_param(x, y, cx, cy);
    int k = j * G.width_print + i;
    char *buf = calloc(1, 65536);
    mpfr_snprintf(buf, 65536,
      "\nINFO\n"
      "pixel: %d %d\n"
      "real: %Re\n"
      "imag: %Re\n"
      "dwell_n: %d\n"
      "dwell_f: %g\n"
      "angle_f: %g\n"
      "distance: %g\n"
      "domain_n: %d\n"
      "domain_x: %g\n"
      "domain_y: %g\n"
      , i, j, cx, cy
      , perturbator_get_dwell_n(G.image)[k]
      , perturbator_get_dwell_f(G.image)[k]
      , perturbator_get_dwell_a(G.image)[k]
      , perturbator_get_distance(G.image)[k]
      , perturbator_get_domain_n(G.image)[k]
      , perturbator_get_domain_x(G.image)[k]
      , perturbator_get_domain_y(G.image)[k]
      );
    log_append(buf);
    free(buf);
    mpfr_clear(cx);
    mpfr_clear(cy);
  }
}

#if 0
static double compute_minimum_exterior_dwell(void)
{
  const int32_t *dwell_n = perturbator_get_dwell_n(G.image);
  const float   *dwell_f = perturbator_get_dwell_f(G.image);
  double dwell = 1.0 / 0.0;
  for (int i = 0; i < G.width_print; ++i)
  {
    for (int j = 0; j < G.height_print; j += G.height_print- 1)
    {
      int k = j * G.width_print + i;
      double n = dwell_n[k];
      double f = dwell_f[k];
      if (n > 0)
      {
        dwell = fmin(dwell, n + f);
      }
    }
  }
  for (int i = 0; i < G.width_print; i += G.width_print - 1)
  {
    for (int j = 0; j < G.height_print; ++j)
    {
      int k = j * G.width_print + i;
      double n = dwell_n[k];
      double f = dwell_f[k];
      if (n > 0)
      {
        dwell = fmin(dwell, n + f);
      }
    }
  }
  return dwell;
}
#endif

static gboolean info_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) widget;
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    info_update(event->x, event->y);
  }
  return TRUE;
}

static void info_toggled_cb(GtkWidget *info, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(info));
  if (active) {
    G.info_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(info_button_press_event_cb), NULL);
  } else {
    if (G.info_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.info_button_press_handler);
    }
    G.info_button_press_handler = 0;
  }
}

static void selection_update(double x, double y) {
  int i = x, j = y;
  cairo_surface_flush(G.selection_surface);
  uint32_t *pixels = (uint32_t *) cairo_image_surface_get_data(G.selection_surface);
  int channels = 4;
  int stride = cairo_image_surface_get_stride(G.selection_surface) / channels;
  if (0 <= i && i < G.width_screen && 0 <= j && j < G.height_screen)
  {
    int id = pixels[j * stride + i] & 0x00ffffff;
    struct annotation *ls = G.anno;
    while (ls)
    {
      ls = ls->next;
    }
    GtkTreeSelection *ts = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
    gtk_tree_selection_unselect_all(ts);
    GtkTreeModel *model = GTK_TREE_MODEL(G.annostore);
    GtkTreeIter iter;
    if (gtk_tree_model_get_iter_first(model, &iter))
    {
      do
      {
        gpointer thing = 0;
        gtk_tree_model_get(model, &iter, 1, &thing, -1);
        if (thing)
        {
          struct annotation *ls = thing;
          ls->selected = (ls->id == id);
          if (ls->selected)
          {
            gtk_tree_selection_select_iter(ts, &iter);
          }
        }
      } while(gtk_tree_model_iter_next(model, &iter));
    }
  }
}


static gboolean selection_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) widget;
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    selection_update(event->x, event->y);
  }
  return TRUE;
}

static void selection_toggled_cb(GtkWidget *sel, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(sel));
  if (active) {
    G.selection_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(selection_button_press_event_cb), NULL);
  } else {
    if (G.selection_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.selection_button_press_handler);
    }
    G.selection_button_press_handler = 0;
  }
}

static gboolean ray_out_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    start_ray_out(event->x, event->y);
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void ray_out_toggled_cb(GtkWidget *ray_out, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(ray_out));
  if (active) {
    G.ray_out_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(ray_out_button_press_event_cb), NULL);
  } else {
    if (G.ray_out_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.ray_out_button_press_handler);
    }
    G.ray_out_button_press_handler = 0;
  }
}

static void ray_in_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  const char *label = "";
  struct annotation *anno = get_selected_annotation();
  if (anno)
  {
    label = anno->label;
  }
  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("Ray In", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Ray In", GTK_RESPONSE_ACCEPT, NULL);
  GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
  GtkWidget *aentry = gtk_entry_new();
  GtkWidget *ppentry = gtk_entry_new();
  GtkWidget *pentry = gtk_entry_new();
  GtkWidget *dentry = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(aentry), "Angle");
  gtk_entry_set_placeholder_text(GTK_ENTRY(ppentry), "Pre-period");
  gtk_entry_set_placeholder_text(GTK_ENTRY(pentry), "Period");
  gtk_entry_set_placeholder_text(GTK_ENTRY(dentry), "Depth");
  gtk_entry_set_text(GTK_ENTRY(aentry), label);
  gtk_entry_set_text(GTK_ENTRY(dentry), "200");
  gtk_container_add(GTK_CONTAINER(box), aentry);
  gtk_container_add(GTK_CONTAINER(box), ppentry);
  gtk_container_add(GTK_CONTAINER(box), pentry);
  gtk_container_add(GTK_CONTAINER(box), dentry);
  gtk_widget_show_all(dialog);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    const char *atext = gtk_entry_get_text(GTK_ENTRY(aentry));
    const char *pptext = gtk_entry_get_text(GTK_ENTRY(ppentry));
    const char *ptext = gtk_entry_get_text(GTK_ENTRY(pentry));
    const char *dtext = gtk_entry_get_text(GTK_ENTRY(dentry));
    int pp = atoi(pptext);
    int p = atoi(ptext);
    struct m_binangle *angle = calloc(1, sizeof(*angle));
    m_binangle_init(angle);
    const char *result;
    char *atext2 = 0;
    if (p > 0 && strlen(atext) >= (size_t) (pp + p))
    {
      atext2 = calloc(1, 1 + pp + 1 + p + 1 + 1);
      int k = 0;
      atext2[k++] = '.';
      for (int j = 0; j < pp; ++j)
      {
        atext2[k++] = atext[j];
      }
      atext2[k++] = '(';
      for (int j = pp; j < pp + p; ++j)
      {
        atext2[k++] = atext[j];
      }
      atext2[k++] = ')';
      atext2[k++] = 0;
      result = m_binangle_from_string(angle, atext2);
    }
    else
    {
      result = m_binangle_from_string(angle, atext);
    }
    int depth = atoi(dtext);
    if (angle && result && *result == 0 && depth > 0) {
      start_ray_in(angle, depth);
    }
    if (atext2) free(atext2);
  }
  gtk_widget_destroy(dialog);
}

static void rays_of_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer thing;
  struct annotation *anno = 0;
  if (gtk_tree_selection_get_selected(select, &model, &iter))
  {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing)
    {
      anno = (struct annotation *) thing;
    }
  }
  int count = atoi(gtk_entry_get_text(GTK_ENTRY(G.rays_of_nrays)));
  int period = atoi(gtk_entry_get_text(GTK_ENTRY(G.filaments_period)));
  if (anno)
  {
    switch (anno->tag)
    {
      case annotation_nucleus:
        start_rays_of(2, 0, anno->u.nucleus.period, anno->u.nucleus.xy);
        break;
      case annotation_atom:
        start_rays_of(2, 0, anno->u.atom.period, anno->u.atom.nucleus);
        break;
      case annotation_domain:
        start_rays_of(2, 0, anno->u.domain.period, anno->u.domain.nucleus);
        break;
      case annotation_domain_estimate:
        start_rays_of(anno->u.domain_estimate.preperiod == 0 ? 2 : count, anno->u.domain_estimate.preperiod == 0 ? 0 : anno->u.domain_estimate.preperiod - 1, period ? period : anno->u.domain_estimate.period, anno->u.domain_estimate.c);
        break;
      case annotation_misiurewicz_domain:
        start_rays_of(count, anno->u.misiurewicz_domain.hipreperiod - 1, period ? period : anno->u.misiurewicz_domain.period, anno->u.misiurewicz_domain.misiurewicz);
        break;
      case annotation_misiurewicz:
        start_rays_of(count, anno->u.misiurewicz.preperiod - 1, period ? period : anno->u.misiurewicz.period, anno->u.misiurewicz.xy);
        break;
      case annotation_ray_in:
        start_rays_of(anno->u.ray_in.angle.pre.length == 0 ? 2 : count, anno->u.ray_in.angle.pre.length, anno->u.ray_in.angle.per.length, anno->u.ray_in.line_end->xy);
        break;
    }
  }
}

static void extend_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer thing;
  struct annotation *anno = 0;
  if (gtk_tree_selection_get_selected(select, &model, &iter))
  {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing)
    {
      anno = (struct annotation *) thing;
    }
  }
  if (anno && anno->tag == annotation_ray_in) {
    GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
    GtkWidget *dialog = gtk_dialog_new_with_buttons("Extend", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Extend", GTK_RESPONSE_ACCEPT, NULL);
    GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
    GtkWidget *dentry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(dentry), "1000");
    gtk_container_add(GTK_CONTAINER(box), dentry);
    gtk_widget_show_all(dialog);
    gint res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
      const char *dtext = gtk_entry_get_text(GTK_ENTRY(dentry));
      int depth = atoi(dtext);
      if (depth > 0) {
        gtk_tree_store_remove(G.annostore, &iter);
        start_ray_extend(anno, depth);
      }
    }
    gtk_widget_destroy(dialog);
  }
}

static void zoom_to_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_nucleus) {
    GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
    GtkWidget *dialog = gtk_dialog_new_with_buttons("Zoom To", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Zoom To", GTK_RESPONSE_ACCEPT, NULL);
    GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
    GtkWidget *dpower = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(dpower), "1.0");
    GtkWidget *dfactor = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(dfactor), "4.0");
    gtk_container_add(GTK_CONTAINER(box), dpower);
    gtk_container_add(GTK_CONTAINER(box), dfactor);
    gtk_widget_show_all(dialog);
    gint res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
      mpfr_t factor;
      mpfr_init2(factor, 53);
      struct view *v = view_new();
      const char *dtext = gtk_entry_get_text(GTK_ENTRY(dpower));
      mpfr_set_str(factor, dtext, 0, MPFR_RNDN);
      mpfr_pow(v->radius, anno->u.nucleus.size, factor, MPFR_RNDN);
      dtext = gtk_entry_get_text(GTK_ENTRY(dfactor));
      mpfr_set_str(factor, dtext, 0, MPFR_RNDN);
      mpfr_mul(v->radius, v->radius, factor, MPFR_RNDN);
      gtk_widget_destroy(dialog);
      mpfr_clear(factor);
      mpfr_set_prec(v->cx, 53 - mpfr_get_exp(v->radius));
      mpfr_set_prec(v->cy, 53 - mpfr_get_exp(v->radius));
      mpfr_set(v->cx, mpc_realref(anno->u.nucleus.xy), MPFR_RNDN);
      mpfr_set(v->cy, mpc_imagref(anno->u.nucleus.xy), MPFR_RNDN);
      start_render(v);
    } else {
      gtk_widget_destroy(dialog);
    }
  }
}

static void zoom_to_domain_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && (anno->tag == annotation_nucleus || anno->tag == annotation_misiurewicz)) {
    GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
    GtkWidget *dialog = gtk_dialog_new_with_buttons("Zoom To", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Zoom To", GTK_RESPONSE_ACCEPT, NULL);
    GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
    GtkWidget *dpower = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(dpower), "1.0");
    GtkWidget *dfactor = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(dfactor), "4.0");
    gtk_container_add(GTK_CONTAINER(box), dpower);
    gtk_container_add(GTK_CONTAINER(box), dfactor);
    gtk_widget_show_all(dialog);
    gint res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
      struct view *v = view_new();
      mpfr_t factor;
      mpfr_init2(factor, 53);
      const char *dtext = gtk_entry_get_text(GTK_ENTRY(dpower));
      mpfr_set_str(factor, dtext, 0, MPFR_RNDN);
      mpfr_pow
        ( v->radius
        , anno->tag == annotation_nucleus
        ? anno->u.nucleus.domain_size
        : anno->u.misiurewicz.domain_size
        , factor
        , MPFR_RNDN
        );
      dtext = gtk_entry_get_text(GTK_ENTRY(dfactor));
      mpfr_set_str(factor, dtext, 0, MPFR_RNDN);
      mpfr_mul(v->radius, v->radius, factor, MPFR_RNDN);
      gtk_widget_destroy(dialog);
      mpfr_clear(factor);
      mpfr_set_prec(v->cx, 53 - mpfr_get_exp(v->radius));
      mpfr_set_prec(v->cy, 53 - mpfr_get_exp(v->radius));
      mpc_t *c
        = anno->tag == annotation_nucleus
        ? &anno->u.nucleus.xy
        : &anno->u.misiurewicz.xy
        ;
      mpfr_set(v->cx, mpc_realref(*c), MPFR_RNDN);
      mpfr_set(v->cy, mpc_imagref(*c), MPFR_RNDN);
      start_render(v);
    } else {
      gtk_widget_destroy(dialog);
    }
  }
}

static void muunit_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_nucleus) {
    task_enqueue_cb(task_new_muunit(G.annotation_queue, anno->u.nucleus.xy, anno->u.nucleus.period, &G.style));
  }
}

static void atom_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_nucleus) {
    start_atom(anno->u.nucleus.xy, anno->u.nucleus.period);
  }
}

static void domain_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_nucleus) {
    int loperiod = atoi(gtk_entry_get_text(GTK_ENTRY(G.filaments_period)));
    start_domain(anno->u.nucleus.xy, anno->u.nucleus.period, loperiod);
  }
  if (anno && anno->tag == annotation_misiurewicz) {
    int lopreperiod = atoi(gtk_entry_get_text(GTK_ENTRY(G.filaments_period)));
    start_misiurewicz_domain(anno->u.misiurewicz.xy, anno->u.misiurewicz.period, lopreperiod, anno->u.misiurewicz.preperiod);
  }
}

static void domain_estimate_clicked_cb(GtkToolButton *toolbutton, gpointer user_data)
{
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && (anno->tag == annotation_nucleus || anno->tag == annotation_misiurewicz))
  {
    char *label = malloc(100);
    mpc_t *c;
    mpfr_t *r;
    if (anno->tag == annotation_nucleus)
    {
      c = &anno->u.nucleus.xy;
      r = &anno->u.nucleus.domain_size;
      snprintf(label, 100, "p%d DE", anno->u.nucleus.period);
      start_domain_estimate(*c, *r, 0, anno->u.nucleus.period, label);
    }
    else
    {
      c = &anno->u.misiurewicz.xy;
      r = &anno->u.misiurewicz.domain_size;
      snprintf(label, 100, "%dp%d DE", anno->u.misiurewicz.preperiod, anno->u.misiurewicz.period);
      start_domain_estimate(*c, *r, anno->u.misiurewicz.preperiod, anno->u.misiurewicz.period, label);
    }
    free(label);
  }
}

static void filaments_clicked_cb(GtkToolButton *toolbutton, gpointer user_data)
{
  (void) toolbutton;
  (void) user_data;
  struct annotation *anno = get_selected_annotation();
  if (anno && anno->tag == annotation_nucleus) {
    int preperiod = atoi(gtk_entry_get_text(GTK_ENTRY(G.filaments_preperiod)));
    int outer_period = atoi(gtk_entry_get_text(GTK_ENTRY(G.filaments_period)));
    int depth = atoi(gtk_entry_get_text(GTK_ENTRY(G.filaments_depth)));
    task_enqueue_cb(task_new_filaments(G.annotation_queue, anno->u.nucleus.xy, anno->u.nucleus.period, outer_period, preperiod, depth, &G.style));
  }
}

static gboolean nucleus_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    G.ball = TRUE;
    G.ball_x1 = event->x;
    G.ball_y1 = event->y;
    G.ball_x2 = event->x;
    G.ball_y2 = event->y;
    gtk_widget_queue_draw(widget);
  } else {
    G.ball = FALSE;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static gboolean nucleus_button_release_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    if (G.ball) {
      if (G.view) {
        double x, y, r;
        ball_circle(&x, &y, &r);
        if (r > 0) {
          int i = x * G.width_print / G.width_screen, j = y * G.height_print / G.height_screen;
          int dwell = G.maximum_iterations;
          if (0 <= i && i < G.width_print && 0 <= j && j < G.height_print)
          {
            int k = j * G.width_print + i;
            double dwelln = perturbator_get_dwell_n(G.image)[k];
            double dwellf = perturbator_get_dwell_f(G.image)[k];
            if (! (dwelln <= 0 || dwellf <= 0))
            {
              dwell = floor(dwelln + dwellf);
            }
          }
          x = (x - G.width_screen / 2.0) / (G.height_screen / 2.0);
          y = (G.height_screen / 2.0 - y) / (G.height_screen / 2.0);
          r = r / (G.height_screen / 2.0);
          mpfr_t clickx, clicky, clickr, logradius;
          mpfr_init2(clickx, 53);
          mpfr_init2(clicky, 53);
          mpfr_init2(clickr, 53);
          mpfr_init2(logradius, 53);
          mpfr_set_d(clickx, x, GMP_RNDN);
          mpfr_set_d(clicky, y, GMP_RNDN);
          mpfr_mul(clickx, clickx, G.view->radius, GMP_RNDN);
          mpfr_mul(clicky, clicky, G.view->radius, GMP_RNDN);
          mpfr_mul_d(clickr, G.view->radius, r, GMP_RNDN);
          mpfr_log2(logradius, clickr, GMP_RNDN);
          int prec = fmax(53, 16 - mpfr_get_d(logradius, GMP_RNDN));
          mpfr_prec_round(clickx, prec, GMP_RNDN);
          mpfr_prec_round(clicky, prec, GMP_RNDN);
          mpfr_add(clickx, G.view->cx, clickx, GMP_RNDN);
          mpfr_add(clicky, G.view->cy, clicky, GMP_RNDN);
          mpfr_clear(logradius);
          start_nucleus(clickx, clicky, clickr, dwell);
          mpfr_clear(clickx);
          mpfr_clear(clicky);
          mpfr_clear(clickr);
        }
      }
      G.ball = FALSE;
      gtk_widget_queue_draw(widget);
    }
  }
  return TRUE;
}

static gboolean nucleus_motion_notify_event_cb(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
  (void) data;
  if (event->state & GDK_BUTTON1_MASK) {
    G.ball_x2 = event->x;
    G.ball_y2 = event->y;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void nucleus_toggled_cb(GtkWidget *nucleus, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(nucleus));
  if (active) {
    G.nucleus_motion_notify_handler = g_signal_connect(G.da, "motion-notify-event", G_CALLBACK(nucleus_motion_notify_event_cb), NULL);
    G.nucleus_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(nucleus_button_press_event_cb), NULL);
    G.nucleus_button_release_handler = g_signal_connect(G.da, "button-release-event", G_CALLBACK(nucleus_button_release_event_cb), NULL);
  } else {
    if (G.nucleus_motion_notify_handler) {
      g_signal_handler_disconnect(G.da, G.nucleus_motion_notify_handler);
    }
    if (G.nucleus_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.nucleus_button_press_handler);
    }
    if (G.nucleus_button_release_handler) {
      g_signal_handler_disconnect(G.da, G.nucleus_button_release_handler);
    }
    G.nucleus_motion_notify_handler = 0;
    G.nucleus_button_press_handler = 0;
    G.nucleus_button_release_handler = 0;
  }
}

static void start_misiurewicz(mpfr_t x, mpfr_t y, mpfr_t r, int maxpreperiod, int maxperiod)
{
  mpc_t c;
  mpc_init2(c, mpfr_get_prec(x));
  mpc_set_fr_fr(c, x, y, MPC_RNDNN);
  struct task *t = task_new_misiurewicz(G.annotation_queue, c, r, maxpreperiod, maxperiod, &G.style);
  mpc_clear(c);
  task_enqueue_cb(t);
}

static gboolean misiurewicz_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    G.box = TRUE;
    G.box_x1 = event->x;
    G.box_y1 = event->y;
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  } else {
    G.box = FALSE;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static gboolean misiurewicz_button_release_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    if (G.box) {
      if (G.view) {
        double x, y, w, h;
        box_rectangle(&x, &y, &w, &h);
        if (w > 0 && h > 0) {
          int i = (x + w/2.0) * G.width_print / G.width_screen, j = (y + h/2.0) * G.height_print / G.height_screen;
          int dwell = G.maximum_iterations;
          if (0 <= i && i < G.width_print && 0 <= j && j < G.height_print)
          {
            int k = j * G.width_print + i;
            double dwelln = perturbator_get_dwell_n(G.image)[k];
            double dwellf = perturbator_get_dwell_f(G.image)[k];
            if (! (dwelln <= 0 || dwellf <= 0))
            {
              dwell = floor(dwelln + dwellf);
            }
          }
          x = (x + w/2.0 - G.width_screen / 2.0) / (G.height_screen / 2.0);
          y = (G.height_screen / 2.0 - (y + h/2.0)) / (G.height_screen / 2.0);
          double r = (h/2.0) / (G.height_screen / 2.0);
          mpfr_t clickx, clicky, clickr, logradius;
          mpfr_init2(clickx, 53);
          mpfr_init2(clicky, 53);
          mpfr_init2(clickr, 53);
          mpfr_init2(logradius, 53);
          mpfr_set_d(clickx, x, GMP_RNDN);
          mpfr_set_d(clicky, y, GMP_RNDN);
          mpfr_mul(clickx, clickx, G.view->radius, GMP_RNDN);
          mpfr_mul(clicky, clicky, G.view->radius, GMP_RNDN);
          mpfr_mul_d(clickr, G.view->radius, r, GMP_RNDN);
          mpfr_log2(logradius, clickr, GMP_RNDN);
          int prec = fmax(53, 16 - mpfr_get_d(logradius, GMP_RNDN));
          mpfr_prec_round(clickx, prec, GMP_RNDN);
          mpfr_prec_round(clicky, prec, GMP_RNDN);
          mpfr_add(clickx, G.view->cx, clickx, GMP_RNDN);
          mpfr_add(clicky, G.view->cy, clicky, GMP_RNDN);
          mpfr_clear(logradius);
          const char *txt = gtk_entry_get_text(GTK_ENTRY(G.filaments_period));
          int maxperiod = atoi(txt);
          start_misiurewicz(clickx, clicky, clickr, dwell, maxperiod > 0 ? maxperiod : dwell);
          mpfr_clear(clickx);
          mpfr_clear(clicky);
          mpfr_clear(clickr);
        }
      }
      G.box = FALSE;
      gtk_widget_queue_draw(widget);
    }
  }
  return TRUE;
}

static gboolean misiurewicz_motion_notify_event_cb(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
  (void) data;
  if (event->state & GDK_BUTTON1_MASK) {
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void misiurewicz_toggled_cb(GtkWidget *misiurewicz, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(misiurewicz));
  if (active) {
    G.box_aspect = 1.0;
    G.misiurewicz_motion_notify_handler = g_signal_connect(G.da, "motion-notify-event", G_CALLBACK(misiurewicz_motion_notify_event_cb), NULL);
    G.misiurewicz_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(misiurewicz_button_press_event_cb), NULL);
    G.misiurewicz_button_release_handler = g_signal_connect(G.da, "button-release-event", G_CALLBACK(misiurewicz_button_release_event_cb), NULL);
  } else {
    if (G.misiurewicz_motion_notify_handler) {
      g_signal_handler_disconnect(G.da, G.misiurewicz_motion_notify_handler);
    }
    if (G.misiurewicz_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.misiurewicz_button_press_handler);
    }
    if (G.misiurewicz_button_release_handler) {
      g_signal_handler_disconnect(G.da, G.misiurewicz_button_release_handler);
    }
    G.misiurewicz_motion_notify_handler = 0;
    G.misiurewicz_button_press_handler = 0;
    G.misiurewicz_button_release_handler = 0;
  }
}

// bond

static gboolean bond_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) widget;
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    int i = event->x * G.width_print / G.width_screen;
    int j = event->y * G.height_print / G.height_screen;
    if (0 <= i && i < G.width_print && 0 <= j && j < G.height_print) {
      int32_t dwell = perturbator_get_dwell_n(G.image)[j * G.width_print + i];
      if (dwell < 0) {
        int period = -dwell;
        mpc_t c;
        mpc_init2(c, 53);
        screen_to_param(event->x, event->y, mpc_realref(c), mpc_imagref(c));
        start_bond(c, period);
        mpc_clear(c);
      }
    }
  }
  return TRUE;
}

static void bond_toggled_cb(GtkWidget *bond, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(bond));
  if (active) {
    G.bond_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(bond_button_press_event_cb), NULL);
  } else {
    if (G.bond_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.bond_button_press_handler);
    }
    G.bond_button_press_handler = 0;
  }
}

static void update_fill_type(int t)
{
  G.style.fill_type = t;
  gtk_combo_box_set_active(GTK_COMBO_BOX(G.fillcombo), t + 1);
}

static void update_line_type(int t)
{
  G.style.line_type = t;
  gtk_combo_box_set_active(GTK_COMBO_BOX(G.dashcombo), t);
}

static void update_colour(double r, double g, double b)
{
  G.style.colour_r = r;
  G.style.colour_g = g;
  G.style.colour_b = b;
  GdkRGBA c = { r, g, b, 1 };
  gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(G.colourbutton), &c);
}

static void annotree_selection_changed_cb(GtkTreeSelection *selection, gpointer data) {
  (void) data;
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer thing;
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    struct annotation *ls;
    for (ls = G.anno; ls; ls = ls->next) {
      ls->selected = 0;
    }
    ls = (struct annotation *) thing;
    if (ls) {
      ls->selected = 1;
      update_line_type(ls->style.line_type);
      update_fill_type(ls->style.fill_type);
      update_colour(ls->style.colour_r, ls->style.colour_g, ls->style.colour_b);
    }
  }
  gtk_widget_queue_draw(G.da);
}

static void delete_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer thing;
  if (gtk_tree_selection_get_selected(select, &model, &iter)) {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing) {
      gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
      delete_annotation((struct annotation *) thing);
    }
  }
}

int serialize_toml(const char *filename)
{
  FILE *out = fopen(filename, "w");
  if (out)
  {
    fprintf(out, "program = \"m-perturbator-gtk\"\nversion =\"%s\"\n", VERSION);
    fprintf(out, "width = %d\nheight = %d\n", G.width_print, G.height_print);
    switch (G.dark)
    {
      case false:
        fprintf(out, "theme = \"light\"\n");
        break;
      case true:
        fprintf(out, "theme = \"dark\"\n");
        break;
    }
    switch (G.colour)
    {
      case colour_theme_monochrome:
        fprintf(out, "colour = \"monochrome\"\n");
        break;
      case colour_theme_low_colour:
        fprintf(out, "colour = \"low\"\n");
        break;
      case colour_theme_full_colour:
        fprintf(out, "colour = \"full\"\n");
        break;
    }
    switch (G.key)
    {
      case key_theme_none:
        fprintf(out, "key = \"none\"\n");
        break;
      case key_theme_overlay:
        fprintf(out, "key = \"overlay\"\n");
        break;
      case key_theme_detach:
        fprintf(out, "key = \"detach\"\n");
        break;
    }
    mpfr_fprintf
      ( out
      , "view.default = { precision = %Pd, re = \"%Re\", im = \"%Re\", radius = \"%Re\", iterations = %d }\n"
      , mpfr_get_prec(G.view->cx)
      , G.view->cx
      , G.view->cy
      , G.view->radius
      , G.maximum_iterations
      );
    fprintf(out, "annotations = [\n");
    for (struct annotation *anno = G.anno; anno; anno = anno->next)
    {
      char *label = escape_for_toml_string(anno->label);
      fprintf
        ( out
        , "  { style = { rgb = [ %f, %f, %f ], fill_type = %d, line_type = %d }, label = \"%s\", "
        , anno->style.colour_r
        , anno->style.colour_g
        , anno->style.colour_b
        , anno->style.fill_type
        , anno->style.line_type
        , label ? label : anno->label
        );
      if (label)
      {
        free(label);
      }
      switch (anno->tag)
      {
        case annotation_ray_out: 
        {
          struct point *p = anno->u.ray_out.line_end;
          mpfr_fprintf
            ( out
            , "type = \"ray_out\", precision = %Pd, re = \"%Re\", im = \"%Re\""
            , mpfr_get_prec(mpc_realref(p->xy))
            , mpc_realref(p->xy)
            , mpc_imagref(p->xy)
            );
          break;
        }
        case annotation_ray_in:
        {
          char *s = m_binangle_to_new_string(&anno->u.ray_in.angle);
          fprintf
            ( out
            , "type = \"ray_in\", depth = %d, angle = \"%s\""
            , anno->u.ray_in.depth
            , s
            );
          free(s);
          break;
        }
        case annotation_text: {
          mpfr_fprintf
            ( out
            , "type = \"text\", precision = %Pd, re = \"%Re\", im =\"%Re\""
            , mpfr_get_prec(mpc_realref(anno->u.text.xy))
            , mpc_realref(anno->u.text.xy)
            , mpc_imagref(anno->u.text.xy)
            );
          break;
        }
        case annotation_nucleus:
        {
          mpfr_fprintf
            ( out
            , "type = \"nucleus\", precision = %Pd, re = \"%Re\", im = \"%Re\", period = %d, domain_size = \"%Re\", size = \"%Re\""
            , mpfr_get_prec(mpc_realref(anno->u.nucleus.xy))
            , mpc_realref(anno->u.nucleus.xy)
            , mpc_imagref(anno->u.nucleus.xy)
            , anno->u.nucleus.period
            , anno->u.nucleus.domain_size
            , anno->u.nucleus.size
            );
          break;
        }
        case annotation_misiurewicz:
        {
          mpfr_fprintf
            ( out
            , "type = \"misiurewicz\", precision = %Pd, re = \"%Re\", im = \"%Re\", preperiod = %d, period = %d, domain_size = \"%Re\""
            , mpfr_get_prec(mpc_realref(anno->u.misiurewicz.xy))
            , mpc_realref(anno->u.misiurewicz.xy)
            , mpc_imagref(anno->u.misiurewicz.xy)
            , anno->u.misiurewicz.preperiod
            , anno->u.misiurewicz.period
            , anno->u.misiurewicz.domain_size
            );
          break;
        }
        case annotation_wake:
        {
          fprintf
            ( out
            , "type = \"wake\", depth = %d, lo = \"%s\", hi = \"%s\""
            , anno->u.wake.ray_depth
            , anno->u.wake.ray_lo
            , anno->u.wake.ray_hi
            );
          break;
        }
        case annotation_atom:
        {
          mpfr_fprintf
            ( out
            , "type = \"atom\", precision = %Pd, re = \"%Re\", im = \"%Re\", period = %d"
            , mpfr_get_prec(mpc_realref(anno->u.atom.nucleus))
            , mpc_realref(anno->u.atom.nucleus)
            , mpc_imagref(anno->u.atom.nucleus)
            , anno->u.atom.period
            );
          break;
        }
        case annotation_domain:
        {
          mpfr_fprintf
            ( out
            , "type = \"domain\", precision = %Pd, re = \"%Re\", im = \"%Re\", period = %d, loperiod = %d"
            , mpfr_get_prec(mpc_realref(anno->u.domain.nucleus))
            , mpc_realref(anno->u.domain.nucleus)
            , mpc_imagref(anno->u.domain.nucleus)
            , anno->u.domain.period
            , anno->u.domain.loperiod
            );
          break;
        }
        case annotation_misiurewicz_domain:
        {
          mpfr_fprintf
            ( out
            , "type = \"misiurewicz_domain\", precision = %Pd, re = \"%Re\", im = \"%Re\", period = %d, lopreperiod = %d, hipreperiod = %d"
            , mpfr_get_prec(mpc_realref(anno->u.misiurewicz_domain.misiurewicz))
            , mpc_realref(anno->u.misiurewicz_domain.misiurewicz)
            , mpc_imagref(anno->u.misiurewicz_domain.misiurewicz)
            , anno->u.misiurewicz_domain.period
            , anno->u.misiurewicz_domain.lopreperiod
            , anno->u.misiurewicz_domain.hipreperiod
            );
          break;
        }
        case annotation_domain_estimate:
        {
          mpfr_fprintf
            ( out
            , "type = \"domain_estimate\", precision = %Pd, re = \"%Re\", im = \"%Re\", radius = \"%Re\", preperiod = %d, period = %d"
            , mpfr_get_prec(mpc_realref(anno->u.domain_estimate.c))
            , mpc_realref(anno->u.domain_estimate.c)
            , mpc_imagref(anno->u.domain_estimate.c)
            , anno->u.domain_estimate.r
            , anno->u.domain_estimate.preperiod
            , anno->u.domain_estimate.period
            );
          break;
        }
      }
      fprintf(out, " },\n");
    }
    fprintf(out, "]\n");
    fclose(out);
    return 0;
  }
  else
  {
    return 1;
  }
}

int deserialize_toml(const char *filename)
{
  int retval = 0;
  FILE *in = fopen(filename, "r");
  if (in)
  {
    char errbuf[256];
    toml_table_t *f = toml_parse_file(in, errbuf, sizeof(errbuf));
    fclose(in);
    if (f)
    {
      toml_datum_t program = toml_string_in(f, "program");
      if (program.ok)
      {
        if (0 == strcmp("m-perturbator-gtk", program.u.s))
        {
          // delete annotations
          gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(G.annostore), 0, GTK_SORT_ASCENDING); // makes "(none)" last, prevents infinite loop
          while (G.anno)
          {
            GtkTreeIter iter;
            gpointer thing;
            gtk_tree_model_get_iter_first(GTK_TREE_MODEL(G.annostore), &iter);
            gtk_tree_model_get(GTK_TREE_MODEL(G.annostore), &iter, 1, &thing, -1);
            if (thing)
            {
              delete_annotation((struct annotation *) thing);
              gtk_tree_store_remove(G.annostore, &iter);
            }
          }
          // reset style
          set_dark_theme(false, false);
          set_colour_theme(colour_theme_low_colour);
          set_key_theme(key_theme_none);
          G.style.line_type = 0;
          G.style.fill_type = -1;
          G.style.colour_r = 0;
          G.style.colour_g = 0;
          G.style.colour_b = 0;
          // check version
          {
            toml_datum_t version = toml_string_in(f, "version");
            if (version.ok)
            {
              if (0 < strcmp(version.u.s, VERSION))
              {
                printf("version mismatch: file %s, program %s\n", version.u.s, VERSION);
              }
              else
              {
                // ok
              }
              free(version.u.s);
            }
            else
            {
              printf("unknown file version\n");
            }
          }
          // read size
          toml_datum_t width = toml_int_in(f, "width");
          if (width.ok)
          {
            // ... width.u.i // TODO
          }
          toml_datum_t height = toml_int_in(f, "height");
          if (height.ok)
          {
            // ... height.u.i // TODO
          }
          // read dark theme
          {
            toml_datum_t theme = toml_string_in(f, "theme");
            if (theme.ok)
            {
              if (0 == strcmp("light", theme.u.s))
              {
                set_dark_theme(false, false);
              }
              else if (0 == strcmp("dark", theme.u.s))
              {
                set_dark_theme(true, false);
              }
              else
              {
                printf("invalid: theme (%s)\n", theme.u.s);
              }
              free(theme.u.s);
            }
          }
          // read colour theme
          {
            toml_datum_t colour = toml_string_in(f, "colour");
            if (colour.ok)
            {
              if (0 == strcmp("monochrome", colour.u.s))
              {
                set_colour_theme(colour_theme_monochrome);
              }
              else if (0 == strcmp("low", colour.u.s))
              {
                set_colour_theme(colour_theme_low_colour);
              }
              else if (0 == strcmp("full", colour.u.s))
              {
                set_colour_theme(colour_theme_full_colour);
              }
              else
              {
                printf("invalid: colour (%s)\n", colour.u.s);
              }
              free(colour.u.s);
            }
          }
          // read key theme
          {
            toml_datum_t key = toml_string_in(f, "key");
            if (key.ok)
            {
              if (0 == strcmp("none", key.u.s))
              {
                set_key_theme(key_theme_none);
              }
              else if (0 == strcmp("overlay", key.u.s))
              {
                set_key_theme(key_theme_overlay);
              }
              else if (0 == strcmp("detach", key.u.s))
              {
                set_key_theme(key_theme_detach);
              }
              else
              {
                printf("invalid: key (%s)\n", key.u.s);
              }
              free(key.u.s);
            }
          }
          // read view
          {
            int prec = 0;
            char *sre = 0;
            char *sim = 0;
            char *sradius = 0;
            int iters = 1 << 16; // FIXME default iteration count
            toml_table_t *v = toml_table_in(f, "view");
            if (v)
            {
              toml_table_t *d = toml_table_in(v, "default");
              if (d)
              {
                toml_datum_t precision = toml_int_in(d, "precision");
                if (precision.ok)
                {
                  prec = precision.u.i;
                }
                else
                {
                  retval = 1;
                  printf("expected: precision\n");
                }
                toml_datum_t re = toml_string_in(d, "re");
                if (re.ok)
                {
                  sre = re.u.s;
                }
                else
                {
                  retval = 1;
                  printf("expected: re\n");
                }
                toml_datum_t im = toml_string_in(d, "im");
                if (im.ok)
                {
                  sim = im.u.s;
                }
                else
                {
                  retval = 1;
                  printf("expected: im\n");
                }
                toml_datum_t radius = toml_string_in(d, "radius");
                if (radius.ok)
                {
                  sradius = radius.u.s;
                }
                else
                {
                  retval = 1;
                  printf("expected: radius\n");
                }
                toml_datum_t iterations = toml_int_in(d, "iterations");
                if (iterations.ok)
                {
                  iters = iterations.u.i;
                }
                else
                {
                  // use default
                }
              }
              else
              {
                retval = 1;
                printf("expected: default\n");
              }
            }
            else
            {
              retval = 1;
              printf("expected: view\n");
            }
            if (prec > 0 && sre && sim && sradius)
            {
              struct view *v = view_new();
              mpfr_set_prec(v->cx, prec);
              mpfr_set_prec(v->cy, prec);
              int set_result = mpfr_set_str(v->cx, sre, 0, MPFR_RNDN);
              set_result += mpfr_set_str(v->cy, sim, 0, MPFR_RNDN);
              set_result += mpfr_set_str(v->radius, sradius, 0, MPFR_RNDN);
              if (set_result < 0)
              {
                retval = 1;
                printf("invalid: view\n");
                view_delete(v);
              }
              else
              {
                G.maximum_iterations = iters;
                start_render(v);
              }
            }
            else
            {
              retval = 1;
              printf("invalid: view\n");
            }
            if (sre) free(sre);
            if (sim) free(sim);
            if (sradius) free(sradius);
          }
          // read annotations
          toml_array_t *as = toml_array_in(f, "annotations");
          if (as)
          {
          int na = toml_array_nelem(as);
          for (int i = 0; i < na; ++i)
          {
            toml_table_t *a = toml_table_at(as, i);
            if (a)
            {
              toml_table_t *s = toml_table_in(a, "style");
              if (s)
              {
                toml_array_t *rgb = toml_array_in(s, "rgb");
                if (rgb)
                {
                  if (toml_array_nelem(rgb) == 3)
                  {
                    toml_datum_t c;
                    c = toml_double_at(rgb, 0);
                    if (c.ok && 0 <= c.u.d && c.u.d <= 1)
                    {
                      G.style.colour_r = c.u.d;
                    }
                    else
                    {
                      retval = 1;
                      printf("expected: red value\n");
                    }
                    c = toml_double_at(rgb, 1);
                    if (c.ok && 0 <= c.u.d && c.u.d <= 1)
                    {
                      G.style.colour_g = c.u.d;
                    }
                    else
                    {
                      retval = 1;
                      printf("expected: green value\n");
                    }
                    c = toml_double_at(rgb, 2);
                    if (c.ok && 0 <= c.u.d && c.u.d <= 1)
                    {
                      G.style.colour_b = c.u.d;
                    }
                    else
                    {
                      retval = 1;
                      printf("expected: blue value\n");
                    }
                    update_colour(G.style.colour_r, G.style.colour_g, G.style.colour_b);
                  }
                  else
                  {
                    printf("expected: 3 values\n");
                  }
                }
                else
                {
                  // ok
                }
                toml_datum_t fill_type = toml_int_in(s, "fill_type");
                if (fill_type.ok)
                {
                  G.style.fill_type = fill_type.u.i;
                  update_fill_type(G.style.fill_type);
                }
                else
                {
                  // ok
                }
                toml_datum_t line_type = toml_int_in(s, "line_type");
                if (line_type.ok)
                {
                  G.style.line_type = line_type.u.i;
                  update_line_type(G.style.line_type);
                }
                else
                {
                  // ok
                }
              }
              else
              {
                // ok
              }
              char *label = 0;
              toml_datum_t l = toml_string_in(a, "label");
              if (l.ok)
              {
                label = l.u.s;
              }
              else
              {
                retval = 1;
                printf("expected: label\n");
              }
              toml_datum_t t = toml_string_in(a, "type");
              if (t.ok)
              {
                bool ok = true;
                enum annotation_t tag;
                if (0 == strcmp("nucleus", t.u.s))
                {
                  tag = annotation_nucleus;
                }
                else if (0 == strcmp("misiurewicz", t.u.s))
                {
                  tag = annotation_misiurewicz;
                }
#if 0
                else if (0 == strcmp("bond", t.u.s))
                {
                  tag = annotation_bond;
                }
#endif
                else if (0 == strcmp("atom", t.u.s))
                {
                  tag = annotation_atom;
                }
                else if (0 == strcmp("domain", t.u.s))
                {
                  tag = annotation_domain;
                }
                else if (0 == strcmp("misiurewicz_domain", t.u.s))
                {
                  tag = annotation_misiurewicz_domain;
                }
                else if (0 == strcmp("domain_estimate", t.u.s))
                {
                  tag = annotation_domain_estimate;
                }
                else if (0 == strcmp("ray_in", t.u.s))
                {
                  tag = annotation_ray_in;
                }
                else if (0 == strcmp("ray_out", t.u.s))
                {
                  tag = annotation_ray_out;
                }
                else if (0 == strcmp("wake", t.u.s))
                {
                  tag = annotation_wake;
                }
                else if (0 == strcmp("text", t.u.s))
                {
                  tag = annotation_text;
                }
                else
                {
                  ok = false;
                }
                if (ok)
                {
                  switch (tag)
                  {
                    case annotation_nucleus:
                    case annotation_misiurewicz:
                    // case annotation_bond:
                    case annotation_atom:
                    case annotation_domain:
                    case annotation_misiurewicz_domain:
                    case annotation_domain_estimate:
                    case annotation_ray_out:
                    case annotation_text:
                    {
                      int prec = -1;
                      int period = -1;
                      int preperiod = -1;
                      int loperiod = -1;
                      int lopreperiod = -1;
                      int hipreperiod = -1;
                      char *sre = 0;
                      char *sim = 0;
                      char *sradius = 0;
                      char *ssize = 0;
                      char *sdomain_size = 0;
                      {
                        toml_datum_t precision = toml_int_in(a, "precision");
                        if (precision.ok && precision.u.i > 0)
                        {
                          prec = precision.u.i;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: precision\n");
                        }
                      }
                      {
                        toml_datum_t re = toml_string_in(a, "re");
                        if (re.ok)
                        {
                          sre = re.u.s;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: re\n");
                        }
                      }
                      {
                        toml_datum_t im = toml_string_in(a, "im");
                        if (im.ok)
                        {
                          sim = im.u.s;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: im\n");
                        }
                      }
                      if (tag == annotation_nucleus)
                      {
                        toml_datum_t size = toml_string_in(a, "size");
                        if (size.ok)
                        {
                          ssize = size.u.s;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: size\n");
                        }
                      }
                      if (tag == annotation_nucleus || tag == annotation_misiurewicz)
                      {
                        toml_datum_t size = toml_string_in(a, "domain_size");
                        if (size.ok)
                        {
                          sdomain_size = size.u.s;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: domain_size\n");
                        }
                      }
                      if (tag == annotation_domain_estimate)
                      {
                        toml_datum_t size = toml_string_in(a, "radius");
                        if (size.ok)
                        {
                          sradius = size.u.s;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: radius\n");
                        }
                      }
                      if (tag != annotation_ray_out && tag != annotation_text)
                      {
                        toml_datum_t p = toml_int_in(a, "period");
                        if (p.ok && 0 < p.u.i)
                        {
                          period = p.u.i;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: period\n");
                        }
                      }
                      if (tag == annotation_misiurewicz || tag == annotation_domain_estimate)
                      {
                        toml_datum_t p = toml_int_in(a, "preperiod");
                        if (p.ok && 0 <= p.u.i)
                        {
                          preperiod = p.u.i;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: preperiod\n");
                        }
                      }
                      if (tag == annotation_domain)
                      {
                        toml_datum_t p = toml_int_in(a, "loperiod");
                        if (p.ok && 0 < p.u.i)
                        {
                          loperiod = p.u.i;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: loperiod\n");
                        }
                      }
                      if (tag == annotation_misiurewicz_domain)
                      {
                        toml_datum_t p = toml_int_in(a, "lopreperiod");
                        if (p.ok && 0 <= p.u.i)
                        {
                          lopreperiod = p.u.i;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: lopreperiod\n");
                        }
                      }
                      if (tag == annotation_misiurewicz_domain)
                      {
                        toml_datum_t p = toml_int_in(a, "hipreperiod");
                        if (p.ok && 0 <= p.u.i)
                        {
                          hipreperiod = p.u.i;
                        }
                        else
                        {
                          retval = 1;
                          printf("expected: hipreperiod\n");
                        }
                      }
                      if (retval == 0 && prec > 0)
                      {
                        mpfr_t re, im, size, domain_size, radius;
                        mpfr_init2(re, prec);
                        mpfr_init2(im, prec);
                        mpfr_init2(size, 53);
                        mpfr_init2(domain_size, 53);
                        mpfr_init2(radius, 53);
                        if (sre)
                        {
                          int set_result = mpfr_set_str(re, sre, 0, MPFR_RNDN);
                          if (set_result < 0)
                          {
                            retval = 1;
                            printf("invalid: re (%s)\n", sre);
                          }
                        }
                        if (sim)
                        {
                          int set_result = mpfr_set_str(im, sim, 0, MPFR_RNDN);
                          if (set_result < 0)
                          {
                            retval = 1;
                            printf("invalid: im (%s)\n", sim);
                          }
                        }
                        if (ssize)
                        {
                          int set_result = mpfr_set_str(size, ssize, 0, MPFR_RNDN);
                          if (set_result < 0)
                          {
                            retval = 1;
                            printf("invalid: size (%s)\n", ssize);
                          }
                        }
                        if (sdomain_size)
                        {
                          int set_result = mpfr_set_str(domain_size, sdomain_size, 0, MPFR_RNDN);
                          if (set_result < 0)
                          {
                            retval = 1;
                            printf("invalid: domain_size (%s)\n", sdomain_size);
                          }
                        }
                        if (sradius)
                        {
                          int set_result = mpfr_set_str(radius, sradius, 0, MPFR_RNDN);
                          if (set_result < 0)
                          {
                            retval = 1;
                            printf("invalid: radius (%s)\n", sradius);
                          }
                        }
                        mpc_t c;
                        mpc_init2(c, prec);
                        mpfr_set(mpc_realref(c), re, MPFR_RNDN);
                        mpfr_set(mpc_imagref(c), im, MPFR_RNDN);
                        switch (tag)
                        {
                          case annotation_nucleus:
                          {
                            struct annotation *anno = calloc(1, sizeof(struct annotation));
                            anno->tag = annotation_nucleus;
                            anno->label = strdup(label);
                            anno->style = G.style;
                            anno->u.nucleus.period = period;
                            mpfr_init2(mpc_realref(anno->u.nucleus.xy), prec);
                            mpfr_init2(mpc_imagref(anno->u.nucleus.xy), prec);
                            mpfr_init2(anno->u.nucleus.domain_size, 53);
                            mpfr_init2(anno->u.nucleus.size, 53);
                            mpfr_set(mpc_realref(anno->u.nucleus.xy), re, MPFR_RNDN);
                            mpfr_set(mpc_imagref(anno->u.nucleus.xy), im, MPFR_RNDN);
                            mpfr_set(anno->u.nucleus.domain_size, domain_size, MPFR_RNDN);
                            mpfr_set(anno->u.nucleus.size, size, MPFR_RNDN);
                            add_annotation(anno);
                            break;
                          }
                          case annotation_misiurewicz:
                          {
                            struct annotation *anno = calloc(1, sizeof(struct annotation));
                            anno->tag = annotation_misiurewicz;
                            anno->label = strdup(label);
                            anno->style = G.style;
                            anno->u.misiurewicz.preperiod = preperiod;
                            anno->u.misiurewicz.period = period;
                            mpfr_init2(mpc_realref(anno->u.misiurewicz.xy), prec);
                            mpfr_init2(mpc_imagref(anno->u.misiurewicz.xy), prec);
                            mpfr_init2(anno->u.misiurewicz.domain_size, 53);
                            mpfr_set(mpc_realref(anno->u.misiurewicz.xy), re, MPFR_RNDN);
                            mpfr_set(mpc_imagref(anno->u.misiurewicz.xy), im, MPFR_RNDN);
                            mpfr_set(anno->u.misiurewicz.domain_size, domain_size, MPFR_RNDN);
                            add_annotation(anno);
                            break;
                          }
                          case annotation_text:
                          {
                            struct annotation *anno = calloc(1, sizeof(struct annotation));
                            anno->tag = annotation_text;
                            anno->label = strdup(label);
                            anno->style = G.style;
                            mpfr_init2(mpc_realref(anno->u.text.xy), prec);
                            mpfr_init2(mpc_imagref(anno->u.text.xy), prec);
                            mpfr_set(mpc_realref(anno->u.text.xy), re, MPFR_RNDN);
                            mpfr_set(mpc_imagref(anno->u.text.xy), im, MPFR_RNDN);
                            add_annotation(anno);
                            break;
                          }
                          case annotation_domain_estimate:
                          {
                            start_domain_estimate(c, radius, preperiod, period, label);
                            break;
                          }
                          case annotation_atom:
                          {
                            start_atom(c, period);
                            break;
                          }
                          case annotation_domain:
                          {
                            start_domain(c, period, loperiod);
                            break;
                          }
                          case annotation_misiurewicz_domain:
                          {
                            start_misiurewicz_domain(c, period, lopreperiod, hipreperiod);
                            break;
                          }
                          case annotation_ray_out:
                          {
                            double x = 0, y = 0;
                            param_to_screen(&x, &y, re, im);
                            start_ray_out(x, y);
                            break;
                          }
                        }
                        mpc_clear(c);
                        mpfr_clear(re);
                        mpfr_clear(im);
                        mpfr_clear(size);
                        mpfr_clear(domain_size);
                        mpfr_clear(radius);
                      }
                      if (sre) free(sre);
                      if (sim) free(sim);
                      if (ssize) free(ssize);
                      if (sdomain_size) free(sdomain_size);
                      if (sradius) free(sradius);
                      break;
                    }
                    case annotation_ray_in:
                    {
                      int depth = 0;
                      toml_datum_t d = toml_int_in(a, "depth");
                      if (d.ok && d.u.i > 0)
                      {
                        depth = d.u.i;
                      }
                      else
                      {
                        retval = 1;
                        printf("expected: depth\n");
                      }
                      toml_datum_t b = toml_string_in(a, "angle");
                      char *s = 0;
                      m_binangle angle;
                      m_binangle_init(&angle);
                      if (b.ok)
                      {
                        s = b.u.s;
                      }
                      else
                      {
                        retval = 1;
                        printf("expected: angle\n");
                      }
                      if (s)
                      {
                        const char *s2 = m_binangle_from_string(&angle, s);
                        if ((! s2) || (*s2))
                        {
                          retval = 1;
                          printf("invalid: angle (%s)\n", s);
                        }
                        free(s);
                      }
                      if (retval == 0)
                      {
                        start_ray_in(&angle, depth);
                      }
                      m_binangle_clear(&angle);
                      break;
                    }
                    case annotation_wake:
                    {
                      int depth = 0;
                      toml_datum_t d = toml_int_in(a, "depth");
                      if (d.ok && d.u.i > 0)
                      {
                        depth = d.u.i;
                      }
                      else
                      {
                        retval = 1;
                        printf("expected: depth\n");
                      }
                      char *lo = 0;
                      toml_datum_t l = toml_string_in(a, "lo");
                      if (l.ok)
                      {
                        lo = l.u.s;
                      }
                      else
                      {
                        retval = 1;
                        printf("expected: lo\n");
                      }
                      char *hi = 0;
                      toml_datum_t h = toml_string_in(a, "hi");
                      if (h.ok)
                      {
                        hi = h.u.s;
                      }
                      else
                      {
                        retval = 1;
                        printf("expected: hi\n");
                      }
                      if (depth > 0 && lo && hi)
                      {
                        start_wake2(depth, lo, hi);
                      }
                      if (lo) free(lo);
                      if (hi) free(hi);
                      break;
                    }
                  }
                }
                else
                {
                  retval = 1;
                  printf("invalid: type (%s)\n", t.u.s);
                }
                free(t.u.s);
              }
              else
              {
                retval = 1;
                printf("expected: type\n");
              }
              if (label)
              {
                free(label);
              }
            }
            else
            {
              retval = 1;
              printf("expected: annotation\n");
            }
          }
          }
          else
          {
            // ok
          }
        }
        else
        {
          printf("expected: program = \"m-perturbator-gtk\"\n");
          retval = 1;
        }
        free(program.u.s);
      }
      else
      {
        printf("expected: program\n");
        retval = 1;
      }
      toml_free(f);
    }
    else
    {
      printf("could not parse TOML: %s\n", errbuf);
      retval = 1;
    }
  }
  else
  {
    printf("could not open file\n");
    retval = 1;
  }
  return retval;
}

int deserialize_old(const char *filename)
{
  FILE *in = fopen(filename, "r");
  if (in) {
    while (G.anno) {
      GtkTreeIter iter;
      gpointer thing;
      gtk_tree_model_get_iter_first(GTK_TREE_MODEL(G.annostore), &iter);
      if (gtk_tree_model_iter_next(GTK_TREE_MODEL(G.annostore), &iter)) {
        gtk_tree_model_get(GTK_TREE_MODEL(G.annostore), &iter, 1, &thing, -1);
        if (thing) {
          delete_annotation((struct annotation *) thing);
          gtk_tree_store_remove(G.annostore, &iter);
        }
      }
    }
    // reset style
    set_dark_theme(false, false);
    set_colour_theme(colour_theme_low_colour);
    set_key_theme(key_theme_none);
    G.style.line_type = 0;
    G.style.fill_type = -1;
    G.style.colour_r = 0;
    G.style.colour_g = 0;
    G.style.colour_b = 0;
    char *line = 0;
    size_t linelen = 0;
    ssize_t readlen = 0;
    while (-1 != (readlen = getline(&line, &linelen, in))) {
      if (readlen && line[readlen-1] == '\n') {
        line[readlen - 1] = 0;
      }
      if (0 == strncmp(line, "m-perturbator-gtk ", 18)) {
        if (0 < strcmp(line + 18, VERSION)) {
          printf("version mismatch: file %s, program %s\n", line + 18, VERSION);
        }
      } else if (0 == strncmp(line, "size ", 5)) {
        int w = 0, h = 0;
        sscanf(line + 5, "%d %d\n", &w, &h);
        // resize_image(w, h); // FIXME TODO make this work...
      } else if (0 == strncmp(line, "view ", 5)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        char *rs = malloc(readlen);
        sscanf(line + 5, "%d %s %s %s", &p, xs, ys, rs);
        struct view *v = view_new();
        mpfr_set_prec(v->cx, p);
        mpfr_set_prec(v->cy, p);
        set_result = mpfr_set_str(v->cx, xs, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(v->cy, ys, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(v->radius, rs, 0, GMP_RNDN);
        free(xs);
        free(ys);
        free(rs);
        if (set_result < 0) { printf("view line not valid, check chars\n"); return 1; }
        start_render(v);
      } else if (0 == strncmp(line, "dark ", 5)) {
        sscanf(line + 5, "%d", &G.dark);
        set_dark_theme(G.dark, false);
      } else if (0 == strncmp(line, "colour ", 7)) {
        sscanf(line + 7, "%d", &G.colour);
        set_colour_theme(G.colour);
      } else if (0 == strncmp(line, "rgb ", 4)) {
        sscanf(line + 4, "%lf %lf %lf", &G.style.colour_r, &G.style.colour_g, &G.style.colour_b);
        update_colour(G.style.colour_r, G.style.colour_g, G.style.colour_b);
      } else if (0 == strncmp(line, "line_type ", 10)) {
        sscanf(line + 10, "%d", &G.style.line_type);
        update_line_type(G.style.line_type);
      } else if (0 == strncmp(line, "fill_type ", 10)) {
        sscanf(line + 10, "%d", &G.style.fill_type);
        update_fill_type(G.style.fill_type);
      } else if (0 == strncmp(line, "ray_out ", 8)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        sscanf(line + 8, "%d %s %s", &p, xs, ys);
        mpfr_t cx, cy;
        mpfr_init2(cx, p);
        mpfr_init2(cy, p);
        set_result = mpfr_set_str(cx, xs, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(cy, ys, 0, GMP_RNDN);
        free(xs);
        free(ys);
        double x = 0, y = 0;
        param_to_screen(&x, &y, cx, cy);
        mpfr_clear(cx);
        mpfr_clear(cy);
        if (set_result < 0) { printf("ray_out line not valid, check chars\n"); return 1; }
        start_ray_out(x, y);
      } else if (0 == strncmp(line, "ray_in ", 7)) {
        int depth = 1000;
        char *as = malloc(readlen);
        sscanf(line + 7, "%d %s", &depth, as);
        struct m_binangle *angle = malloc(sizeof(*angle));
        m_binangle_init(angle);
        m_binangle_from_string(angle, as);
        m_binangle_canonicalize(angle);
        start_ray_in(angle, depth);
        free(as);
      } else if (0 == strncmp(line, "text ", 5)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        char *ss = malloc(readlen);
        sscanf(line + 5, "%d %s %s %s", &p, xs, ys, ss);
        struct annotation *anno = calloc(1, sizeof(struct annotation));
        anno->tag = annotation_text;
        anno->label = ss;
        anno->style = G.style;
        mpfr_init2(mpc_realref(anno->u.text.xy), p);
        mpfr_init2(mpc_imagref(anno->u.text.xy), p);
        set_result = mpfr_set_str(mpc_realref(anno->u.text.xy), xs, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(mpc_imagref(anno->u.text.xy), ys, 0, GMP_RNDN);
        free(xs);
        free(ys);
        if (set_result < 0) {
          free(ss);
          mpfr_clear(mpc_realref(anno->u.text.xy));
          mpfr_clear(mpc_imagref(anno->u.text.xy));
          printf("text line not valid, check chars \n");
          return 1;
        }
        add_annotation(anno);
      } else if (0 == strncmp(line, "nucleus ", 8)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        int period = 0;
        char *dzs = malloc(readlen);
        char *szs = malloc(readlen);
        char *ss = malloc(readlen);
        sscanf(line + 8, "%d %s %s %d %s %s %s", &p, xs, ys, &period, dzs, szs, ss);
        struct annotation *anno = calloc(1, sizeof(struct annotation));
        anno->tag = annotation_nucleus;
        anno->label = ss;
        anno->style = G.style;
        anno->u.nucleus.period = period;
        mpfr_init2(mpc_realref(anno->u.nucleus.xy), p);
        mpfr_init2(mpc_imagref(anno->u.nucleus.xy), p);
        mpfr_init2(anno->u.nucleus.domain_size, 53);
        mpfr_init2(anno->u.nucleus.size, 53);
        set_result  = mpfr_set_str(mpc_realref(anno->u.nucleus.xy), xs, 0, GMP_RNDN);
        set_result += mpfr_set_str(mpc_imagref(anno->u.nucleus.xy), ys, 0, GMP_RNDN);
        set_result += mpfr_set_str(anno->u.nucleus.domain_size, dzs, 0, GMP_RNDN);
        set_result += mpfr_set_str(anno->u.nucleus.size, szs, 0, GMP_RNDN);
        free(xs);
        free(ys);
        free(dzs);
        free(szs);
        if (set_result < 0 || period <= 0) {
          free(ss);
          mpc_clear(anno->u.nucleus.xy);
          mpfr_clear(anno->u.nucleus.domain_size);
          mpfr_clear(anno->u.nucleus.size);
          free(anno);
          printf("nucleus line not valid, check chars \n");
          return 1;
        }
        add_annotation(anno);
      } else if (0 == strncmp(line, "misiurewicz ", 12)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        int preperiod = -1;
        int period = 0;
        char *dzs = malloc(readlen);
        char *ss = malloc(readlen);
        sscanf(line + 12, "%d %s %s %d %d %s %s", &p, xs, ys, &preperiod, &period, dzs, ss);
        struct annotation *anno = calloc(1, sizeof(struct annotation));
        anno->tag = annotation_misiurewicz;
        anno->label = ss;
        anno->style = G.style;
        anno->u.misiurewicz.preperiod = preperiod;
        anno->u.misiurewicz.period = period;
        mpfr_init2(mpc_realref(anno->u.misiurewicz.xy), p);
        mpfr_init2(mpc_imagref(anno->u.misiurewicz.xy), p);
        mpfr_init2(anno->u.misiurewicz.domain_size, 53);
        set_result  = mpfr_set_str(mpc_realref(anno->u.misiurewicz.xy), xs, 0, GMP_RNDN);
        set_result += mpfr_set_str(mpc_imagref(anno->u.misiurewicz.xy), ys, 0, GMP_RNDN);
        set_result += mpfr_set_str(anno->u.misiurewicz.domain_size, dzs, 0, GMP_RNDN);
        free(xs);
        free(ys);
        free(dzs);
        if (set_result < 0 || preperiod <= 0 || period <= 0) {
          free(ss);
          mpc_clear(anno->u.misiurewicz.xy);
          mpfr_clear(anno->u.misiurewicz.domain_size);
          free(anno);
          printf("misiurewicz line not valid, check chars \n");
          return 1;
        }
        add_annotation(anno);
      } else if (0 == strncmp(line, "wake ", 5)) {
        int depth = 0;
        char *lo = malloc(readlen);
        char *hi = malloc(readlen);
        sscanf(line + 5, "%d %s %s", &depth, lo, hi);
        start_wake2(depth, lo, hi);
        free(lo);
        free(hi);
      } else if (0 == strncmp(line, "atom ", 5)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        int period = 0;
        sscanf(line + 5, "%d %s %s %d", &p, xs, ys, &period);
        mpc_t nucleus;
        mpc_init2(nucleus, p);
        set_result  = mpfr_set_str(mpc_realref(nucleus), xs, 0, MPFR_RNDN);
        set_result += mpfr_set_str(mpc_imagref(nucleus), ys, 0, MPFR_RNDN);
        free(xs);
        free(ys);
        start_atom(nucleus, period);
        mpc_clear(nucleus);
      } else if (0 == strncmp(line, "domain ", 7)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        int period = 0;
        int loperiod = 0;
        if (5 != sscanf(line + 7, "%d %s %s %d %d", &p, xs, ys, &period, &loperiod))
        {
          loperiod = 0;
          sscanf(line + 7, "%d %s %s %d", &p, xs, ys, &period);
        }
        mpc_t nucleus;
        mpc_init2(nucleus, p);
        set_result  = mpfr_set_str(mpc_realref(nucleus), xs, 0, MPFR_RNDN);
        set_result += mpfr_set_str(mpc_imagref(nucleus), ys, 0, MPFR_RNDN);
        free(xs);
        free(ys);
        start_domain(nucleus, period, loperiod);
        mpc_clear(nucleus);
      } else if (0 == strncmp(line, "misiurewicz_domain ", 19)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        int period = 0;
        int lopreperiod = 0;
        int hipreperiod = 0;
        if (6 != sscanf(line + 19, "%d %s %s %d %d %d", &p, xs, ys, &period, &lopreperiod, &hipreperiod))
        {
          lopreperiod = 0;
          sscanf(line + 19, "%d %s %s %d %d", &p, xs, ys, &period, &hipreperiod);
        }
        mpc_t nucleus;
        mpc_init2(nucleus, p);
        set_result  = mpfr_set_str(mpc_realref(nucleus), xs, 0, MPFR_RNDN);
        set_result += mpfr_set_str(mpc_imagref(nucleus), ys, 0, MPFR_RNDN);
        free(xs);
        free(ys);
        start_misiurewicz_domain(nucleus, period, lopreperiod, hipreperiod);
        mpc_clear(nucleus);
      } else if (0 == strncmp(line, "domain_estimate ", 16)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        char *rs = malloc(readlen);
        char *dummy = malloc(readlen);
        int preperiod = -1;
        int period = -1;
        sscanf(line + 16, "%d %s %s %s %d %d %s", &p, xs, ys, rs, &preperiod, &period, dummy);
        mpc_t c;
        mpc_init2(c, p);
        set_result  = mpfr_set_str(mpc_realref(c), xs, 0, MPFR_RNDN);
        set_result += mpfr_set_str(mpc_imagref(c), ys, 0, MPFR_RNDN);
        mpfr_t r;
        mpfr_init2(r, 53);
        set_result += mpfr_set_str(r, rs, 0, MPFR_RNDN);
        free(xs);
        free(ys);
        free(rs);
        free(dummy);
        char *label = line + 16;
        for (int i = 0; i < 6; ++i)
          label = label ? (strchr(label, ' ') + 1) : label;
        if (! label)
        {
          label = "unknown";
        }
        start_domain_estimate(c, r, preperiod, period, label);
        mpc_clear(c);
        mpfr_clear(r);
      }
    }
    free(line);
    fclose(in);
    return 0;
  } else {
    return 1;
  }
}

static gint sortable_annotation_compare(GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer user_data)
{
  (void) user_data;
  gpointer thinga = 0, thingb = 0;
  gtk_tree_model_get(model, a, 1, &thinga, -1);
  gtk_tree_model_get(model, b, 1, &thingb, -1);
  struct annotation *la = (struct annotation *) thinga;
  struct annotation *lb = (struct annotation *) thingb;
  if (la && lb) {
    if (la->tag < lb->tag) return -1;
    if (la->tag > lb->tag) return 1;
    switch (la->tag)
    {
      case annotation_text:
        return strcmp(la->label, lb->label);
      case annotation_ray_out:
        return strcmp(la->label, lb->label);
      case annotation_ray_in:
      {
        mpq_t a, b;
        mpq_init(a);
        mpq_init(b);
        m_binangle_to_rational(a, &la->u.ray_in.angle);
        m_binangle_to_rational(b, &lb->u.ray_in.angle);
        int r = mpq_cmp(a, b);
        mpq_clear(a);
        mpq_clear(b);
        return (r > 0) - (0 > r);
      }
      case annotation_nucleus:
      {
        int dp = la->u.nucleus.period - lb->u.nucleus.period;
        return (dp > 0) - (0 > dp);
      }
      case annotation_misiurewicz:
      {
        int pp = la->u.misiurewicz.preperiod - lb->u.misiurewicz.preperiod;
        int p = la->u.misiurewicz.period - lb->u.misiurewicz.period;
        pp = (pp > 0) - (0 > pp);
        p = (p > 0) - (0 > p);
        return pp ? pp : p;
      }
      case annotation_wake:
        return strcmp(la->label, lb->label);
      case annotation_atom:
        return strcmp(la->label, lb->label);
      case annotation_domain:
      {
        int pp = la->u.domain.loperiod - lb->u.domain.loperiod;
        int p = la->u.domain.period - lb->u.domain.period;
        pp = (pp > 0) - (0 > pp);
        p = (p > 0) - (0 > p);
        return pp ? pp : p;
      }
      case annotation_misiurewicz_domain:
      {
        int r = la->u.misiurewicz_domain.hipreperiod - lb->u.misiurewicz_domain.hipreperiod;
        int q = la->u.misiurewicz_domain.lopreperiod - lb->u.misiurewicz_domain.lopreperiod;
        int p = la->u.misiurewicz_domain.period - lb->u.misiurewicz_domain.period;
        r = (r > 0) - (r > q);
        q = (q > 0) - (0 > q);
        p = (p > 0) - (0 > p);
        return  r ? r : q ? q : p;
      }
      case annotation_domain_estimate:
        return strcmp(la->label, lb->label);
    }
  }
  if (la) return -1;
  if (lb) return 1;
  return 0;
}

static void task_func(gpointer data, gpointer userdata);
static gpointer annotation_thread(gpointer userdata);

static void dashing_selection_changed_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  G.style.line_type = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
  struct annotation *anno = get_selected_annotation();
  if (anno)
  {
    if (annotation_set_line_type(anno, G.style.line_type))
    {
      gtk_widget_queue_draw(G.da);
    }
  }
}

static void fill_selection_changed_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  G.style.fill_type = gtk_combo_box_get_active(GTK_COMBO_BOX(widget)) - 1;
  struct annotation *anno = get_selected_annotation();
  if (anno)
  {
    if (annotation_set_fill_type(anno, G.style.fill_type))
    {
      gtk_widget_queue_draw(G.da);
    }
  }
}

void set_dark_theme(bool use_dark_theme, bool fromGUI)
{
  bool toggle = G.dark != use_dark_theme;
  G.dark = use_dark_theme;
  if (! fromGUI)
  {
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_theme_dark), use_dark_theme);
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_theme_light), ! use_dark_theme);
  }
  int active = gtk_combo_box_get_active(GTK_COMBO_BOX(G.dashcombo));
  gtk_combo_box_set_model(GTK_COMBO_BOX(G.dashcombo), GTK_TREE_MODEL(G.dashstore[G.dark]));
  gtk_combo_box_set_active(GTK_COMBO_BOX(G.dashcombo), active);
  active = gtk_combo_box_get_active(GTK_COMBO_BOX(G.fillcombo));
  gtk_combo_box_set_model(GTK_COMBO_BOX(G.fillcombo), GTK_TREE_MODEL(G.fillstore[G.dark]));
  gtk_combo_box_set_active(GTK_COMBO_BOX(G.fillcombo), active);
  if (toggle)
  {
    for (struct annotation *anno = G.anno; anno; anno = anno->next)
    {
      rgb_invert(&anno->style.colour_r, &anno->style.colour_g, &anno->style.colour_b);
    }
  }
#ifdef GDK_WINDOWING_X11
  GdkWindow *window = gtk_widget_get_parent_window(G.dashcombo);
  if (! window)
  {
    fprintf(stderr, "bad window for widget %p\n", (void *) G.dashcombo);
    return;
  }
  GdkDisplay *display = gdk_display_get_default();
  if (GDK_IS_X11_DISPLAY(display))
  {
    Display* xdisplay = GDK_DISPLAY_XDISPLAY(display);
    XChangeProperty
      ( xdisplay
      , GDK_WINDOW_XID(window)
      , XInternAtom(xdisplay, "_GTK_THEME_VARIANT", False)
      , XInternAtom(xdisplay, "UTF8_STRING", False)
      , 8
      , PropModeReplace
      , (unsigned char *) (use_dark_theme ? "dark" : "light")
      , use_dark_theme ? 4 : 5
      );
  }
#endif
  docolour(G.image);
}

void set_colour_theme(enum colour_theme_t use_colour_theme)
{
  G.colour = use_colour_theme;
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_colour_monochrome), use_colour_theme == colour_theme_monochrome);
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_colour_low), use_colour_theme == colour_theme_low_colour);
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_colour_full), use_colour_theme == colour_theme_full_colour);
}

void set_key_theme(enum key_theme_t use_key_theme)
{
  G.key = use_key_theme;
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_key_none), use_key_theme == key_theme_none);
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_key_overlay), use_key_theme == key_theme_overlay);
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(G.menu_key_detach), use_key_theme == key_theme_detach);
}

static void dark_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    set_dark_theme(true, true);
    gtk_widget_queue_draw(G.da);
  }
}

static void light_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    set_dark_theme(false, true);
    gtk_widget_queue_draw(G.da);
  }
}

static void monochrome_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    G.colour = colour_theme_monochrome;
    gtk_widget_queue_draw(G.da);
  }
}

static void low_colour_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    G.colour = colour_theme_low_colour;
    gtk_widget_queue_draw(G.da);
  }
}

static void full_colour_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    G.colour = colour_theme_full_colour;
    gtk_widget_queue_draw(G.da);
  }
}

void key_none_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    G.key = key_theme_none;
    gtk_widget_queue_draw(G.da);
  }
}

void key_overlay_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    G.key = key_theme_overlay;
    gtk_widget_queue_draw(G.da);
  }
}

void key_detach_toggled_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  gboolean active = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
  if (active)
  {
    G.key = key_theme_detach;
    gtk_widget_queue_draw(G.da);
  }
}

static void colour_activated_cb(GtkWidget *widget, gpointer userdata)
{
  (void) userdata;
  GdkRGBA rgba = {0,0,0,1};
  gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(widget), &rgba);
  G.style.colour_r = rgba.red;
  G.style.colour_g = rgba.green;
  G.style.colour_b = rgba.blue;
  struct annotation *anno = get_selected_annotation();
  if (anno)
  {
    if (annotation_set_colour(anno, G.style.colour_r, G.style.colour_g, G.style.colour_b))
    {
      gtk_widget_queue_draw(G.da);
    }
  }
}

char **outputfiles = 0;
int noutputfiles = 0;

gboolean wait_save_exit(gpointer userdata)
{
  (void) userdata;
  if (g_thread_pool_unprocessed(G.task_workers) > 0 || g_thread_pool_get_num_threads(G.task_workers) > 0 || g_async_queue_length(G.annotation_queue) > 0)
  {
    return TRUE;
  }
  else
  {
    for (int i = 0; i < noutputfiles; ++i)
    {
      const char *ext = strrchr(outputfiles[i], '.');
      if (ext)
      {
        if (0 == strcmp(ext, ".pdf"))
        {
          save_pdf(outputfiles[i]);
        }
        else if (0 == strcmp(ext, ".png"))
        {
          save_png(outputfiles[i]);
        }
        else if (0 == strcmp(ext, ".svg"))
        {
          save_svg(outputfiles[i]);
        }
      }
    }
    exit(0);
    return FALSE;
  }
}

extern int main(int argc, char **argv) {
  memset(&G, 0, sizeof(G));
  gtk_disable_setlocale(); // FIXME TODO find a better way for safe (de)serialisation
  gtk_init(&argc, &argv);
  GdkRectangle workarea = {0};
  gdk_monitor_get_workarea(
    gdk_display_get_primary_monitor(
      gdk_display_get_default()
    ), &workarea);
  G.dpi_screen = workarea.width * 2 / 3 * 25.4 / (297 - 40);
  G.dpi_print = 0;
  const char *i_filename = 0;
  for (int i = 1; i < argc; ++i)
  {
    if (0 == strncmp(argv[i], "--dpi=", 6))
    {
      G.dpi_print = atof(argv[i] + 6);
    }
    else if (0 == strncmp(argv[i], "-i=", 3))
    {
      i_filename = argv[i] + 3;
    }
    else if (0 == strncmp(argv[i], "-o=", 3))
    {
      if (! outputfiles)
      {
        outputfiles = calloc(1, argc * sizeof(*outputfiles));
        if (! outputfiles)
        {
          fprintf(stderr, "%s: error: out of memory\n", argv[0]);
          return 1;
        }
      }
      outputfiles[noutputfiles++] = argv[i] + 3;
      const char *ext = strrchr(argv[i], '.');
      if (ext)
      {
        if (0 == strcmp(ext, ".png"))
        {
          // ok
        }
        else if (0 == strcmp(ext, ".pdf"))
        {
          // ok
        }
        else if (0 == strcmp(ext, ".svg"))
        {
          // ok
        }
        else
        {
          fprintf(stderr, "%s: error: unsupported output (%s)\n", argv[0], ext);
          return 1;
        }
      }
      else
      {
        fprintf(stderr, "%s: error: could not determine type (%s)\n", argv[0], argv[i] + 3);
        return 1;
      }
    }
    else
    {
      fprintf(stderr, "%s: error: unrecognized argument (%s)\n", argv[0], argv[i]);
      return 1;
    }
  }
  if (! (G.dpi_print > 0))
  {
    G.dpi_print = G.dpi_screen;
  }
  G.width_screen  = (297 - 40) / 25.4 * G.dpi_screen;
  G.height_screen = (210 - 40) / 25.4 * G.dpi_screen;
  G.width_print   = (297 - 40) / 25.4 * G.dpi_print;
  G.height_print  = (210 - 40) / 25.4 * G.dpi_print;
  G.threads = g_get_num_processors();
  G.dark = false;
  G.colour = colour_theme_low_colour;
  G.style.fill_type = -1;
  G.key = key_theme_none;

  G.wakes_head.u.wake.next_wake = &G.wakes_tail;
  G.wakes_tail.u.wake.pred_wake = &G.wakes_head;
  G.maximum_iterations = 1 << 16;
  G.chunk = 256;
  G.escape_radius = 512;
  G.glitch_threshold = 1e-3;
  mpfr_init2(G.escape_radius2, 53);
  mpfr_set_d(G.escape_radius2, G.escape_radius * G.escape_radius, GMP_RNDN);
  G.view = view_new();
  mpfr_set_d(G.view->cx, -0.75, GMP_RNDN);
  mpfr_set_d(G.view->cy, 0.0, GMP_RNDN);
  mpfr_set_d(G.view->radius, 1.5, GMP_RNDN);
  G.image = perturbator_new(G.threads, G.width_print, G.height_print, G.maximum_iterations, G.chunk, G.escape_radius, G.glitch_threshold);

  G.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  char title[1024];
  snprintf(title, 1024, "Mandelbrot Perturbator GTK (version %s)", VERSION);
  gtk_window_set_title(GTK_WINDOW(G.window), title);
  gtk_window_set_resizable(GTK_WINDOW(G.window), TRUE);
  g_signal_connect(G.window, "destroy", G_CALLBACK(close_window), NULL);

  GtkWidget *h0 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *v1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *h2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *v3 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *v4 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

  G.da = gtk_drawing_area_new();
  gtk_widget_set_size_request(G.da, G.width_screen, G.height_screen);
  g_signal_connect(G.da, "draw", G_CALLBACK(draw_cb), NULL);
  g_signal_connect(G.da,"configure-event", G_CALLBACK(configure_event_cb), NULL);
  gtk_widget_set_events(G.da, gtk_widget_get_events(G.da) | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK);

  GtkWidget *menu = gtk_menu_bar_new();

  GtkWidget *file = gtk_menu_item_new_with_label("File");
  GtkWidget *filem = gtk_menu_new();

  GtkWidget *open = gtk_menu_item_new_with_label("Open");
  g_signal_connect(open, "activate", G_CALLBACK(load_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), open);

  GtkWidget *save = gtk_menu_item_new_with_label("Save");
  g_signal_connect(save, "activate", G_CALLBACK(save_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), save);

  GtkWidget *save_pdf = gtk_menu_item_new_with_label("Save PDF");
  g_signal_connect(save_pdf, "activate", G_CALLBACK(save_pdf_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), save_pdf);

  GtkWidget *save_image = gtk_menu_item_new_with_label("Save PNG");
  g_signal_connect(save_image, "activate", G_CALLBACK(save_image_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), save_image);

  GtkWidget *save_svg = gtk_menu_item_new_with_label("Save SVG");
  g_signal_connect(save_svg, "activate", G_CALLBACK(save_svg_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), save_svg);

  GSList *dark_group = NULL;

  GtkWidget *dark = gtk_radio_menu_item_new_with_label(dark_group, "Theme: Dark");
  dark_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(dark));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(dark), G.dark);
  g_signal_connect(dark, "toggled", G_CALLBACK(dark_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), dark);
  G.menu_theme_dark = dark;

  GtkWidget *light = gtk_radio_menu_item_new_with_label(dark_group, "Theme: Light");
  dark_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(light));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(light), ! G.dark);
  g_signal_connect(light, "toggled", G_CALLBACK(light_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), light);
  G.menu_theme_light = light;

  GSList *colour_group = NULL;

  GtkWidget *monochrome = gtk_radio_menu_item_new_with_label(colour_group, "Colour: Monochrome");
  colour_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(monochrome));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(monochrome), G.colour == colour_theme_monochrome);
  g_signal_connect(monochrome, "toggled", G_CALLBACK(monochrome_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), monochrome);
  G.menu_colour_monochrome = monochrome;

  GtkWidget *low_colour = gtk_radio_menu_item_new_with_label(colour_group, "Colour: Low");
  colour_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(low_colour));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(low_colour), G.colour == colour_theme_low_colour);
  g_signal_connect(low_colour, "toggled", G_CALLBACK(low_colour_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), low_colour);
  G.menu_colour_low = low_colour;

  GtkWidget *full_colour = gtk_radio_menu_item_new_with_label(colour_group, "Colour: Full");
  colour_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(full_colour));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(full_colour), G.colour == colour_theme_full_colour);
  g_signal_connect(full_colour, "toggled", G_CALLBACK(full_colour_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), full_colour);
  G.menu_colour_full = full_colour;

  GSList *key_group = NULL;

  GtkWidget *key_none = gtk_radio_menu_item_new_with_label(key_group, "Key: None");
  key_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(key_none));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(key_none), G.key == key_theme_none);
  g_signal_connect(key_none, "toggled", G_CALLBACK(key_none_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), key_none);
  G.menu_key_none = key_none;

  GtkWidget *key_overlay = gtk_radio_menu_item_new_with_label(key_group, "Key: Overlay");
  key_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(key_overlay));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(key_overlay), G.key == key_theme_overlay);
  g_signal_connect(key_overlay, "toggled", G_CALLBACK(key_overlay_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), key_overlay);
  G.menu_key_overlay = key_overlay;

  GtkWidget *key_detach = gtk_radio_menu_item_new_with_label(key_group, "Key: Detach");
  key_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(key_detach));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(key_detach), G.key == key_theme_detach);
  g_signal_connect(key_detach, "toggled", G_CALLBACK(key_detach_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(filem), key_detach);
  G.menu_key_detach = key_detach;

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(file), filem);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), file);

  GSList *group = NULL;

  GtkWidget *explore = gtk_menu_item_new_with_label("Explore");
  GtkWidget *explorem = gtk_menu_new();

  GtkWidget *home = gtk_menu_item_new_with_label("Home");
  g_signal_connect(home, "activate", G_CALLBACK(home_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), home);

  GtkWidget *zoom_out = gtk_menu_item_new_with_label("Zoom Out");
  g_signal_connect(zoom_out, "activate", G_CALLBACK(zoom_out_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), zoom_out);

  GtkWidget *zoom = gtk_radio_menu_item_new_with_label(group, "Zoom");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(zoom));
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(zoom), TRUE);
  g_signal_connect(zoom, "toggled", G_CALLBACK(zoom_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), zoom);

  GtkWidget *zoom_to = gtk_menu_item_new_with_label("Zoom To");
  g_signal_connect(zoom_to, "activate", G_CALLBACK(zoom_to_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), zoom_to);

  GtkWidget *zoom_to_domain = gtk_menu_item_new_with_label("Zoom To Domain");
  g_signal_connect(zoom_to_domain, "activate", G_CALLBACK(zoom_to_domain_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), zoom_to_domain);

  GtkWidget *info = gtk_radio_menu_item_new_with_label(group, "Info");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(info));
  g_signal_connect(info, "toggled", G_CALLBACK(info_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), info);

  GtkWidget *select_ = gtk_radio_menu_item_new_with_label(group, "Select");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(select_));
  g_signal_connect(select_, "toggled", G_CALLBACK(selection_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), select_);

  GtkWidget *more_iters = gtk_menu_item_new_with_label("Increase Iterations");
  g_signal_connect(more_iters, "activate", G_CALLBACK(more_iters_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), more_iters);

  GtkWidget *fewer_iters = gtk_menu_item_new_with_label("Decrease Iterations");
  g_signal_connect(fewer_iters, "activate", G_CALLBACK(fewer_iters_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(explorem), fewer_iters);

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(explore), explorem);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), explore);


  GtkWidget *feature = gtk_menu_item_new_with_label("Feature");
  GtkWidget *featurem = gtk_menu_new();

  GtkWidget *nucleus = gtk_radio_menu_item_new_with_label(group, "Nucleus");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(nucleus));
  g_signal_connect(nucleus, "toggled", G_CALLBACK(nucleus_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), nucleus);

  GtkWidget *bond = gtk_radio_menu_item_new_with_label(group, "Bond");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(bond));
  g_signal_connect(bond, "toggled", G_CALLBACK(bond_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), bond);

  GtkWidget *misiurewicz = gtk_radio_menu_item_new_with_label(group, "Misiurewicz");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(misiurewicz));
  g_signal_connect(misiurewicz, "toggled", G_CALLBACK(misiurewicz_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), misiurewicz);

  GtkWidget *ray_out = gtk_radio_menu_item_new_with_label(group, "Ray Out");
  group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(ray_out));
  g_signal_connect(ray_out, "toggled", G_CALLBACK(ray_out_toggled_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), ray_out);

  GtkWidget *rays_of = gtk_menu_item_new_with_label("Rays Of");
  g_signal_connect(rays_of, "activate", G_CALLBACK(rays_of_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), rays_of);

  GtkWidget *ray_in = gtk_menu_item_new_with_label("Ray In");
  g_signal_connect(ray_in, "activate", G_CALLBACK(ray_in_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), ray_in);

  GtkWidget *extend = gtk_menu_item_new_with_label("Extend Ray");
  g_signal_connect(extend, "activate", G_CALLBACK(extend_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), extend);

  GtkWidget *wakel = gtk_menu_item_new_with_label("Wake ←");
  g_signal_connect(wakel, "activate", G_CALLBACK(wakel_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), wakel);

  GtkWidget *waker = gtk_menu_item_new_with_label("Wake →");
  g_signal_connect(waker, "activate", G_CALLBACK(waker_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), waker);

  GtkWidget *atom = gtk_menu_item_new_with_label("Atom");
  g_signal_connect(atom, "activate", G_CALLBACK(atom_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), atom);

  GtkWidget *domain = gtk_menu_item_new_with_label("Domain");
  g_signal_connect(domain, "activate", G_CALLBACK(domain_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), domain);

  GtkWidget *domain_estimate = gtk_menu_item_new_with_label("Domain Estimate");
  g_signal_connect(domain_estimate, "activate", G_CALLBACK(domain_estimate_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), domain_estimate);

  GtkWidget *mu_unit = gtk_menu_item_new_with_label("Mu-Unit");
  g_signal_connect(mu_unit, "activate", G_CALLBACK(muunit_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), mu_unit);

  GtkWidget *filaments = gtk_menu_item_new_with_label("Filaments");
  g_signal_connect(filaments, "activate", G_CALLBACK(filaments_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), filaments);

  GtkWidget *delete_ = gtk_menu_item_new_with_label("Delete");
  g_signal_connect(delete_, "activate", G_CALLBACK(delete_clicked_cb), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(featurem), delete_);

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(feature), featurem);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), feature);

  GtkWidget *tbar = gtk_toolbar_new();

  G.filaments_period = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(G.filaments_period), "Period");
  gtk_entry_set_width_chars(GTK_ENTRY(G.filaments_period), 6);
  GtkToolItem *filaments_period = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(filaments_period, 0);
  gtk_container_add(GTK_CONTAINER(filaments_period), G.filaments_period);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), filaments_period, -1);
  G.filaments_preperiod = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(G.filaments_preperiod), "n-Fold");
  gtk_entry_set_width_chars(GTK_ENTRY(G.filaments_preperiod), 6);
  GtkToolItem *filaments_preperiod = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(filaments_preperiod, 0);
  gtk_container_add(GTK_CONTAINER(filaments_preperiod), G.filaments_preperiod);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), filaments_preperiod, -1);
  G.filaments_depth = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(G.filaments_depth), "Depth");
  gtk_entry_set_width_chars(GTK_ENTRY(G.filaments_depth), 6);
  GtkToolItem *filaments_depth = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(filaments_depth, 0);
  gtk_container_add(GTK_CONTAINER(filaments_depth), G.filaments_depth);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), filaments_depth, -1);

  G.rays_of_nrays = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(G.rays_of_nrays), "Rays");
  gtk_entry_set_width_chars(GTK_ENTRY(G.rays_of_nrays), 6);
  GtkToolItem *rays_of_nrays = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(rays_of_nrays, 0);
  gtk_container_add(GTK_CONTAINER(rays_of_nrays), G.rays_of_nrays);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), rays_of_nrays, -1);

  for (int dark = 0; dark <= 1; ++dark)
  {
    G.dashstore[dark] = gtk_list_store_new(1, GDK_TYPE_PIXBUF);
    for (int lt = 0; lt < 16; ++lt)
    {
      cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 64, 16);
      cairo_t *cr = cairo_create(surface);
      cairo_set_source_rgba(cr, 0, 0, 0, 0);
      cairo_paint(cr);
      if (dark)
      {
        cairo_set_source_rgba(cr, 1, 1, 1, 1);
      }
      else
      {
        cairo_set_source_rgba(cr, 0, 0, 0, 1);
      }
      cairo_set_line_width(cr, 1);
      cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
      cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
      cairo_set_dash(cr, dashes[lt].pattern, dashes[lt].count, 0);
      cairo_move_to(cr, 8, 8);
      cairo_line_to(cr, 56, 8);
      cairo_stroke(cr);
      GtkTreeIter iter;
      gtk_list_store_append(G.dashstore[dark], &iter);
      gtk_list_store_set(G.dashstore[dark], &iter, 0, gdk_pixbuf_get_from_surface(surface, 0, 0, 64, 16), -1);
      cairo_destroy(cr);
      cairo_surface_destroy(surface);
    }
  }
  G.dashcombo = gtk_combo_box_new_with_model(GTK_TREE_MODEL(G.dashstore[G.dark]));
  gtk_combo_box_set_active(GTK_COMBO_BOX(G.dashcombo), 0);
  GtkCellRenderer *renderer2 = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(G.dashcombo), renderer2, FALSE);
  gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(G.dashcombo), renderer2, "pixbuf", 0, NULL);
  g_signal_connect(G.dashcombo, "changed", G_CALLBACK(dashing_selection_changed_cb), NULL);
  GtkToolItem *dashing = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(dashing, 0);
  gtk_container_add(GTK_CONTAINER(dashing), G.dashcombo);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), dashing, -1);

  initialize_fill_patterns();
  for (int dark = 0; dark <= 1; ++dark)
  {
    G.fillstore[dark] = gtk_list_store_new(1, GDK_TYPE_PIXBUF);
    for (int ft = -1; ft < 26; ++ft)
    {
      cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 64, 16);
      cairo_t *cr = cairo_create(surface);
      cairo_set_source_rgba(cr, 0, 0, 0, 0);
      cairo_paint(cr);
      if (ft >= 0)
      {
        cairo_set_source(cr, G.fillpattern[dark][ft]);
        cairo_paint(cr);
      }
      GtkTreeIter iter;
      gtk_list_store_append(G.fillstore[dark], &iter);
      gtk_list_store_set(G.fillstore[dark], &iter, 0, gdk_pixbuf_get_from_surface(surface, 0, 0, 64, 16), -1);
      cairo_destroy(cr);
      cairo_surface_destroy(surface);
    }
  }
  G.fillcombo = gtk_combo_box_new_with_model(GTK_TREE_MODEL(G.fillstore[G.dark]));
  gtk_combo_box_set_active(GTK_COMBO_BOX(G.fillcombo), 0);
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(G.fillcombo), renderer2, FALSE);
  gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(G.fillcombo), renderer2, "pixbuf", 0, NULL);
  g_signal_connect(G.fillcombo, "changed", G_CALLBACK(fill_selection_changed_cb), NULL);
  GtkToolItem *filling = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(filling, 0);
  gtk_container_add(GTK_CONTAINER(filling), G.fillcombo);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), filling, -1);

  G.colourbutton = gtk_color_button_new();
  g_signal_connect(G.colourbutton, "color-set", G_CALLBACK(colour_activated_cb), NULL);
  GtkToolItem *colortool = gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(colortool, 0);
  gtk_container_add(GTK_CONTAINER(colortool), G.colourbutton);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), colortool, -1);

  gtk_container_add(GTK_CONTAINER(h0), menu);
  gtk_container_add(GTK_CONTAINER(h0), tbar);

  G.log = gtk_text_view_new();
  gtk_text_view_set_editable(GTK_TEXT_VIEW(G.log), FALSE);
  gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(G.log), FALSE);
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(G.log), GTK_WRAP_CHAR);
  GtkWidget *scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_size_request(scroll, G.width_screen, -1);
  gtk_widget_set_vexpand(G.log, TRUE);
  gtk_widget_set_hexpand(G.log, FALSE);
  gtk_widget_set_vexpand(scroll, TRUE);
  gtk_widget_set_hexpand(scroll, FALSE);
  gtk_container_add(GTK_CONTAINER(scroll), G.log);

  G.annostore = gtk_tree_store_new(2, G_TYPE_STRING, G_TYPE_POINTER);
  G.annotree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(G.annostore));
  g_object_unref(G_OBJECT(G.annostore));
  GtkTreeIter iter;
  gtk_tree_store_append(G.annostore, &iter, NULL);
  gtk_tree_store_set(G.annostore, &iter, 0, "(none)", 1, NULL, -1);
  GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
  GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes("Annotations", renderer, "text", 0, NULL);
  gtk_tree_view_column_set_sort_column_id(column, 0);
  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(G.annostore), 0, sortable_annotation_compare, 0, 0);
  gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(G.annostore), 0, GTK_SORT_ASCENDING);

  gtk_tree_view_append_column(GTK_TREE_VIEW(G.annotree), column);
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
  g_signal_connect(select, "changed", G_CALLBACK(annotree_selection_changed_cb), NULL);
  GtkWidget *scroll2 = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_hexpand(G.annotree, TRUE);
  gtk_widget_set_vexpand(G.annotree, TRUE);
  gtk_widget_set_hexpand(scroll2, TRUE);
  gtk_widget_set_vexpand(scroll2, TRUE);
  gtk_container_add(GTK_CONTAINER(scroll2), G.annotree);

  G.annotation_queue = g_async_queue_new();
  g_thread_new("annotate", annotation_thread, G.annotation_queue);
  G.task_workers = g_thread_pool_new(task_func, NULL, g_get_num_processors(), FALSE, NULL);
  G.task_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *scroll3 = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_size_request(scroll3, -1, -1);
  gtk_widget_set_vexpand(G.task_box, TRUE);
  gtk_widget_set_hexpand(G.task_box, TRUE);
  gtk_widget_set_vexpand(scroll3, TRUE);
  gtk_widget_set_hexpand(scroll3, TRUE);
  gtk_container_add(GTK_CONTAINER(scroll3), G.task_box);

  gtk_container_add(GTK_CONTAINER(G.window), v1);
  gtk_container_add(GTK_CONTAINER(v1), h0);
  gtk_container_add(GTK_CONTAINER(v1), h2);
  gtk_container_add(GTK_CONTAINER(h2), v3);
  gtk_container_add(GTK_CONTAINER(v3), G.da);
  gtk_container_add(GTK_CONTAINER(v3), scroll);
  gtk_container_add(GTK_CONTAINER(h2), v4);
  gtk_container_add(GTK_CONTAINER(v4), scroll2);
  gtk_container_add(GTK_CONTAINER(v4), scroll3);

  gtk_window_maximize(GTK_WINDOW(G.window));
  gtk_widget_show_all(G.window);
  zoom_toggled_cb(zoom, 0);

  if (i_filename)
  {
    if (load_file(i_filename))
    {
      fprintf(stderr, "%s: error: could not load file (%s)\n", argv[0], i_filename);
      return 1;
    }
  }
  if (noutputfiles)
  {
    g_timeout_add(10, wait_save_exit, 0);
  }
  gtk_main();
  return 0;
}

enum task_t
{
  task_ray_out,
  task_ray_in,
  task_ray_extend,
  task_nucleus,
  task_bond,
  task_misiurewicz,
  task_muunit,
  task_muunit_nucleus,
  task_rays_of,
  task_filaments,
  task_wake,
  task_wake2, // traces its own rays
  task_atom,
  task_domain,
  task_misiurewicz_domain
};

struct task_ray_out
{
  mpc_t c;
  int dwell;
};

struct task_ray_in
{
  m_binangle angle;
  int depth;
};

struct task_ray_extend
{
  struct annotation *anno;
  int depth;
};

struct task_nucleus
{
  mpc_t c;
  mpfr_t r;
  int maxperiod;
};

struct task_bond
{
  mpc_t c;
  int period;
};

struct task_misiurewicz
{
  mpc_t c;
  mpfr_t r;
  int maxpreperiod;
  int maxperiod;
};

struct task_muunit
{
  mpc_t nucleus;
  int period;
};

struct task_muunit_nucleus
{
  mpc_t nucleus;
  int nucleus_period;
  int ray_period;
  mpq_t angle;
};

struct task_filaments
{
  mpc_t inner_nucleus;
  int inner_period;
  int outer_period;
  int preperiod;
  int depth;
};

struct task_rays_of
{
  mpc_t nucleus;
  int count;
  int preperiod;
  int period;
};

struct task_wake
{
  struct annotation *lo;
  struct annotation *hi;
};

struct task_wake2
{
  char *lo;
  char *hi;
  int depth;
};

struct task_atom
{
  mpc_t nucleus;
  int period;
};

struct task_domain
{
  mpc_t nucleus;
  int loperiod;
  int period;
};

struct task_misiurewicz_domain
{
  mpc_t misiurewicz;
  int period;
  int lopreperiod;
  int hipreperiod;
};

struct task
{
  enum task_t tag;
  char *label;
  GAsyncQueue *output;
  GtkWidget *hbox;
  GtkWidget *progress_bar;
  double progress;
  bool cancelled;
  struct annotation_style style;
  union
  {
    struct task_ray_out ray_out;
    struct task_ray_in ray_in;
    struct task_ray_extend ray_extend;
    struct task_nucleus nucleus;
    struct task_bond bond;
    struct task_misiurewicz misiurewicz;
    struct task_muunit muunit;
    struct task_muunit_nucleus muunit_nucleus;
    struct task_filaments filaments;
    struct task_rays_of rays_of;
    struct task_wake wake;
    struct task_wake2 wake2;
    struct task_atom atom;
    struct task_domain domain;
    struct task_misiurewicz_domain misiurewicz_domain;
  } u;
};

static void task_cancel_cb(GtkWidget *cancel, gpointer userdata)
{
  struct task *t = userdata;
  t->cancelled = true;
  gtk_widget_set_sensitive(cancel, FALSE);
}

static gboolean task_enqueue_cb(gpointer userdata)
{
  struct task *t = userdata;
  t->progress_bar = gtk_progress_bar_new();
  GtkWidget *label = gtk_label_new(t->label);
  GtkWidget *cancel = gtk_button_new_with_label("Cancel");
  g_signal_connect(cancel, "clicked", G_CALLBACK(task_cancel_cb), t);
  t->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_container_add(GTK_CONTAINER(t->hbox), cancel);
  gtk_container_add(GTK_CONTAINER(t->hbox), t->progress_bar);
  gtk_container_add(GTK_CONTAINER(t->hbox), label);
  gtk_container_add(GTK_CONTAINER(G.task_box), t->hbox);
  gtk_widget_show_all(G.task_box);
  g_thread_pool_push(G.task_workers, t, NULL);
  return G_SOURCE_REMOVE;
}

static void task_enqueue(struct task *t)
{
  gdk_threads_add_idle(task_enqueue_cb, t);
}

static gboolean task_set_progress_cb(gpointer userdata)
{
  struct task *t = userdata;
  if (t->progress_bar)
  {
    if (t->progress == 0.0)
    {
      gtk_progress_bar_pulse(GTK_PROGRESS_BAR(t->progress_bar));
    }
    else
    {
      gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(t->progress_bar), t->progress);
    }
  }
  return G_SOURCE_REMOVE;
}

static void task_set_progress(struct task *t)
{
  gdk_threads_add_idle(task_set_progress_cb, t);
}

static gboolean task_destroy_cb(gpointer userdata)
{
  struct task *t = userdata;
  t->output = 0;
  if (t->hbox)
  {
    gtk_widget_destroy(t->hbox);
    t->hbox = 0;
  }
  t->progress_bar = 0;
  if (t->label)
  {
    free(t->label);
    t->label = 0;
  }
  switch (t->tag)
  {
    case task_ray_out:
    {
      mpc_clear(t->u.ray_out.c);
      break;
    }
    case task_ray_in:
    {
      m_binangle_clear(&t->u.ray_in.angle);
      break;
    }
    case task_ray_extend:
    {
      // nop, always succeeds
      break;
    }
    case task_nucleus:
    {
      mpc_clear(t->u.nucleus.c);
      mpfr_clear(t->u.nucleus.r);
      break;
    }
    case task_bond:
    {
      mpc_clear(t->u.bond.c);
      break;
    }
    case task_misiurewicz:
    {
      mpc_clear(t->u.misiurewicz.c);
      mpfr_clear(t->u.misiurewicz.r);
      break;
    }
    case task_muunit:
    {
      mpc_clear(t->u.muunit.nucleus);
      break;
    }
    case task_muunit_nucleus:
    {
      mpc_clear(t->u.muunit_nucleus.nucleus);
      mpq_clear(t->u.muunit_nucleus.angle);
      break;
    }
    case task_filaments:
    {
      mpc_clear(t->u.filaments.inner_nucleus);
      break;
    }
    case task_rays_of:
    {
      mpc_clear(t->u.rays_of.nucleus);
      break;
    }
    case task_wake:
    {
      // nop, always succeeds
      break;
    }
    case task_wake2:
    {
      free(t->u.wake2.lo);
      free(t->u.wake2.hi);
      break;
    }
    case task_atom:
    {
      mpc_clear(t->u.atom.nucleus);
      break;
    }
    case task_domain:
    {
      mpc_clear(t->u.domain.nucleus);
      break;
    }
    case task_misiurewicz_domain:
    {
      mpc_clear(t->u.misiurewicz_domain.misiurewicz);
      break;
    }
  }
  free(t);
  return G_SOURCE_REMOVE;
}

static void task_done(struct task *t)
{
  gdk_threads_add_idle(task_destroy_cb, t);
}

static struct task *task_new_ray_out(GAsyncQueue *output, const mpc_t c, int dwell, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Ray Out from %+Re + %Re i dwell %d";
  int bytes = mpfr_snprintf(NULL, 0, format, mpc_realref(c), mpc_imagref(c), dwell);
  if (bytes < 0)
  {
    bytes = 0; // error?
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(c), mpc_imagref(c), dwell);
  if (bytes == 0)
  {
    t->label[bytes] = 0; // paranoia
  }
  t->tag = task_ray_out;
  mpc_init2(t->u.ray_out.c, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.ray_out.c, c, MPC_RNDNN);
  t->u.ray_out.dwell = dwell;
  return t;
}

static struct task *task_new_ray_in(GAsyncQueue *output, const m_binangle *angle, int depth, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  t->label = m_binangle_to_new_string(angle);
  t->tag = task_ray_in;
  m_binangle_init(&t->u.ray_in.angle);
  m_binangle_set(&t->u.ray_in.angle, angle);
  t->u.ray_in.depth = depth;
  return t;
}

static struct task *task_new_ray_extend(GAsyncQueue *output, struct annotation *anno, int depth, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  t->label = strdup(anno->label);
  t->tag = task_ray_extend;
  t->u.ray_extend.anno = anno;
  t->u.ray_extend.depth = depth;
  return t;
}

static struct task *task_new_nucleus(GAsyncQueue *output, const mpc_t c, const mpfr_t r, int dwell, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "nucleus near %+Re + %+Re i @ %Re";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(c), mpc_imagref(c), r);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(c), mpc_imagref(c), r);
  t->tag = task_nucleus;
  mpc_init2(t->u.nucleus.c, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.nucleus.c, c, MPC_RNDNN);
  mpfr_init2(t->u.nucleus.r, 53);
  mpfr_set(t->u.nucleus.r, r, MPFR_RNDN);
  t->u.nucleus.maxperiod = dwell;
  return t;
}

static struct task *task_new_bond(GAsyncQueue *output, const mpc_t c, int period, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Bond near %+Re + %+Re i P %d";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(c), mpc_imagref(c), period);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(c), mpc_imagref(c), period);
  t->tag = task_bond;
  mpc_init2(t->u.bond.c, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.bond.c, c, MPC_RNDNN);
  t->u.bond.period = period;
  return t;
}

static struct task *task_new_misiurewicz(GAsyncQueue *output, const mpc_t c, const mpfr_t r, int maxpreperiod, int maxperiod, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Misiurewicz near %+Re + %+Re i @ %Re";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(c), mpc_imagref(c), r);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(c), mpc_imagref(c), r);
  t->tag = task_misiurewicz;
  mpc_init2(t->u.misiurewicz.c, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.misiurewicz.c, c, MPC_RNDNN);
  mpfr_init2(t->u.misiurewicz.r, 53);
  mpfr_set(t->u.misiurewicz.r, r, MPFR_RNDN);
  t->u.misiurewicz.maxpreperiod = maxpreperiod;
  t->u.misiurewicz.maxperiod = maxperiod;
  return t;
}

static struct task *task_new_muunit(GAsyncQueue *output, const mpc_t c, int period, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Mu-unit near %+Re + %+Re i P %d";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(c), mpc_imagref(c), period);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(c), mpc_imagref(c), period);
  t->tag = task_muunit;
  mpc_init2(t->u.muunit.nucleus, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.muunit.nucleus, c, MPC_RNDNN);
  t->u.muunit.period = period;
  return t;
}

static struct task *task_new_muunit_nucleus(GAsyncQueue *output, const mpc_t nucleus, int nucleus_period, int ray_period, const mpq_t angle, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Mu-unit nucleus near %+Re + %+Re i P %d = %d * %d";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(nucleus), mpc_imagref(nucleus), nucleus_period * ray_period, nucleus_period, ray_period);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(nucleus), mpc_imagref(nucleus), nucleus_period * ray_period, nucleus_period, ray_period);
  t->tag = task_muunit_nucleus;
  mpc_init2(t->u.muunit_nucleus.nucleus, mpfr_get_prec(mpc_realref(nucleus)));
  mpc_set(t->u.muunit_nucleus.nucleus, nucleus, MPC_RNDNN);
  t->u.muunit_nucleus.nucleus_period = nucleus_period;
  t->u.muunit_nucleus.ray_period = ray_period;
  mpq_init(t->u.muunit_nucleus.angle);
  mpq_set(t->u.muunit_nucleus.angle, angle);
  return t;
}

static struct task *task_new_filaments(GAsyncQueue *output, const mpc_t inner_nucleus, int inner_period, int outer_period, int preperiod, int depth, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Filaments near %+Re + %+Re i P %d / %d / %d";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(inner_nucleus), mpc_imagref(inner_nucleus), inner_period, outer_period, preperiod);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(inner_nucleus), mpc_imagref(inner_nucleus), inner_period, outer_period, preperiod);
  t->tag = task_filaments;
  mpc_init2(t->u.filaments.inner_nucleus, mpfr_get_prec(mpc_realref(inner_nucleus)));
  mpc_set(t->u.filaments.inner_nucleus, inner_nucleus, MPC_RNDNN);
  t->u.filaments.inner_period = inner_period;
  t->u.filaments.outer_period = outer_period;
  t->u.filaments.preperiod = preperiod;
  t->u.filaments.depth = depth;
  return t;
}

static struct task *task_new_rays_of(GAsyncQueue *output, int count, int preperiod, int period, const mpc_t nucleus, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Rays Of %dp%d%%%d near %+Re + %+Re i";
  int bytes = mpfr_snprintf(0, 0, format, preperiod, period, count, mpc_realref(nucleus), mpc_imagref(nucleus));
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, preperiod, period, count, mpc_realref(nucleus), mpc_imagref(nucleus));
  t->tag = task_rays_of;
  mpc_init2(t->u.rays_of.nucleus, mpfr_get_prec(mpc_realref(nucleus)));
  mpc_set(t->u.rays_of.nucleus, nucleus, MPC_RNDNN);
  t->u.rays_of.count = count;
  t->u.rays_of.preperiod = preperiod;
  t->u.rays_of.period = period;
  return t;
}

static struct task *task_new_wake(GAsyncQueue *output, struct annotation *lo, struct annotation *hi, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  t->label = strdup("Wake");
  t->tag = task_wake;
  t->u.wake.lo = lo;
  t->u.wake.hi = hi;
  return t;
}

static struct task *task_new_wake2(GAsyncQueue *output, char *lo, char *hi, int depth, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  t->label = strdup("Wake");
  t->tag = task_wake2;
  t->u.wake2.lo = strdup(lo);
  t->u.wake2.hi = strdup(hi);
  t->u.wake2.depth = depth;
  return t;
}

static struct task *task_new_atom(GAsyncQueue *output, const mpc_t c, int period, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Atom near %+Re + %+Re i P %d";
  int bytes = mpfr_snprintf(0, 0, format, mpc_realref(c), mpc_imagref(c), period);
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, mpc_realref(c), mpc_imagref(c), period);
  t->tag = task_atom;
  mpc_init2(t->u.atom.nucleus, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.atom.nucleus, c, MPC_RNDNN);
  t->u.atom.period = period;
  return t;
}

static struct task *task_new_domain(GAsyncQueue *output, const mpc_t c, int period, int loperiod, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Domain %dp%d near %+Re + %+Re i ";
  int bytes = mpfr_snprintf(0, 0, format, loperiod, period, mpc_realref(c), mpc_imagref(c));
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, loperiod, period, mpc_realref(c), mpc_imagref(c));
  t->tag = task_domain;
  mpc_init2(t->u.domain.nucleus, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.domain.nucleus, c, MPC_RNDNN);
  t->u.domain.period = period;
  t->u.domain.loperiod = loperiod;
  return t;
}

static struct task *task_new_misiurewicz_domain(GAsyncQueue *output, const mpc_t c, int period, int lopreperiod, int hipreperiod, const struct annotation_style *style)
{
  struct task *t = calloc(1, sizeof(*t));
  t->output = output;
  t->style = *style;
  const char *format = "Domain %d/%dp%d near %+Re + %+Re i ";
  int bytes = mpfr_snprintf(0, 0, format, lopreperiod, hipreperiod, period, mpc_realref(c), mpc_imagref(c));
  if (bytes < 0)
  {
    bytes = 0;
  }
  t->label = malloc(bytes + 1);
  mpfr_snprintf(t->label, bytes + 1, format, lopreperiod, hipreperiod, period, mpc_realref(c), mpc_imagref(c));
  t->tag = task_misiurewicz_domain;
  mpc_init2(t->u.misiurewicz_domain.misiurewicz, mpfr_get_prec(mpc_realref(c)));
  mpc_set(t->u.misiurewicz_domain.misiurewicz, c, MPC_RNDNN);
  t->u.misiurewicz_domain.period = period;
  t->u.misiurewicz_domain.lopreperiod = lopreperiod;
  t->u.misiurewicz_domain.hipreperiod = hipreperiod;
  return t;
}

static gboolean annotation_cb(gpointer userdata)
{
  struct annotation *a = userdata;
  add_annotation(a);
  gtk_widget_queue_draw(G.da);
  return G_SOURCE_REMOVE;
}

static gpointer annotation_thread(gpointer userdata)
{
  GAsyncQueue *q = userdata;
  struct annotation *a;
  while ((a = g_async_queue_pop(q)))
  {
    gdk_threads_add_idle(annotation_cb, a);
  }
  return 0;
}

static void task_func(gpointer data, gpointer userdata)
{
  (void) userdata;
  struct task *t = data;
  switch (t->tag)
  {

    case task_ray_out:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->tag = annotation_ray_out;
      a->style = t->style;
      char *accumulated_bits = malloc(t->u.ray_out.dwell + 10);
      int i = 0;
      int sharpness = 16;
      m_r_exray_out *ray = m_r_exray_out_new(t->u.ray_out.c, sharpness, t->u.ray_out.dwell * 2, 65536);
      double total = 0.5 * sharpness * t->u.ray_out.dwell * (1 + t->u.ray_out.dwell);
      double progress = 0;
      double increment = t->u.ray_out.dwell;
      m_newton success = m_failed;
      int first = 1;
      while (! t->cancelled && (m_failed != (success = m_r_exray_out_step(ray))))
      {
        progress = progress + increment;
        t->progress = progress / total;
        task_set_progress(t);
        a->u.ray_out.line_start = point_new(a->u.ray_out.line_start);
        if (first)
        {
          first = 0;
          a->u.ray_out.line_end = a->u.ray_out.line_start;
        }
        m_r_exray_out_get(ray, a->u.ray_out.line_start->xy);
        if (m_r_exray_out_have_bit(ray))
        {
          increment = increment - 1;
          bool bit = m_r_exray_out_get_bit(ray);
          accumulated_bits[i++] = bit ? '1' : '0';
          if (i >= t->u.ray_out.dwell + 10 - 1)
          {
            // should never happen?
            success = m_failed;
            break;
          }
        }
        if (success == m_converged)
        {
          break;
        }
      }
      if (! t->cancelled && success == m_converged)
      {
        accumulated_bits[i] = 0;
        int nbits = i;
        char *reversed_bits = malloc(nbits + 1);
        for (int i = 0; i < nbits; ++i)
        {
          reversed_bits[i] = accumulated_bits[nbits-1 - i];
        }
        reversed_bits[nbits] = 0;
        a->label = reversed_bits;
        g_async_queue_push(t->output, a);
      }
      else
      {
        free_annotation(a);
      }
      free(accumulated_bits);
      m_r_exray_out_delete(ray);
      break;
    } // task_ray_out

    case task_ray_in:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->style = t->style;
      a->label = m_binangle_to_new_string(&t->u.ray_in.angle);
      a->tag = annotation_ray_in;
      m_binangle_init(&a->u.ray_in.angle);
      m_binangle_set(&a->u.ray_in.angle, &t->u.ray_in.angle);
      int depth = t->u.ray_in.depth;
      int sharpness = 16;
      mpq_t q;
      mpq_init(q);
      m_binangle_to_rational(q, &t->u.ray_in.angle);
      mpq_canonicalize(q);
      m_r_exray_in *ray = m_r_exray_in_new(q, sharpness);
      mpq_clear(q);
      int i = 0;
      double total = 0.5 * sharpness * depth * (1 + depth);
      double progress = 0;
      int increment = 1;
      m_newton success = m_failed;
      int first = 1;
      while (! t->cancelled && (m_failed != (success = m_r_exray_in_step(ray, 64))))
      {
        progress = progress + increment;
        t->progress = progress / total;
        task_set_progress(t);
        a->u.ray_in.line_start = point_new(a->u.ray_in.line_start);
        m_r_exray_in_get(ray, a->u.ray_in.line_start->xy);
        if (first)
        {
          first = 0;
          a->u.ray_in.line_end = a->u.ray_in.line_start;
        }
        if (++i >= sharpness)
        {
          i = 0;
          increment = increment + 1;
          if (increment >= depth)
          {
            success = m_converged;
            break;
          }
        }
        if (success == m_converged)
        {
          break;
        }
      }
      a->u.ray_in.ray = ray;
      a->u.ray_in.depth = increment;
      if (! t->cancelled && success == m_converged)
      {
        g_async_queue_push(t->output, a);
      }
      else
      {
        free_annotation(a);
      }
      break;
    } // task_ray_in

    case task_ray_extend:
    {
      struct annotation *a = t->u.ray_extend.anno;
      a->style = t->style;
      m_r_exray_in *ray = a->u.ray_in.ray;
      int sharpness = 16; // FIXME must match ray_in above
      int start_depth = a->u.ray_in.depth;
      int end_depth = t->u.ray_extend.depth;
      int i = 0;
      double total = 0.5 * sharpness * (end_depth - start_depth) * (end_depth + start_depth);
      double progress = 0;
      int increment = start_depth;
      m_newton success = m_failed;
      while (! t->cancelled && (m_failed != (success = m_r_exray_in_step(ray, 64))))
      {
        progress = progress + increment;
        t->progress = progress / total;
        task_set_progress(t);
        a->u.ray_in.line_start = point_new(a->u.ray_in.line_start);
        m_r_exray_in_get(ray, a->u.ray_in.line_start->xy);
        if (++i >= sharpness)
        {
          i = 0;
          increment = increment + 1;
          if (increment >= end_depth)
          {
            success = m_converged;
            break;
          }
        }
        if (success == m_converged)
        {
          break;
        }
      }
      a->u.ray_in.depth = increment;
      g_async_queue_push(t->output, a);
      break;
    } // task_ray_extend

    case task_nucleus:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->style = t->style;
      a->tag = annotation_nucleus;
      int prec = 53 - 2 * mpfr_get_exp(t->u.nucleus.r);
      mpc_init2(a->u.nucleus.xy, prec);
      mpfr_init2(a->u.nucleus.size, 53);
      mpfr_init2(a->u.nucleus.domain_size, 53);
      int period = m_r_ball_period_do(t->u.nucleus.c, t->u.nucleus.r, t->u.nucleus.maxperiod);
      task_set_progress(t);
      bool done = false;
      if (period && ! t->cancelled)
      {
        a->label = malloc(100);
        snprintf(a->label, 99, "%d", period);
        a->u.nucleus.period = period;
        if (prec > 4 * period + 53) prec = 4 * period + 53;
        if (prec < 53) prec = 53;
        mpfr_prec_round(mpc_realref(a->u.nucleus.xy), prec, MPFR_RNDN);
        mpfr_prec_round(mpc_imagref(a->u.nucleus.xy), prec, MPFR_RNDN);
        mpc_set(a->u.nucleus.xy, t->u.nucleus.c, MPC_RNDNN);
        m_newton ok = m_r_nucleus(a->u.nucleus.xy, a->u.nucleus.xy, period, 64, 0);
        task_set_progress(t);
        if (ok != m_failed && ! t->cancelled)
        {
          mpc_t delta;
          mpc_init2(delta, 53);
          mpc_sub(delta, t->u.nucleus.c, a->u.nucleus.xy, MPC_RNDNN);
          mpfr_t distance;
          mpfr_init2(distance, 53);
          mpc_abs(distance, delta, MPFR_RNDN);
          bool nearby = mpfr_less_p(distance, t->u.nucleus.r);
          mpc_clear(delta);
          mpfr_clear(distance);
          task_set_progress(t);
          if (nearby && ! t->cancelled)
          {
            mpc_t size;
            mpc_init2(size, 53);
            m_r_size(size, a->u.nucleus.xy, period);
            mpc_abs(a->u.nucleus.size, size, MPFR_RNDN);
            mpc_clear(size);
            task_set_progress(t);
            if (! t->cancelled)
            {
              prec = 53 - mpfr_get_exp(a->u.nucleus.size);
              if (prec > 4 * period + 53) prec = 4 * period + 53;
              if (prec < 53) prec = 53;
              mpfr_prec_round(mpc_realref(a->u.nucleus.xy), prec, MPFR_RNDN);
              mpfr_prec_round(mpc_imagref(a->u.nucleus.xy), prec, MPFR_RNDN);
              m_r_nucleus(a->u.nucleus.xy, a->u.nucleus.xy, period, 2, 0);
              task_set_progress(t);
              if (! t->cancelled)
              {
                m_r_domain_size(a->u.nucleus.domain_size, a->u.nucleus.xy, period);
                task_set_progress(t);
                if (! t->cancelled)
                {
                  g_async_queue_push(t->output, a);
                  done = true;
                }
              }
            }
          }
        }
      }
      if (! done)
      {
        free_annotation(a);
      }
      break;
    } // task_nucleus

    case task_bond:
    {
      bool done = false;
      struct annotation *a = 0;
      mpfr_prec_t prec = mpfr_get_prec(mpc_realref(t->u.bond.c));
      mpc_t c, root, parent;
      mpc_init2(c, prec);
      mpc_init2(root, prec);
      mpc_init2(parent, prec);
      mpc_set(c, t->u.bond.c, MPC_RNDNN);
      m_r_nucleus(c, c, t->u.bond.period, 64, 0);
      task_set_progress(t);
      mpq_t angle;
      mpq_init(angle);
      if (! t->cancelled)
      {
        int p = m_r_parent(angle, root, parent, c, t->u.bond.period, 16, 0);
        task_set_progress(t);
        if (p && ! t->cancelled) {
          a = calloc(1, sizeof(*a));
          a->style = t->style;
          a->label = malloc(100);
          mpfr_snprintf(a->label, 99, "%Qd", angle);
          a->tag = annotation_text;
          mpc_init2(a->u.text.xy, mpfr_get_prec(mpc_realref(root)));
          mpc_set(a->u.text.xy, root, MPC_RNDNN);
          g_async_queue_push(t->output, a);
          done = true;
        }
      }
      mpq_clear(angle);
      mpc_clear(root);
      mpc_clear(parent);
      mpc_clear(c);
      if (! done)
      {
        if (a)
        {
          free_annotation(a);
        }
      }
      break;
    } // task_bond

    case task_misiurewicz:
    {
      bool done = false;
      struct annotation *a = 0;
      int preperiod = -1, period = 0;
      if (m_r_box_misiurewicz_do(&preperiod, &period, t->u.misiurewicz.c, t->u.misiurewicz.r, t->u.misiurewicz.maxpreperiod, t->u.misiurewicz.maxperiod))
      {
        task_set_progress(t);
        if (! t->cancelled)
        {
          a = calloc(1, sizeof(*a));
          a->style = t->style;
          a->label = malloc(100);
          snprintf(a->label, 99, "%dp%d", preperiod, period);
          a->tag = annotation_misiurewicz;
          a->u.misiurewicz.preperiod = preperiod;
          a->u.misiurewicz.period = period;
          mpc_init2(a->u.misiurewicz.xy, mpfr_get_prec(mpc_realref(t->u.misiurewicz.c)));
          mpfr_init2(a->u.misiurewicz.domain_size, 53);
          m_r_misiurewicz(a->u.misiurewicz.xy, t->u.misiurewicz.c, preperiod, period, 64);
          task_set_progress(t);
          if (! t->cancelled)
          {
            m_r_misiurewicz_naive(a->u.misiurewicz.xy, a->u.misiurewicz.xy, preperiod, period, 64);
            task_set_progress(t);
            if (! t->cancelled)
            {
              m_r_misiurewicz_size(a->u.misiurewicz.domain_size, a->u.misiurewicz.xy, preperiod, period);
              task_set_progress(t);
              if (! t->cancelled)
              {
                g_async_queue_push(t->output, a);
                done = true;
              }
            }
          }
        }
      }
      if (! done)
      {
        if (a)
        {
          free_annotation(a);
        }
      }
      break;
    } // task_misiurewicz

    case task_muunit:
    {
      mpq_t q;
      mpq_init(q);
      for (int period = 2; period <= 6; ++period)
      {
        int den = (1 << period) - 1;
        for (int num = 1; num < den; num += 2)
        {
          mpq_set_ui(q, num, den);
          mpq_canonicalize(q);
          task_enqueue(task_new_muunit_nucleus(t->output, t->u.muunit.nucleus, t->u.muunit.period, period, q, &t->style));
        }
      }
      mpq_clear(q);
      break;
    } // task muunit

    case task_muunit_nucleus:
    {
      bool done = false;
      struct annotation *a = 0;
      mpc_t c, z, cc;
      mpc_init2(c, 53);
      mpc_init2(z, 53);
      mpc_init2(cc, 53);
      m_r_exray_in *ray = m_r_exray_in_new(t->u.muunit_nucleus.angle, 16);
      for (int i = 0; i < 16 * 4 * t->u.muunit_nucleus.ray_period; ++i)
      {
        m_r_exray_in_step(ray, 64);
      }
      m_r_exray_in_get(ray, c);
      m_r_exray_in_delete(ray);
      if (m_failed != m_r_nucleus(c, c, t->u.muunit_nucleus.ray_period, 64, 0) && ! t->cancelled)
      {
        task_set_progress(t);
        mpc_set_dc(z, 0, MPC_RNDNN);
        for (int r = 0; r <= 64; ++r)
        {
          mpfr_mul_d(mpc_realref(cc), mpc_realref(c), r / 64.0, MPFR_RNDN);
          mpfr_mul_d(mpc_imagref(cc), mpc_imagref(c), r / 64.0, MPFR_RNDN);
          if (m_failed == m_r_attractor(z, z, cc, 1, 64)) break;
        }
        mpfr_mul_d(mpc_realref(cc), mpc_realref(z), 2, MPFR_RNDN);
        mpfr_mul_d(mpc_imagref(cc), mpc_imagref(z), 2, MPFR_RNDN);
        mpfr_prec_t prec = mpfr_get_prec(mpc_realref(t->u.muunit_nucleus.nucleus));
        mpfr_set_prec(mpc_realref(z), prec);
        mpfr_set_prec(mpc_imagref(z), prec);
        mpfr_set_prec(mpc_realref(c), prec);
        mpfr_set_prec(mpc_imagref(c), prec);
        mpc_set_dc(z, 0, MPC_RNDNN);
        if (m_failed != m_r_interior(z, c, z, t->u.muunit_nucleus.nucleus, cc, t->u.muunit_nucleus.nucleus_period, 64) && ! t->cancelled)
        {
          task_set_progress(t);
          int period = t->u.muunit_nucleus.nucleus_period * t->u.muunit_nucleus.ray_period;
          if (m_failed != m_r_nucleus(c, c, period, 64, 0) && ! t->cancelled)
          {
            task_set_progress(t);
            a = calloc(1, sizeof(*a));
            a->style = t->style;
            a->label = malloc(100);
            snprintf(a->label, 99, "%d", period);
            a->tag = annotation_nucleus;
            if (prec > 4 * period + 53) prec = 4 * period + 53;
            if (prec < 53) prec = 53;
            mpc_init2(a->u.nucleus.xy, prec);
            mpfr_init2(a->u.nucleus.size, 53);
            mpfr_init2(a->u.nucleus.domain_size, 53);
            a->u.nucleus.period = period;
            if (m_failed != m_r_nucleus(a->u.nucleus.xy, c, period, 64, 0) && ! t->cancelled)
            {
              task_set_progress(t);
              mpc_t size;
              mpc_init2(size, 53);
              m_r_size(size, a->u.nucleus.xy, period);
              mpc_abs(a->u.nucleus.size, size, MPFR_RNDN);
              mpc_clear(size);
              task_set_progress(t);
              if (! t->cancelled)
              {
                prec = 53 - mpfr_get_exp(a->u.nucleus.size);
                if (prec > 4 * period + 53) prec = 4 * period + 53;
                if (prec < 53) prec = 53;
                mpfr_prec_round(mpc_realref(a->u.nucleus.xy), prec, MPFR_RNDN);
                mpfr_prec_round(mpc_imagref(a->u.nucleus.xy), prec, MPFR_RNDN);
                m_r_nucleus(a->u.nucleus.xy, a->u.nucleus.xy, period, 2, 0);
                task_set_progress(t);
                if (! t->cancelled)
                {
                  m_r_domain_size(a->u.nucleus.domain_size, a->u.nucleus.xy, period);
                  task_set_progress(t);
                  if (! t->cancelled)
                  {
                    g_async_queue_push(t->output, a);
                    done = true;
                  }
                }
              }
            }
          }
        }
      }
      if (! done)
      {
        if (a)
        {
          free_annotation(a);
        }
      }
      break;
    } // task_muunit_nucleus

    case task_filaments:
    {
      if (t->u.filaments.outer_period <= 0) t->u.filaments.outer_period = 1;
      if (t->u.filaments.preperiod <= 0) t->u.filaments.preperiod = 0;
      if (t->u.filaments.depth <= 0) t->u.filaments.depth = 1;
      char *lohi[2] = { 0, 0 };
      m_r_external_angles(2, &lohi[0], t->u.filaments.inner_nucleus, 0, t->u.filaments.inner_period, 0.75, 32);
      if (lohi[0] && lohi[1])
      {
        m_binangle inner, INNER, outer, OUTER, a, b, i, o;
        m_binangle_init(&inner);
        m_binangle_init(&INNER);
        m_binangle_init(&outer);
        m_binangle_init(&OUTER);
        m_binangle_init(&a);
        m_binangle_init(&b);
        m_binangle_init(&i);
        m_binangle_init(&o);
        m_binangle_from_string(&inner, lohi[0]);
        m_binangle_from_string(&INNER, lohi[1]);
        free(lohi[0]);
        lohi[0] = 0;
        free(lohi[1]);
        lohi[1] = 0;
        // FIXME verify correctness
        mpc_t outer_nucleus;
        mpc_init2(outer_nucleus, mpfr_get_prec(mpc_realref(t->u.filaments.inner_nucleus)));
        m_r_nucleus(outer_nucleus, t->u.filaments.inner_nucleus, t->u.filaments.outer_period, 64, 0);
        m_r_external_angles(2, lohi, outer_nucleus, 0, t->u.filaments.outer_period, 0.75, 32);
        mpc_clear(outer_nucleus);
        if (lohi[0] && lohi[1])
        {
          m_binangle_from_string(&outer, lohi[0]);
          m_binangle_from_string(&OUTER, lohi[1]);
          int depth = 2 * (4 * t->u.filaments.outer_period + t->u.filaments.inner_period * (t->u.filaments.preperiod + 1));
          int outer_preperiod_max = t->u.filaments.depth - t->u.filaments.preperiod;
          if (outer_preperiod_max < 1) outer_preperiod_max = 1;
          for (int outer_preperiod = 0; outer_preperiod <= outer_preperiod_max; ++outer_preperiod)
          {
            for (int outer_pre = outer_preperiod ? 1 : 0; outer_pre < 1 << outer_preperiod; outer_pre += 2)
            {
              mpz_set_ui(a.pre.bits, outer_pre);
              a.pre.length = outer_preperiod;
              mpz_set_ui(a.per.bits, 0);
              a.per.length = 1;
              for (int inner_pre = 0; inner_pre < 1 << t->u.filaments.preperiod; ++inner_pre)
              {
                mpz_set_ui(b.pre.bits, inner_pre);
                b.pre.length = t->u.filaments.preperiod;
                mpz_set_ui(b.per.bits, 0);
                b.per.length = 1;
                m_binangle_tune(&i, &b, &inner.per, &INNER.per);
                m_binangle_tune(&o, &a, &outer.per, &OUTER.per);
                m_block_append(&o.pre, &i.pre, &o.pre);
                task_enqueue(task_new_ray_in(t->output, &o, depth, &t->style));
                if (m_binangle_other_representation(&a) && outer.per.length > 1)
                {
                  m_binangle_tune(&i, &b, &inner.per, &INNER.per);
                  m_binangle_tune(&o, &a, &outer.per, &OUTER.per);
                  m_block_append(&o.pre, &i.pre, &o.pre);
                  task_enqueue(task_new_ray_in(t->output, &o, depth, &t->style));
                }
              }
            }
          }
        }
        m_binangle_clear(&inner);
        m_binangle_clear(&INNER);
        m_binangle_clear(&outer);
        m_binangle_clear(&OUTER);
        m_binangle_clear(&a);
        m_binangle_clear(&b);
        m_binangle_clear(&i);
        m_binangle_clear(&o);
      }
      if (lohi[0])
      {
        free(lohi[0]);
      }
      if (lohi[1])
      {
        free(lohi[1]);
      }
      break;
    } // task_filaments

    case task_rays_of:
    {
      if (t->u.rays_of.count > 0)
      {
        char **rays = calloc(1, t->u.rays_of.count * sizeof(*rays));
        m_r_external_angles(t->u.rays_of.count, rays, t->u.rays_of.nucleus, t->u.rays_of.preperiod, t->u.rays_of.period, 0.75, t->u.rays_of.preperiod == 0 ? 32 : 8);
        for (int i = 0; i < t->u.rays_of.count; ++i)
        {
          if (rays[i])
          {
            m_binangle o;
            m_binangle_init(&o);
            m_binangle_from_string(&o, rays[i]);
            task_enqueue(task_new_ray_in(t->output, &o, 200, &t->style));
            m_binangle_clear(&o);
            free(rays[i]);
          }
        }
        free(rays);
      }
      break;
    } // task_rays_of

    case task_wake:
    {
      task_set_progress(t);
      // copy lines
      struct annotation *a = calloc(1, sizeof(*a));
      a->tag = annotation_wake;
      a->style = t->style;
      int bytes = strlen("[") + strlen(t->u.wake.lo->label) + strlen("|") + strlen(t->u.wake.hi->label) + strlen("]") + 1;
      a->label = calloc(1, bytes + 1);
      snprintf(a->label, bytes, "[%s|%s]", t->u.wake.lo->label, t->u.wake.hi->label);
      a->u.wake.ray_lo = strdup(t->u.wake.lo->label);
      a->u.wake.ray_hi = strdup(t->u.wake.hi->label);
      int dlo = t->u.wake.lo->u.ray_in.depth;
      int dhi = t->u.wake.hi->u.ray_in.depth;
      a->u.wake.ray_depth = dlo > dhi ? dlo : dhi;
      task_set_progress(t);
      a->u.wake.line_start = points_copy(t->u.wake.lo->u.ray_in.line_end, &a->u.wake.line_end);
      task_set_progress(t);
      struct point *p = 0, *q = 0;
      p = points_copy(t->u.wake.hi->u.ray_in.line_end, &q);
      // reverse and prepend
      task_set_progress(t);
      while (p)
      {
        q = p->next;
        p->pred = 0;
        p->next = a->u.wake.line_start;
        a->u.wake.line_start = p;
        if (p->next) p->next->pred = p;
        p = q;
      }
      task_set_progress(t);
      mpq_t slo, shi;
      mpq_init(slo);
      mpq_init(shi);
      m_binangle_to_rational(slo, &t->u.wake.lo->u.ray_in.angle);
      m_binangle_to_rational(shi, &t->u.wake.hi->u.ray_in.angle);
      mpq_init(a->u.wake.width);
      mpq_sub(a->u.wake.width, shi, slo);
      mpq_clear(shi);
      mpq_clear(slo);
      g_async_queue_push(t->output, t->u.wake.lo);
      g_async_queue_push(t->output, t->u.wake.hi);
      g_async_queue_push(t->output, a);
      break;
    } // task_wake

    case task_wake2:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->tag = annotation_wake;
      a->style = t->style;
      int bytes = strlen("[") + strlen(t->u.wake2.lo) + strlen("|") + strlen(t->u.wake2.hi) + strlen("]") + 1;
      a->label = calloc(1, bytes + 1);
      snprintf(a->label, bytes, "[%s|%s]", t->u.wake2.lo, t->u.wake2.hi);
      a->u.wake.ray_lo = strdup(t->u.wake2.lo);
      a->u.wake.ray_hi = strdup(t->u.wake2.hi);
      a->u.wake.ray_depth = t->u.wake2.depth;
      task_set_progress(t);
      // trace lo ray
      m_binangle b;
      m_binangle_init(&b);
      m_binangle_from_string(&b, t->u.wake2.lo);
      int depth = t->u.wake2.depth;
      int sharpness = 16;
      mpq_t q1, q2;
      mpq_init(q1);
      mpq_init(q2);
      m_binangle_to_rational(q1, &b);
      m_r_exray_in *ray = m_r_exray_in_new(q1, sharpness);
      int i = 0;
      double total = 0.5 * sharpness * depth * (1 + depth) * 2;
      double progress = 0;
      int increment = 1;
      m_newton success1 = m_failed;
      m_newton success2 = m_failed;
      int first = 1;
      while (! t->cancelled && (m_failed != (success1 = m_r_exray_in_step(ray, 64))))
      {
        progress = progress + increment;
        t->progress = progress / total;
        task_set_progress(t);
        // prepend point
        a->u.wake.line_start = point_new(a->u.wake.line_start);
        m_r_exray_in_get(ray, a->u.wake.line_start->xy);
        if (first)
        {
          first = 0;
          a->u.wake.line_end = a->u.wake.line_start;
        }
        if (++i >= sharpness)
        {
          i = 0;
          increment = increment + 1;
          if (increment >= depth)
          {
            success1 = m_converged;
            break;
          }
        }
        if (success1 == m_converged)
        {
          break;
        }
      }
      m_r_exray_in_delete(ray);
      // trace hi ray
      m_binangle_from_string(&b, t->u.wake2.hi);
      m_binangle_to_rational(q2, &b);
      ray = m_r_exray_in_new(q2, sharpness);
      i = 0;
      increment = 1;
      while (! t->cancelled && (m_failed != (success2 = m_r_exray_in_step(ray, 64))))
      {
        progress = progress + increment;
        t->progress = progress / total;
        task_set_progress(t);
        // append point
        struct point *end = point_new(0);
        m_r_exray_in_get(ray, end->xy);
        a->u.wake.line_end->next = end;
        end->pred = a->u.wake.line_end;
        a->u.wake.line_end = end;
        if (++i >= sharpness)
        {
          i = 0;
          increment = increment + 1;
          if (increment >= depth)
          {
            success2 = m_converged;
            break;
          }
        }
        if (success2 == m_converged)
        {
          break;
        }
      }
      m_r_exray_in_delete(ray);
      mpq_init(a->u.wake.width);
      mpq_sub(a->u.wake.width, q2, q1);
      mpq_clear(q1);
      mpq_clear(q2);
      if (! t->cancelled && success1 == m_converged && success2 == m_converged)
      {
        g_async_queue_push(t->output, a);
      }
      else
      {
        free_annotation(a);
      }
      break;
    } // task_wake2

    case task_atom:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->tag = annotation_atom;
      a->style = t->style;
      mpfr_prec_t prec = mpfr_get_prec(mpc_realref(t->u.atom.nucleus));
      a->label = malloc(100);
      snprintf(a->label, 99, "Atom p%d", t->u.atom.period);
      mpc_init2(a->u.atom.nucleus, prec);
      mpc_set(a->u.atom.nucleus, t->u.atom.nucleus, MPC_RNDNN);
      a->u.atom.period = t->u.atom.period;
      a->u.atom.shape = m_r_shape(a->u.atom.nucleus, a->u.atom.period);
      mpc_t z, c, interior;
      mpc_init2(interior, 53);
      mpc_init2(z, prec);
      mpc_init2(c, prec);
      mpc_set_dc(z, 0, MPC_RNDNN);
      mpc_set(c, a->u.atom.nucleus, MPC_RNDNN);
      for (int i = 0; i < 64; ++i)
      {
        if (t->cancelled) break;
        t->progress = (i + 0.5) / 64;
        task_set_progress(t);
        double s = (i + (a->u.atom.shape == m_circle ? 0.5 : 0)) * twopi / 64;
        mpc_set_dc(interior, cexp(s * I), MPC_RNDNN);
        m_r_interior(z, c, z, c, interior, a->u.atom.period, 64);
        a->u.atom.line_start = point_new(a->u.atom.line_start);
        if (i == 0)
        {
          a->u.atom.line_end = a->u.atom.line_start;
        }
        mpc_set_prec(a->u.atom.line_start->xy, prec);
        mpc_set(a->u.atom.line_start->xy, c, MPC_RNDNN);
      }
      mpc_clear(c);
      mpc_clear(z);
      mpc_clear(interior);
      if (t->cancelled)
      {
        free_annotation(a);
      }
      else
      {
        g_async_queue_push(t->output, a);
      }
      break;
    } // task_atom

    case task_domain:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->tag = annotation_domain;
      mpfr_prec_t prec = mpfr_get_prec(mpc_realref(t->u.domain.nucleus));
      a->label = malloc(100);
      a->style = t->style;
      mpc_init2(a->u.domain.nucleus, prec);
      mpc_set(a->u.domain.nucleus, t->u.domain.nucleus, MPC_RNDNN);
      a->u.domain.period = t->u.domain.period;
      mpc_t c, domain;
      mpc_init2(domain, 53);
      mpc_init2(c, prec);
      mpc_set_dc(c, 0, MPC_RNDNN);
      mpfr_set_d(mpc_realref(domain), 1.0/0.0, MPFR_RNDN);
      a->u.domain.loperiod = t->u.domain.loperiod;
      if (a->u.domain.loperiod <= 0)
      {
        for (int i = 1; i < a->u.domain.period; ++i)
        {
          mpc_sqr(c, c, MPC_RNDNN);
          mpc_add(c, c, a->u.domain.nucleus, MPC_RNDNN);
          mpc_norm(mpc_imagref(domain), c, MPFR_RNDN);
          if (mpfr_less_p(mpc_imagref(domain), mpc_realref(domain)))
          {
            a->u.domain.loperiod = i;
            mpfr_set(mpc_realref(domain), mpc_imagref(domain), MPFR_RNDN);
          }
        }
      }
      snprintf(a->label, 99, "Domain p%d/%d", a->u.domain.loperiod, a->u.domain.period);
      int sharpness = 64;
      for (int i = 0; i < sharpness; ++i)
      {
        if (t->cancelled) break;
        t->progress = (i + 0.5) / sharpness;
        task_set_progress(t);
        mpc_set(c, a->u.domain.nucleus, MPC_RNDNN);
        double _Complex s = cexp(((i + 0.5) / sharpness) * twopi * I);
        for (int j = 1; j <= sharpness; ++j)
        {
          mpc_set_dc(domain, j * s / sharpness, MPC_RNDNN);
          m_r_domain_coord(c, c, domain, a->u.domain.loperiod, a->u.domain.period, 16);
          if (j == sharpness)
          {
            a->u.domain.line_start = point_new(a->u.domain.line_start);
            if (i == 0)
            {
              a->u.domain.line_end = a->u.domain.line_start;
            }
            mpc_set_prec(a->u.domain.line_start->xy, prec);
            mpc_set(a->u.domain.line_start->xy, c, MPC_RNDNN);
          }
        }
      }
      mpc_clear(c);
      mpc_clear(domain);
      if (t->cancelled)
      {
        free_annotation(a);
      }
      else
      {
        g_async_queue_push(t->output, a);
      }
      break;
    } // task_domain

    case task_misiurewicz_domain:
    {
      struct annotation *a = calloc(1, sizeof(*a));
      a->tag = annotation_misiurewicz_domain;
      mpfr_prec_t prec = mpfr_get_prec(mpc_realref(t->u.misiurewicz_domain.misiurewicz));
      a->label = malloc(100);
      a->style = t->style;
      mpc_init2(a->u.misiurewicz_domain.misiurewicz, prec);
      mpc_set(a->u.misiurewicz_domain.misiurewicz, t->u.misiurewicz_domain.misiurewicz, MPC_RNDNN);
      a->u.misiurewicz_domain.period = t->u.misiurewicz_domain.period;
      a->u.misiurewicz_domain.lopreperiod = t->u.misiurewicz_domain.lopreperiod;
      a->u.misiurewicz_domain.hipreperiod = t->u.misiurewicz_domain.hipreperiod;
      mpc_t c, cp, dc, domain;
      mpc_init2(domain, 53);
      mpc_init2(c, prec);
      mpc_init2(cp, prec);
      mpc_init2(dc, prec);
      mpc_set_dc(c, 0, MPC_RNDNN);
      mpc_set_dc(cp, 0, MPC_RNDNN);
      mpc_set_dc(dc, 0, MPC_RNDNN);
      mpfr_set_d(mpc_realref(domain), 1.0/0.0, MPFR_RNDN);
      if (a->u.misiurewicz_domain.lopreperiod <= 0)
      {
        // find preperiod
        for (int i = 0; i < a->u.misiurewicz_domain.period; ++i)
        {
          mpc_sqr(cp, cp, MPC_RNDNN);
          mpc_add(cp, cp, a->u.misiurewicz_domain.misiurewicz, MPC_RNDNN);
        }
        for (int i = 1; i < a->u.misiurewicz_domain.hipreperiod; ++i)
        {
          mpc_sqr(c, c, MPC_RNDNN);
          mpc_add(c, c, a->u.misiurewicz_domain.misiurewicz, MPC_RNDNN);
          mpc_sqr(cp, cp, MPC_RNDNN);
          mpc_add(cp, cp, a->u.misiurewicz_domain.misiurewicz, MPC_RNDNN);
          mpc_sub(dc, c, cp, MPC_RNDNN);
          mpc_norm(mpc_imagref(domain), dc, MPFR_RNDN);
          if (mpfr_less_p(mpc_imagref(domain), mpc_realref(domain)))
          {
            a->u.misiurewicz_domain.lopreperiod = i;
            mpfr_set(mpc_realref(domain), mpc_imagref(domain), MPFR_RNDN);
          }
        }
      }
      snprintf(a->label, 99, "Domain %d/%dp%d", a->u.misiurewicz_domain.lopreperiod, a->u.misiurewicz_domain.hipreperiod, a->u.misiurewicz_domain.period);
      int sharpness = 64;
      for (int i = 0; i < sharpness; ++i)
      {
        if (t->cancelled) break;
        t->progress = (i + 0.5) / sharpness;
        task_set_progress(t);
        mpc_set(c, a->u.misiurewicz_domain.misiurewicz, MPC_RNDNN);
        double _Complex s = cexp(((i + 0.5) / sharpness) * twopi * I);
        for (int j = 1; j <= sharpness; ++j)
        {
          if (t->cancelled) break;
          mpc_set_dc(domain, j * s / sharpness, MPC_RNDNN);
          m_r_misiurewicz_coord(c, c, domain, a->u.misiurewicz.period, a->u.misiurewicz_domain.lopreperiod, a->u.misiurewicz_domain.hipreperiod, 16);
          if (j == sharpness)
          {
            a->u.misiurewicz_domain.line_start = point_new(a->u.misiurewicz_domain.line_start);
            if (i == 0)
            {
              a->u.misiurewicz_domain.line_end = a->u.misiurewicz_domain.line_start;
            }
            mpc_set_prec(a->u.misiurewicz_domain.line_start->xy, prec);
            mpc_set(a->u.misiurewicz_domain.line_start->xy, c, MPC_RNDNN);
          }
        }
      }
      mpc_clear(c);
      mpc_clear(cp);
      mpc_clear(dc);
      mpc_clear(domain);
      if (t->cancelled)
      {
        free_annotation(a);
      }
      else
      {
        g_async_queue_push(t->output, a);
      }
      break;
    } // task_misiurewicz_domain

  }
  task_done(t);
}
