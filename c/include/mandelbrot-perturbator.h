// mandelbrot-perturbator -- efficient deep zooming for Mandelbrot sets
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License AGPLv3 (only) <https://www.gnu.org/licenses/agpl-3.0.html>

#ifndef MANDELBROT_PERTURBATOR_H
#define MANDELBROT_PERTURBATOR_H 1

#include <mpfr.h>

#ifdef __cplusplus
extern "C" {
#endif

struct perturbator;
struct perturbator *perturbator_new(int workers, int width, int height, int maxiters, int maxchunk, double escape_radius, double glitch_threshold);
// void perturbator_delete(struct perturbator *context);
void perturbator_set_maximum_iterations(struct perturbator *context, int maxiters);
void perturbator_set_detect_glitches(struct perturbator *context, int detect_glitches);
void perturbator_set_approx_skip(struct perturbator *context, int approx_skip);

// rendering control
void perturbator_start(struct perturbator *context, const mpfr_t x, const mpfr_t y, const mpfr_t r);
void perturbator_stop(struct perturbator *context, int force);
bool perturbator_wait(struct perturbator *context, const struct timespec *waituntil);
int perturbator_active(struct perturbator *context);
double perturbator_get_progress(struct perturbator *context);

// get output data
int perturbator_get_width(struct perturbator *context);
int perturbator_get_height(struct perturbator *context);
// pointers are valid until perturbator is deleted, contents are updated during rendering
const int32_t *perturbator_get_dwell_n (struct perturbator *context);
const float   *perturbator_get_dwell_f (struct perturbator *context);
const float   *perturbator_get_dwell_a (struct perturbator *context);
const float   *perturbator_get_distance(struct perturbator *context);
const int32_t *perturbator_get_domain_n(struct perturbator *context);
const float   *perturbator_get_domain_x(struct perturbator *context);
const float   *perturbator_get_domain_y(struct perturbator *context);

int perturbator_get_primary_reference(struct perturbator *context, mpfr_t x, mpfr_t y);

#ifdef __cplusplus
}
#endif

#endif
